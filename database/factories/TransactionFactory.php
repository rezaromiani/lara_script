<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Model\Transaction::class, function (Faker $faker) {
    return [
        'ref_number' => $faker->randomDigit,
        'order_number' => $faker->randomDigit,
        'status' => rand(0, 1),
        'created_at' => $faker->randomElement([
            Carbon::createFromDate(2020, 1, 8),
            Carbon::now(),
            Carbon::createFromDate(2020, 2, 15),
            Carbon::createFromDate(2020, 3, 25),
        ])
    ];
});
