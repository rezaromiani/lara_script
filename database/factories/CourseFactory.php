<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\CourseSession;
use Faker\Generator as Faker;

$factory->define(Model\Course::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'demo_url' => '/video/demo.mp4',
        'course_length' => rand(3, 9) . ' ماه ',
        'start_data' => rand(3, 9) . $faker->randomElement(['بهمن', 'اسفند', 'دی', 'مهر', 'شهریور']),
        'status' => $faker->randomElement(['پیش ثبت نام', 'در حال برگزاری']),
        'price' => rand(2, 8) * 100000,
        'discount' => rand(0, 15),
        'image' => $faker->imageUrl(800, 400),
        'prerequisites' => $faker->paragraph,
        'Headlines' => $faker->paragraph(4),
        'slug' => str_replace(' ', '-', trim($faker->sentence, '.')),
    ];
});
$factory->define(CourseSession::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'video_url' => '/video/demo.mp4',
        'video_time' => rand(10, 60),
        'status' => rand(0, 1),
        'content' => $faker->paragraph(4),
        'thumb' => $faker->imageUrl(250, 150),
        'view' => rand(0, 200),
        'slug' => str_replace(' ', '-', trim($faker->sentence, '.')),
    ];
});
