<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Model\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'content' => $faker->paragraph(5),
        'thumb' => $faker->imageUrl(250, 150),
        'view' => $faker->randomDigit,
        'status' => rand(0, 1),
        'tags' => 'php,laravel,js',
        'slug' => str_replace(' ', '-', trim($faker->sentence, '.')),
        'created_at' => $faker->randomElement([
            Carbon::createFromDate(2020, 1, 5),
            Carbon::createFromDate(2020, 2, 4),
            Carbon::createFromDate(2020, 3, 8),
        ])];
});
