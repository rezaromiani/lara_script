<?php

use Illuminate\Database\Seeder;

class BouncerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**ROLES */
        $admin = Bouncer::role()->firstOrCreate([
            'name' => 'admin',
            'title' => 'مدیر',
        ]);
        $teacher = Bouncer::role()->firstOrCreate([
            'name' => 'teacher',
            'title' => 'مدرس',
        ]);
        $writer = Bouncer::role()->firstOrCreate([
            'name' => 'writer',
            'title' => 'نویسنده',
        ]);
        $user = Bouncer::role()->firstOrCreate([
            'name' => 'user',
            'title' => 'مشارکت کننده',
        ]);
        /* ABILITY */
        $craete_course = Bouncer::ability()->firstOrCreate([
            'name' => 'create-course',
            'title' => 'ساخت دوره',
        ]);
        $create_post =  Bouncer::ability()->firstOrCreate([
            'name' => 'create-post',
            'title' => 'ایجاد مطلب',
        ]);
        $see_dash = Bouncer::ability()->firstOrCreate([
            'name' => 'see-dashboard',
            'title' => 'دیدن پنل مدیریت'
        ]);
        /** ASSIGNMENT */
        Bouncer::allow($teacher)->to([$craete_course,$create_post,$see_dash]);
        Bouncer::allow($writer)->to([$create_post,$see_dash]);
        Bouncer::allow($admin)->everything();
    }
}
