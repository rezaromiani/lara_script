<?php

use App\Model\Category;
use App\Model\Comment;
use App\Model\Course;
use App\Model\CourseSession;
use App\Model\DisCount;
use App\Model\ItemUserChoice;
use App\Model\Option;
use App\Model\Poll;
use App\Model\PollItem;
use App\Model\Post;
use App\Model\Progress;
use App\Model\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $admin = User::create([
            'username' => 'admin',
            'first_name' => 'رضا',
            'last_name' => 'رومیانی',
            'email' => 'admin@gmail.com',
            'password' => '123456',
            'status' => 1,
        ]);
        $admin->assign('admin');
        $teacher = User::create([
            'username' => 'ali',
            'first_name' => 'علی',
            'last_name' => 'راستی',
            'email' => 'ali@gmail.com',
            'password' => '123456',
            'status' => 1,
        ]);
        $teacher->assign('teacher');
        $writer = User::create([
            'username' => 'mohammad',
            'first_name' => 'محمد',
            'last_name' => 'محمدی',
            'email' => 'mm@gmail.com',
            'password' => '123456',
            'status' => 1,
        ]);
        $writer->assign('writer');
        $user = User::create([
            'username' => 'maryam',
            'first_name' => 'مریم',
            'last_name' => 'محمدی',
            'email' => 'mrm@gmail.com',
            'password' => '123456',
            'status' => 1,
        ]);
        $user->assign('user');

        factory(User::class, 20)->create()->each(function ($user) {
            $user->assign('user');
        });

        $allUsers = User::whereIs('user')->get();

        $users = [$admin->id, $teacher->id, $writer->id];

        $category1 = Category::create([
            'cat_name' => 'php'
        ]);
        $category2 = Category::create([
            'cat_name' => 'laravel'
        ]);

        Progress::insert([
            [
                'title' => 'دوره آموزش اسکریپت نویسی PHP پیشرفته',
                'color' => 'info',
                'percentage' => '40'
            ],
            [
                'title' => 'دوره آموزش اسکریپت نویسی Adobe Xd',
                'color' => 'success',
                'percentage' => '50'
            ],
            [
                'title' => 'دوره آموزش Laravel ',
                'color' => 'danger',
                'percentage' => '30'
            ],
            [
                'title' => 'دوره آموزش wordpress',
                'color' => 'warning',
                'percentage' => '60'
            ],
        ]);
        $cats = [$category1->id, $category2->id];
        factory(Post::class, 20)->make()->each(function ($post) use ($allUsers, $cats, $users) {
            $post->author_id = $users[rand(0, 2)];
            $post->cat_id = $cats[rand(0, 1)];
            $post->save();
            factory(Comment::class, rand(1, 8))->make()->each(function ($comment) use ($post, $allUsers) {
                $comment->user_id = $allUsers->random()->id;
                $post->Comment()->save($comment);
            });
        });

        factory(Course::class, 20)->make()->each(function ($course) use ($users, $allUsers) {
            $course->teacher_id = $users[rand(0, 1)];
            $course->save();
            $ids = $allUsers->random(rand(5, 20))->pluck('id');
            foreach ($ids as $id) {
                factory(Transaction::class, 1)->create(['price' => $course->price, 'user_id' => $id, 'course_id' => $course->id]);
            }
            $course->Students()->attach($ids);
            factory(CourseSession::class, rand(5, 10))->make()->each(function ($courseSession) use ($course) {
                $course->Sessions()->save($courseSession);
            });

        });

        Option::create([
            'name' => 'options',
            'value' => [
                "social" => [
                    "telegram" => "https://telegram.org",
                    "instagram" => "https://www.instagram.com",
                    "google" => "https://aboutme.google.com",
                    "twitter" => "https://twitter.com",
                    "facebook" => "https://www.facebook.com",
                ],
                "main" => [
                    "welcome" => "<h2>خودساختگی ، کسب تجربه ، ورود به بازار کار</h2><h2>با اساتید مجرب و کارآزموده در خودآموز لاراول</h2>",
                    "top-bar" => "آيا مي دانيد تا کنون ۱۰۶۲۲ نفر در ۲۳ دوره آموزشي لاراول ثبت نام کرده اند !"
                ],
                "footer" => [
                    "about-us" => "<p>لاراول مرجعی تخصصی برای یادگیری طراحی وب و برنامه نویسی است. ما در لاراول با بهره گیری از نیروهای متخصص، باتجربه و با تحصیلات آکادمیک بالا، تمام تلاش خود را برای تهیه و تولید آموزش های با کیفیت و حرفه ای انجام می دهیم. پیشنهاد می کنیم تا حرفه ای شدن با&nbsp; لاراول &nbsp;همراه باشید!</p>"
                ]
            ]

        ]);

        $poll = Poll::create([
            'poll_title' => 'با برگزاری کدامیک از دورهای زیر موافقید؟',
            'status' => 1,
            'expire_at' => Carbon::now()->addDays(2)
        ]);
        $poll->PollItem()->saveMany([
            new PollItem(['title' => 'اسکریپت نویسی PHP']),
            new PollItem(['title' => 'برنامه نویس پایتون']),
            new PollItem(['title' => 'طراحی قالب وردپرس']),
            new PollItem(['title' => 'پلاگین نویسی وردپرس']),
            new PollItem(['title' => 'اصول طراحی قالب در فتوشاپ']),
        ]);
        foreach ($poll->PollItem()->get() as $item) {
            $user->pollItemChoices()->save(new ItemUserChoice(['poll_id' => $poll->id, 'item_id' => $item->id]));
        }
        DisCount::craete([
            'title' => 'تخفیف ویژه به مناسب ولادت حضرت محمد (ص) آغاز شد',
            'end_at' => Carbon::now()->addDays(10)->format('Y-m-d 00:00:00')
        ]);
    }
}
