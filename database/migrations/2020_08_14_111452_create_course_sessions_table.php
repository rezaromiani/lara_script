<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_sessions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->index();
            $table->longText('content');
            $table->string('video_url')->nullable();
            $table->bigInteger('video_time');
            $table->string('file_url')->nullable();
            $table->string('thumb')->nullable();
            $table->bigInteger('course_id');
            $table->string('slug')->index();
            $table->integer('view')->default(0);
            $table->tinyInteger('status')->default(0);//	0=pending,1=publish
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_sessions');
    }
}
