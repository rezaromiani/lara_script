<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->index();
            $table->longText('content');
            $table->string('thumb')->nullable();
            $table->bigInteger('author_id');
            $table->bigInteger('cat_id');
            $table->string('tags');
            $table->string('slug')->index();
            $table->integer('view')->default(0);
            $table->tinyInteger('status')->default(0);//	0=pending,1=publish
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
