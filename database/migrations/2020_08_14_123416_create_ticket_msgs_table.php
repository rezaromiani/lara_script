<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketMsgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_msg', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ticket_id')->index();
            $table->bigInteger('user_id')->index();
            $table->string('content');
            $table->tinyInteger('status')->default(1);//0=old,1=new
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_msgs');
    }
}
