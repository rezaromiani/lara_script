<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('demo_url');
            $table->string('course_length');
            $table->string('start_data');
            $table->string('status');
            $table->mediumInteger('price');
            $table->tinyInteger('discount');
            $table->string('image');
            $table->bigInteger('teacher_id');
            $table->text('prerequisites');
            $table->longText('Headlines');
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
