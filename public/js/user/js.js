jQuery(document).ready(function ($) {
    $(document).on('click', '.comment-reply', function (event) {
        event.preventDefault();
        var id = $(this).data('id');
        $("#parent_id").val(id);
        var comment = $('#comment-' + id).find('.comment-content').html();
        $('.reply-text').html('پاسخ به :' + comment + "<a class='text-danger cancel-reply'>لغو</a>").fadeIn();
        $("#comment-text").focus();
    });
    $(document).on('click', '.cancel-reply', function (ev) {
        ev.preventDefault();
        do_reset();
    });
    function do_reset(){
        $("#parent_id").val(0);
        $('.reply-text').fadeOut();
    }
    $(document).on('submit', "#commentForm", function (event) {
        event.preventDefault();
        var $this = $(this);
        var post_id = $this.find('input[name=post_id]').val();
        var parent_id = $this.find('input[name=parent_id]').val();
        var comment = $this.find('textarea[name=comment]').val();
        var type = $this.find('input[name=type]').val();
        if (!comment || comment.length <= 2) {
            swal({title: "خطا!", text: "لطفا متن کامنت را بنویسید.", icon: "error", button: "تایید",});
            return 0;
        }
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            method: "POST",
            url: url,
            data: {post_id: post_id, parent_id: parent_id, text: comment,type:type},
            success: function (data) {
                if (data.success) {
                    swal({title: "ممنون از شما!", text: data.msg, icon: "success", button: "تایید",});
                    do_reset();
                }
                $this[0].reset();
            },
            error: function (err) {
                swal({title: "خطا!", text: "خطایی هنگام ثبت پیش آمده لطفا بعدا تلاش کنید.", icon: "error", button: "تایید",});
            }
        })
    });
    $(document).ready(function () {
        $('#vertical-ticker').totemticker({
            row_height: '87px',
            next: '#ticker-next',
            previous: '#ticker-previous',
            stop: '#stop',
            start: '#start',
            max_items: 3,
            mousestop: true,

        });
    });
    $.simpleTicker($("#ticker-fade"), {
        speed: 1000,
        delay: 100000000000,
        easing: 'swing',
        effectType: 'fade'
    });

    $(document).ready(function () {
        $('.countdown').final_countdown({
            start: startCountDown,
            end: endCountDown,
            now: nowCountDown,
            seconds: {
                borderColor: '#7995D5',
                borderWidth: '6'
            },
            minutes: {
                borderColor: '#ACC742',
                borderWidth: '6'
            },
            hours: {
                borderColor: '#ECEFCB',
                borderWidth: '6'
            },
            days: {
                borderColor: '#FF9900',
                borderWidth: '6'
            }
        }, function () {
//alert('تخفیفات ویژه به مناسبت عید مبعث به پایان رسیده است');
        });
    });
//random topic ticker

//bootstrap tooltip fire
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
//bootslidemenu
    $('#slide-nav').BootSideMenu({

        // 'left' or 'right'
        side: "right",

        // animation speed
        duration: 500,

        // restore last menu status on page refresh
        remember: true,

        // auto close
        autoClose: false,

        // push the whole page
        pushBody: false,

        // close on click
        closeOnClick: true,

        // width

        // icons
        icons: {
            left: 'glyphicon glyphicon-chevron-left',
            right: 'glyphicon glyphicon-chevron-right',
            down: 'glyphicon glyphicon-chevron-down'
        },

        // 'dracula', 'darkblue', 'zenburn', 'pinklady', 'somebook'
        theme: '',

    });
//owl slider related post
    $('.owl-carousel').owlCarousel({
        rtl: true,
        loop: true,
        margin: 10,
        /*dots:false,*/
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
    });
//show hidden password
    $(document).ready(function () {
        $('#password').password('hide');
    });
});