$(document).on('click', '#btn-thumbnile-add', function (ev) {
    ev.preventDefault();
    var $fileurl=$('#fileUrl').val();
    var $filename=$('#filename').val();
    if (!$fileurl||!$filename){
        alert('لطفا یک تصویر اتخاب کنید.');
        return;
    }
    $('#post-img').attr('src',$fileurl).fadeIn();
    $('#post-img-input').val($filename);
    $('#myModal').modal('hide');
});
Dropzone.options.myAwesomeDropzone = {
    init: function () {
        this.on("success", function (file, response) {
            $('#profile-content').html(response.html);
        });
        this.on("error", function (file, response) {
            console.log(response);
            $(file.previewElement).find('.dz-error-message').text(response.file);
        });
    },
    addRemoveLinks: true,
    dictDefaultMessage: 'برای آپلود فایل را اینجا رها کنید.',
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
};
$(document).on('click', '.img-upload', function () {
    $('.img-upload').removeClass('active');
    var $this = $(this);
    var $src = $this.attr('src');
    var index = $src.lastIndexOf('/');
    $this.addClass('active');
    $('#fileUrl').val($src);
    $('#filename').val($src.substr(index + 1));
    $('#width').html($this.data('width'));
    $('#height').html($this.data('height'));
});