<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{isset($title)?$title:config('app.name')}}</title>
    <link rel="icon" href="/img/icon.png">
    <link rel="stylesheet" href="/css/user/bootstrap-grid.min.css">
    <link rel="stylesheet" href="/css/user/payment.css">

</head>
<body>
<div class="container-fluid payment-wrapper">
    <div class="row align-items-center justify-content-center">
        @yield('content')
    </div>
</div>
</body>
</html>