<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{isset($title)?$title:config('app.name')}}</title>
    <link rel="icon" href="/img/icon.png">
    <!-- STYLESHEETS-->
    <link rel="stylesheet" href="/css/user/bootstrap-rtl.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="/css/user/style.css">
    <link rel="stylesheet" href="/css/user/BootSideMenu.css">
    <link rel="stylesheet" href="/css/user/owl.carousel.css">
    <link rel="stylesheet" href="/css/user/owl.theme.default.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{route('user.home')}}" >
    <!--JS-->
    <script src="/js/user/jquery.min.1.11.2.js"></script>
    <script src="/js/user/bootstrap.min.js"></script>
    <script src="/js/user/kinetic.js"></script>
    <script src="/js/user/jquery.final-countdown.min.js"></script>
    <script src="/js/user/jquery.simpleTicker.js"></script>
    <script src="/js/user/jquery.totemticker.js"></script>
    <script src="/js/user/jquery.BootSideMenu.js"></script>
    <script src="/js/user/jquery.owl.carousel.js"></script>
    <script src="/js/user/jquery.bootstrap-show-password.js"></script>
    <script src="/js/user/sweetalert.min.js"></script>
    @yield('load-script')
</head>
<body>
@if(\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->can('see-dashboard'))
    @include('user.partials.side-menu')
@endif
@include('user.partials.tool-bar')
@include('user.partials.top-menu')
@yield('content')
@include('user.partials.newsletter')
@include('user.partials.footer')
<script src="/js/user/js.js"></script>
</body>
</html>