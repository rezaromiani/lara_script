<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{isset($title)?$title:config('app.name')}}</title>
    <meta name="csrf-token" content="{{csrf_token() }}">
    @section('load_file')

    <link rel="stylesheet" href="/css/admin/bootstrap.rtl.css" />
    <link rel="stylesheet" href="/css/admin/style.css" />
    <link href="/css/admin/persianDatepicker-lightorang.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="/css/admin/font-awesome.css" />
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <script src="/js/admin/jquery.min.1.11.2.js"></script>
    <script src="/js/admin/bootstrap.js"></script>
    <script src="/js/admin/sweetalert.min.js"></script>
    <!--    behzadi date picker-->
    <script src="/js/admin/persianDatepicker.js"></script>
    <script src="/js/admin/ckeditor/ckeditor.js"></script>
    <script src="/js/user/sweetalert.min.js"></script>
    <link rel="stylesheet" href="/css/admin/select2.min.css" />
    <script src="/js/admin/select2.min.js"></script>
    @show
    <script src="/js/admin/admin-custom.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

    {{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>--}}
</head>

<body class="top-body">
    @include('admin.partials.topnav')
    @include('admin.partials.rightnav')
    <div id="content">
        <div class="inner">
            @yield('content')
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="container-fluid" id="footer">
        <div class="row">
            <div class="col-sm-12">

                <p>کلیه حقوق سایت متعلق به <a href="http://www.7learn.com">{{config('app.name')}}</a> می باشد.</p>

            </div>
        </div>
    </div>
</body>

</html>