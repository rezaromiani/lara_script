@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title"> مدیریت تخفیف ها</h4>
        </div>
    </div>
    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-info">
                <div class="panel-heading">
                    تخفیف های موجود
                </div>
                <div class="panel-body">

                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>عنوان</th>
                                <th>تاریخ غعال بودن</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                    @if(isset($dis_count) && $dis_count)
                        @foreach($dis_count as $d)
                            <tr style="height: 100px">
                                <td>{{$d->title}}</td>
                                <td>{{\Morilog\Jalali\Jalalian::forge($d->end_at)->format('Y-m-d H:i:s')}}</td>
                                <td>
                                    <a href="{{route('admin.discount.del',$d->id)}}" onclick="return confirm('آیا می خواهید این تخفیف را حذف کنید.')" class="label label-danger">حدف</a>

                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">تخفیفی ثبت نشده است.</td>
                        </tr>


                    @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                    <hr>
                    <h4>ویرایش تخفیف</h4>
                    <br>
                    @include('admin.partials.error')
                    @include('admin.partials.sweetAlert')
                    <form method="post" action="">
                        {{csrf_field()}}

                        <div class="form-group">
                            <label>عنوان تخفیف*</label>
                            <select name="title" class="form-control">
                                @if(isset($dis_count) && $dis_count)
                                    @foreach($dis_count as $p)
                                        <option value="{{$p->id}}">{{$p->title}}</option>
                                    @endforeach
                                @else
                                    <option value="">تخفیفی وجود ندارد.</option>
                                @endif


                            </select>

                        </div>

                        <div class="form-group">
                            <label>تعداد روز فعال بودن از امروز*</label>
                            <input type="number" name="day_count" class="form-control"
                                   placeholder="تعدا روز فعال بودن (مثال : عددی مثل 10 وارد کنید)">
                        </div>
                        <input type="submit" name="upgrade_porgress" class="btn btn-success center-block"
                               value="بروز رسانی">
                    </form>

                </div>
            </div>
        </div>


    </div>
    </div>
@endsection