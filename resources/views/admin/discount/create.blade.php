@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3> پیشرفت دوره</h3>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-success">
                <div class="panel-heading">
                    ایجاد تخفیف جدید
                </div>
                <div class="panel-body">

                    @include('admin.partials.error')
                    @include('admin.partials.sweetAlert')


                    <form method="post" action="{{route('admin.discount.create')}}">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label>عنوان*</label>
                            <input type="text" name="title" class="form-control" placeholder="عنوان دوره...">
                        </div>
                        <div class="form-group">
                            <label>تعدا روز فعال بودن*</label>
                            <input type="number" name="day_count" class="form-control"
                                   placeholder="تعدا روز فعال بودن (مثال : عددی مثل 10 وارد کنید)">
                        </div>

                        <input type="submit" name="create_progress" class="btn btn-success center-block"
                               value="ثبت">
                    </form>
                </div>
            </div>
        </div>


    </div>
@endsection