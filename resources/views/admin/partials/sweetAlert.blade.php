<?php if (session('error')): ?>
<div class="alert alert-warning">
    <p>{{session('error')}}</p>
</div>
<?php endif; ?>
<?php if (session('success')): ?>
<div class="alert alert-success">
    <p>{{session('success')}}</p>
</div>
<?php endif; ?>