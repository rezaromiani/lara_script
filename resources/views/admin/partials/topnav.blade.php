<div id="top-nav">
    <nav class="navbar navbar-default navbar-fixed-top custom-navbar-default ">
        <a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip" class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#menu" id="menu-toggle">
            <i class="icon-align-justify"></i>
        </a>
        <!-- LOGO SECTION -->
        <header class="navbar-header">

            <a href="index.php" class="navbar-brand">
                <img src="img/7l.png" alt="" height="30" />
                <span class="site-title">پنل مدیریت</span>
            </a>
        </header>
        <!-- END LOGO SECTION -->
        <ul class="nav navbar-top-links navbar-left">
            <!--ADMIN SETTINGS SECTIONS -->

            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="icon-user "></i>&nbsp; <i class="icon-chevron-down "></i>
                </a>

                <ul class="dropdown-menu dropdown-user">
                    <li><a href="{{route('user.home')}}"><i class="icon-circle-arrow-right"></i> بازگشت به سایت </a>
                    </li>
                    <li><a href="{{route('user.account.index')}}"><i class="icon-user"></i> مشاهده پروفایل </a>
                    </li>
                    <li><a href="{{route('admin.options')}}"><i class="icon-gear"></i> تنظیمات </a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="{{route('logOut')}}"><i class="icon-signout"></i> خروج </a>
                    </li>
                </ul>

            </li>
            <!--END ADMIN SETTINGS -->
        </ul>

    </nav>

</div>