<div id="right-nav">
    <div class="user-image-status">
        <img style="margin: 10px auto" class="img-circle img-responsive" width="80" height="80"
             src="{{\App\Utility\Users::getUserAvatarUrl(Auth::user()->email)}}"/>
        <p style="text-align: center;color: #fff">{{Auth::user()->FullName()}}</p>
        <div class="btn btn-success btn-xs btn-circle"
             style="margin-right: 74px;margin-bottom: 20px;width: 16px;height: 16px"> <span style="margin-right: 16px;
    font-size: 13px;
    font-weight: bold;">آنلاین</span></div>
    </div>
    <ul id="menu" class="collapse">
        <li class="panel active">
            <a href="{{route('admin.dashboard')}}">
                <i class="icon-table"></i> داشبورد
            </a>
        </li>
        <li class="panel ">
            <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle"
               data-target="#post-management">
                <i class="icon-tasks"> </i>مدیریت مطالب
                <span class="pull-left">
                    <i class="icon-angle-down"></i>
                </span>
            </a>
            <ul class="collapse" id="post-management">
                <li class=""><a href="{{ route('admin.post.index') }}"><i class="icon-angle-left"></i> کل مطالب </a>
                </li>
                <li class=""><a href="{{route('admin.post.create')}}"><i class="icon-angle-left"></i> افزودن نوشته </a>
                </li>
                <li class=""><a href="{{route('admin.categories.index')}}"><i class="icon-angle-left"></i> دسته ها </a>
                </li>
            </ul>
        </li>
        @if(\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->can('create-course'))
            <li class="panel ">
                <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle"
                   data-target="#course-management">
                    <i class="material-icons">
                        import_contacts
                    </i>مدیریت دوره ها
                    <span class="pull-left">
                    <i class="icon-angle-down"></i>
                </span>
                </a>
                <ul class="collapse" id="course-management">
                    <li class=""><a href="{{ route('admin.course.index') }}"><i class="icon-angle-left"></i> همه دوره ها
                        </a>
                    </li>
                    <li class=""><a href="{{route('admin.course.create')}}"><i class="icon-angle-left"></i> افزودن دوره
                        </a>
                    </li>

                </ul>
            </li>
            <li class="panel">
                <a href="javascript:" data-parent="#menu" data-toggle="collapse" class="accordion-toggle"
                   data-target="#progress_course">
                    <i class="icon-table"></i> پیشرفت دوره ها

                    <span class="pull-left">
                    <i class="icon-angle-down"></i>
                </span>
                </a>
                <ul class="collapse" id="progress_course">
                    <li><a href="{{route('admin.progress.create')}}"><i class="icon-angle-left"></i> ایجاد پیشرفت دوره
                        </a></li>
                    <li><a href="{{route('admin.progress.index')}}"><i class="icon-angle-left"></i> مدیریت پیشرفت دوره
                            ها </a></li>

                </ul>
            </li>
        @endif
        <li class="panel ">
            <a href="{{route('admin.comment.index')}}" data-parent="#menu" data-toggle="collapse"
               class="accordion-toggle collapsed" data-target="#form-nav">
                <i class="icon-comments-alt"></i> دیدگاه ها

                <span class="pull-left">
                    <i class="icon-angle-down"></i>
                </span>
                &nbsp; <span class="label label-success">{{\App\Utility\PostUtility::getNewCommentCount()}}</span>&nbsp;
            </a>
        </li>


        <li class="panel">
            <a href="javascript:" data-parent="#menu" data-toggle="collapse" class="accordion-toggle"
               data-target="#chart-nav">
                <i class="icon-gears"></i>مدیریت منوها
                <span class="pull-left">
                    <i class="icon-angle-down"></i>
                </span>
            </a>
            <ul class="collapse" id="chart-nav">
                <li><a href="{{route('admin.menu.index')}}"><i class="icon-angle-left"></i> ایجاد منو </a></li>
            </ul>
        </li>

        <li class="panel">
            <a href="javascript:" data-parent="#menu" data-toggle="collapse" class="accordion-toggle"
               data-target="#DDL-nav">
                <i class=" icon-user"></i> پروفایل کاربری

                <span class="pull-left">
                    <i class="icon-angle-down"></i>
                </span>
            </a>
            <ul class="collapse" id="DDL-nav">
                <li><a href="{{route('admin.user.profile')}}"><i class="icon-angle-left"></i>شناسنامه من </a></li>
            </ul>
        </li>
        <li class="panel">
            <a href="javascript:" data-parent="#menu" data-toggle="collapse" class="accordion-toggle"
               data-target="#transaction">
                <i class=" icon-dollar"></i> مدیریت تراکنش ها
                <span class="pull-left">
                    <i class="icon-angle-down"></i>
                </span>
            </a>
            <ul class="collapse" id="transaction">
                <li><a href="{{route('admin.transaction.index')}}"><i class="icon-angle-left"></i> مشاهده تراکنش ها </a>
                </li>

            </ul>
        </li>
        <li class="panel">
            <a href="javascript:" data-parent="#menu" data-toggle="collapse" class="accordion-toggle"
               data-target="#tickets">
                <i class="icon-ticket"></i>مدیریت تیکت ها

                <span class="pull-left">
                    <i class="icon-angle-down"></i>
                </span>

            </a>
            <ul class="collapse" id="tickets">
                <li><a href="{{route('admin.ticket.index')}}"><i class="icon-angle-left"></i> مدیریت تیکت های کاربران
                    </a></li>
            </ul>
        </li>
        @if(\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->isAn('admin'))
            <li class="panel">
                <a href="javascript:" data-parent="#menu" data-toggle="collapse" class="accordion-toggle"
                   data-target="#discount">
                    <i class="icon-shopping-cart"></i>مدیریت تخفیف ها

                    <span class="pull-left">
                    <i class="icon-angle-down"></i>
                </span>

                </a>
                <ul class="collapse" id="discount">
                    <li><a href="{{route('admin.discount.create')}}"><i class="icon-angle-left"></i> افزودن تخفیف جدید
                        </a>
                    </li>
                    <li><a href="{{route('admin.discount.index')}}"><i class="icon-angle-left"></i> مدیریت تخفیف </a>
                    </li>

                </ul>
            </li>
            <li class="panel">
                <a href="javascript:" data-parent="#menu" data-toggle="collapse" class="accordion-toggle"
                   data-target="#ads">
                    <i class="icon-gears"></i> مدیریت تبلیغات
                    <span class="pull-left">
                    <i class="icon-angle-down"></i>
                </span>
                </a>
                <ul class="collapse" id="ads">
                    <li><a href="{{route('admin.ads.index')}}"><i class="icon-angle-left"></i> ایجاد تبلیغ </a></li>

                </ul>
            </li>
        @endif
        <li class="panel">
            <a href="javascript:" data-parent="#menu" data-toggle="collapse" class="accordion-toggle"
               data-target="#user-management">
                <i class="icon-user"></i> مدیریت کاربران

                <span class="pull-left">
                    <i class="icon-angle-down"></i>
                </span>

            </a>
            <ul class="collapse" id="user-management">
                <li><a href="{{route('admin.user.create')}}"><i class="icon-angle-left"></i> ایجاد کاربر جدید </a></li>
                <li><a href="{{route('admin.user.index')}}"><i class="icon-angle-left"></i> ویرایش کاربران</a></li>
            </ul>
        </li>
        <li class="panel">
            <a href="javascript:" data-parent="#menu" data-toggle="collapse" class="accordion-toggle"
               data-target="#createmenu">
                <i class="icon-gears"></i> تنظیمات قالب
                <span class="pull-left">
                    <i class="icon-angle-down"></i>
                </span>
            </a>
            <ul class="collapse" id="createmenu">
                <li><a href="{{route('admin.notification.index')}}"><i class="icon-angle-left"></i> اطلاعیه ها </a></li>
                <li><a href="{{route('admin.slider.index')}}"><i class="icon-angle-left"></i>اسلایدر</a></li>
                @if(\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->isAn('admin'))
                    <li><a href="{{route('admin.options')}}"><i class="icon-angle-left"></i>تنظیمات </a></li>
                @endif
            </ul>
        </li>

        <li class="panel">
            <a href="javascript:" data-parent="#menu" data-toggle="collapse" class="accordion-toggle"
               data-target="#polls">
                <i class="icon-gears"></i> نظرسنجی
                <span class="pull-left">
                    <i class="icon-angle-down"></i>
                </span>
            </a>
            <ul class="collapse" id="polls">
                <li><a href="{{route('admin.poll.index')}}"><i class="icon-angle-left"></i>مدیریت نظرسنجی</a></li>

            </ul>
        </li>
    </ul>
</div>