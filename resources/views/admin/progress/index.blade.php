@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title"> مدیریت پیشرفت دوره (ها)</h4>
        </div>
    </div>
    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-info">
                <div class="panel-heading">
                    مدیریت پیشرفت دوره(های) موجود
                </div>
                <div class="panel-body">

                    <form method="post" action="" >
                        {{csrf_field()}}
                        @if(isset($progress) && $progress)
                            @foreach($progress as $p)
                            <div class="col-md-6">

                                <li>
                                    <span>{{$p->title}}</span> <a style="margin-right: 5px" href="{{route('admin.progress.del',$p->id)}}" class="text-danger">حذف</a>
                                    <br>
                                    <br>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-striped progress-bar-{{$p->color}} active"
                                             role="progressbar" aria-valuemin="0" aria-valuemax="100"
                                             style="width:{{$p->percentage}}%">
                                            %{{$p->percentage}}
                                        </div>
                                </li>
                            </div>
                            @endforeach
                        @else
                            <div class="alert alert-warning">
                                <p>پیشرفت دورهای ثبت نشده است.</p>
                            </div>
                        @endif


                        <div class="clearfix"></div>
                            @include('admin.partials.error')
                            @include('admin.partials.sweetAlert')

                            <div class="form-group">
                            <label>عنوان دوره*</label>
                           <select name="title" class="form-control">
                               @if(isset($progress) && $progress)
                                   @foreach($progress as $p)
                                    <option value="{{$p->id}}">{{$p->title}}</option>
                                   @endforeach
                               @else
                                   <option value="">پیشرفت دوره ای وجود ندارد.</option>
                               @endif


                            </select>

                        </div>
                        <div class="form-group">
                            <label>رنگ*</label>
                            <select name="color" class="form-control">
                                <option value="default">پشفرض (آبی تیره)</option>
                                <option value="info">آبی روشن</option>
                                <option value="warning">نارنجی</option>
                                <option value="success">سبز</option>
                                <option value="danger">قرمز</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>میزان پیشرفت دوره*</label>
                            <input type="number" name="percent" class="form-control"
                                   placeholder="میزان پیشرفت دوره (مثال : عددی بین 1 تا 100 وارد نمایید)">
                        </div>
                        <input type="submit" name="upgrade_porgress" class="btn btn-success center-block"
                               value="بروز رسانی">
                    </form>

                </div>
            </div>
        </div>


    </div>
    </div>
@endsection