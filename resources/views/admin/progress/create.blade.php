@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3> پیشرفت دوره</h3>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-success">
                <div class="panel-heading">
                    ایجاد پیشرفت دوره جدید
                </div>
                <div class="panel-body">

                    @include('admin.partials.error')
                    @include('admin.partials.sweetAlert')


                    <form method="post" action="{{route('admin.progress.create')}}">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label>عنوان*</label>
                            <input type="text" name="title" class="form-control" placeholder="عنوان دوره...">
                        </div>
                        <div class="form-group">
                            <label>میزان پیشرفت دوره*</label>
                            <input type="number" name="percent" class="form-control"
                                   placeholder="میزان پیشرفت دوره (مثال : عددی بین 1 تا 100 وارد نمایید)">
                        </div>
                        <div class="form-group">
                            <label>رنگ*</label>
                            <select name="color" class="form-control">
                                <option value="default">پشفرض (آبی تیره)</option>
                                <option value="info">آبی روشن</option>
                                <option value="warning">نارنجی</option>
                                <option value="success">سبز</option>
                                <option value="danger">قرمز</option>
                            </select>
                        </div>
                        <input type="submit" name="create_progress" class="btn btn-success center-block"
                               value="ثبت">
                    </form>
                </div>
            </div>
        </div>


    </div>
@endsection