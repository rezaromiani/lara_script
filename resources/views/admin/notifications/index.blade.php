@extends('layouts.admin')


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title">مدیریت اطلاعیه ها</h4>
        </div>
    </div>
    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-info">
                <div class="panel-heading">
                    مدیریت اطلاعیه ها
                </div>
                <div class="panel-body">
                    @include('admin.partials.error')
                    <p class="tab-pane" id="profile-user-all-tickets">


                    <form method="post" action="">
                        {{csrf_field()}}
                        <div class="col-xs-5">
                            <textarea name="text" class="form-control" id="" cols="30" rows="10">{{ old('slide',isset($notification)?$notification->text : null) }}</textarea>
                            <br>
                        </div>

                        <div class="col-xs-2">
                            <input type="submit" class="form-control btn btn-success" name="btn-submit-slider"
                                   value="ثبت">
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-success">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>متن اطلاعیه</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if(isset($notifications) && $notifications)
                                    @foreach($notifications as $s)
                                        <tr>
                                            <td><p>{{$s->text}}</p></td>
                                            <td>
                                                <a href="{{route('admin.notification.del',$s->id)}}" onclick="return confirm('آیا می خواهید این اطلاعیه را حذف کنید.')" class="label label-danger">حدف</a>
                                                <a href="{{route('admin.notification.edit',$s->id)}}" class="label label-warning">ویرایش</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>

@endsection
