@extends('layouts.admin')


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title">مدیریت تیکت ها</h4>
        </div>
    </div>
    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-info">
                <div class="panel-heading">
                    مدیریت تیکت کاربران
                </div>
                <div class="panel-body">


                    <div class="tab-pane" id="profile-user-all-tickets">

                        @if(isset($tickets)&& $tickets)
                            @foreach($tickets as $ti)
                                <div class="panel panel-info">
                                    <div class="load-t lod ">
                                        <p>بارگذاری...</p>
                                    </div>
                                    <div class="panel-heading" id="">
                                        <a href="#section-{{$ti->id}}" data-toggle="collapse" data-parent="#accordion">موضوع
                                            تیکت
                                            :{{$ti->title}}</a>
                                        <span class="btn btn-warning">{{$ti->ticket_format()}}</span>
                                        <span class="pull-left btn btn-info"
                                              style="margin-right: 5px">شماره تیکت : {{$ti->id}}</span>
                                        <span class="pull-left btn btn-success">پیام جدید : <span class="count-new-msg-{{$ti->id}}">{{\App\Utility\TicketUtility::getNewMsgCount($ti,Auth::user())}}</span></span>
                                    </div>
                                    <div id="section-{{$ti->id}}"
                                         class="panel-collapse collapse  custom-panel-collapse">
                                        <div class="ticket-details-wrapper">
                                            <div id="ticket-msg-container-{{$ti->id}}">
                                                @include('user.account.answermsg')
                                            </div>
                                            <div class="ticket-answer" id="">
                                                <form class="ticket-answer-frm" method="post">
                                                    <div class="form-group">
                                                        <textarea class="form-control" name="ticket_text"
                                                                  rows="10"></textarea>
                                                    </div>
                                                    <input type="hidden" name="ticket_id" value="{{$ti->id}}">

                                                    <div class="form-group text-left">
                                                        <input type="submit" class="btn btn-success" value="ارسال پیام">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <script>
                                jQuery(document).ready(function ($) {
                                    $(document).on('submit', '.ticket-answer-frm', function (ev) {
                                        ev.preventDefault();
                                        let $this = $(this);
                                        let id = $this.find('input[name="ticket_id"]').val();
                                        let load = $('.load-t');
                                        $.ajaxSetup({
                                            headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                            }
                                        });
                                        $.ajax({
                                            url:{!! json_encode(route('admin.ticket.answer')) !!},
                                            method: 'post',
                                            data: $this.serialize(),
                                            beforeSend: function () {
                                                load.fadeIn();
                                            },
                                            success: function (data) {
                                                if (data.success) {
                                                    swal("", "پـیــام با مـوفـقیـت ثـبـت شــد.", "success");
                                                    $this[0].reset();
                                                    $('#ticket-msg-container-' + id).html(data.view);
                                                    $('#count-new-msg-' + id).html(data.count);
                                                }
                                                if (data.error) {
                                                    swal("خـطـا!", "خطایـی هنـگـام عمـیـلـیـات پـیـش آمـده.", "error")
                                                }

                                            },
                                            error: function () {
                                                swal("خـطـا!", "خطایـی هنـگـام عمـیـلـیـات پـیـش آمـده.", "error")
                                            },
                                            complete: function () {
                                                load.fadeOut()
                                            }
                                        })
                                    });
                                })
                            </script>
                        @else
                            <div class="alert alert-danger text-center">در حال حاضر پیامی ندارید!</div>
                        @endif

                    </div>


                </div>
            </div>
        </div>

    </div>
@endsection