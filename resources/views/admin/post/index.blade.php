@extends('layouts.admin')

@section('load_file')
    @parent
    <link rel="stylesheet" href="/css/admin/dropzone.css">
    <script src="/js/admin/dropzone.js"></script>

@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3>مطالب تهیه شده</h3>
        </div>
    </div>
    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-info">
                <div class="panel-heading">
                    <ul class="list-inline user-page-admin" style="position: relative">
                        <li>
                            <form class="navbar-form" method="get" action="post_search.php">
                                <div class="form-group">
                                    <input type="text" name="txt_search" class="form-control" placeholder="جستجو...">
                                </div>
                                <button type="submit" name="search" class="btn btn-info">بگرد</button>
                            </form>
                        </li>

                        <form method="post">
                            {{csrf_field()}}
                            <li style="float: left">
                                <button class="label label-danger" type="submit" name="del-all"
                                        style="position: absolute;top: 0px;left: 45px;">حذف کلی
                                </button>
                            </li>
                    </ul>

                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="select-all"></th>
                                <th>عنوان</th>
                                <th>نویسنده</th>
                                <th>دسته ها</th>
                                <th><i class="fa icon-comments"></i></th>
                                <th>تاریخ</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($posts && count($posts)>0)
                                @foreach($posts as $p)
                                    <tr style="height: 100px">
                                        <th><input type="checkbox" class="checkbox" name="checkbox[]" value="{{$p->id}}"></th>
                                        <th>{{$p->title}}</th>
                                        <th>{{$p->User->username}}</th>
                                        <th>{{$p->Category->cat_name}}</th>
                                        <th>{{$p->Comment->count()}}</th>
                                        <th>{{\Morilog\Jalali\Jalalian::forge($p->created_at)->format('%B %d، %Y')}}</th>
                                        <th>
                                            <a href="{{route('admin.post.delete',$p->id)}}" onclick="return confirm('آیا می خواهید این متلب را حذف کنید.')" class="label label-danger">حدف</a>
                                            <a href="{{route('admin.post.edit',$p->id)}}" class="label label-success">ویرایش</a>
                                        </th>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    هیچ مطلبی ثبت نشده.
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12" style=" text-align: center;">
                @if(isset($posts) &&$posts)
                    {{$posts->links()}}
                @endif
            </div>
        </div>
    </div></div>
@endsection