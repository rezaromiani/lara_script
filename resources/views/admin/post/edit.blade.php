@extends('layouts.admin')

@section('load_file')
    @parent
    <link rel="stylesheet" href="/css/admin/dropzone.css">
    <script src="/js/admin/dropzone.js"></script>

@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3> ویرایش نوشته</h3>
        </div>
    </div>
    <hr/>
    <script>

    </script>
    <div class="row">
        <div class="col-lg-12">

            <div class="panel box box-warning">
                <div class="panel-heading">
                    نوشته جدید
                </div>
                <div class="col-md-12">
                    @include('admin.partials.error')
                </div>
                <div class="panel-body">
                    <form method="post" action="" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" name="title" class="form-control"
                                       placeholder="عنوان محصول را اینجا وارد نمایید..."
                                       value="{{$post->title}}">
                            </div>
                            <div class="from-group" style="margin-bottom: 10px;">
                                <button type="button" id="file_upload_btn_general" class="btn btn-default"
                                        data-toggle="modal"
                                        data-target="#myModal">آپلود فایل
                                </button>
                            </div>
                            <div class="form-group">
                                        <textarea name="detail" class="form-control ckeditor" style="direction: rtl"
                                                  placeholder="محتوای مورد نظر">{{$post->content}}</textarea>
                            </div>
                            <br>
                            <div class="panel box box-info">
                                <div class="panel-heading">نامک :</div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <input type="text" name="slug" class="form-control"
                                               placeholder="نامک را با - جدا کنید"
                                               value="{{old('slug',isset($post)?$post['slug']:'')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-info">
                                <div class="panel-heading">لینک ویدئو :</div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <input type="text" name="video_url" class="form-control"
                                               placeholder="لینک ویدئو ..."
                                               value="{{old('video_url',isset($post)?$post['video_url']:'')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-info">
                                <div class="panel-heading">نویسنده :</div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <select name="author_id" class="form-control">
                                            @if(count($users))
                                                @foreach($users as $user)
                                                    <option value="{{$user->id}}" {{isset($post)&& $post['author_id']==$user->id?'selected':''}}>{{$user->first_name.' '.$user->last_name}}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-3">
                            <div class="panel box box-info">
                                <div class="panel-heading">انتشار</div>
                                <div class="panel-body" style="max-height: 200px; overflow: auto;">
                                    <input type="submit" name="preview" class="btn btn-info btn-sm"
                                           style="float:left" value="پیش نمایش">
                                    <br>
                                    <br>
                                    <br>
                                    <span><i class="icon-calendar"></i> تاریخ انتشار :   </span>
                                    <br>
                                    @if(isset($post))
                                        {{$post['created_at']}}
                                    @endif
                                </div>
                                <div class="panel-footer">

                                    <input type="submit" name="delete" class="btn btn-warning btn-sm"
                                           value="حذف" onclick="return confirm('می خواهید این پست را حذف کنید.')">


                                    @if(isset($post)&& intval($post->status) )
                                        <input type="submit" name="publish" class="btn btn-success btn-sm"
                                               value="انتشار">

                                    @else
                                        <input type="submit" name="pending" class="btn btn-success btn-sm"
                                               value="ذخیره پیش نویس">
                                        <input type="submit" name="publish" class="btn btn-success btn-sm"
                                               value="انتشار">
                                    @endif
                                </div>
                            </div>
                            <div class="panel box box-info">
                                <div class="panel-heading">موضوع مطلب</div>
                                <div class="panel-body" style="max-height: 200px; overflow: auto;">

                                    <div class="form-group">
                                        <select name="cat-id" class="form-control">

                                            @if(count($cats))
                                                @foreach($cats as $cat)
                                                    <option value="{{$cat->id}}" {{isset($post)&& $post['cat_id']==$cat->id?'selected':''}} >{{$cat->cat_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    </ul>
                                </div>
                            </div>
                            <div class="panel box box-info">
                                <div class="panel-heading">برچسب ها</div>
                                <div class="panel-body">
                                    <input type="text" class="form-control" name="tags" placeholder="برچسب..."
                                           value="{{old('tags',isset($post)?$post['tags']:'')}}">
                                </div>
                                <div class="panel-footer" style=" padding: 10px 11px;">
                                    <div class="label label-info">توجه : برچسب ها را با کاما ( , ) از یکدیگر جدا
                                        کنید!
                                    </div>
                                    <input type="hidden" name="post_id"
                                           value="{{$post->id}}">
                                </div>
                            </div>
                            <div class="panel box box-info">
                                <div class="panel-heading">تصویر شاخص</div>
                                <div class="panel-body">
                                    <input type="text" class="form-control" name="file" id="post-img-input"
                                           value="{{old('file',isset($post)?$post['thumb']:'')}}">
                                </div>
                                <div class="panel-footer" style="    padding: 10px 11px;">

                                    <img src="{{isset($post['thumb']) ? asset('storage/upload/image/'.$post['thumb']):''}}"
                                         alt="" class="img-responsive img-thumbnail {{isset($post['thumb']) ? '':'dn'}}"
                                         id="post-img">


                                    <button type="button" id="img-thumb-modal-btn" class="btn btn-default"
                                            data-toggle="modal"
                                            data-target="#myModal">انتخاب تصویر
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
        @include('admin.uploader.upload')
    </div>
    <!-- Modal -->

@endsection