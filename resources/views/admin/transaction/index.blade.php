@extends('layouts.admin')


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title">لیست تراکنش ها</h4>
        </div>

    </div>
    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-info">
                <div class="panel-heading">
                    <div class="col-lg-12">
                        <form action="" method="get">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">جستجو براساس</label>
                                        <select name="search_type" class="form-control" id="">
                                            <option value="ref_number">شماره تراکنش</option>
                                            <option value="order_number">شماره سفارش</option>
                                            <option value="email">شماره ایمیل کاربر</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">عنوان</label>
                                        <input type="text" name="search" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <button class="btn btn-info" type="submit" style="margin-top: 26px;">جستجو
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12">
                        <ul class="list-inline user-page-admin" style="position: relative">

                            <form method="post" action="{{route('admin.transaction.delete.many')}}">
                                {{csrf_field()}}
                                <li style="float: left">
                                    @if($admin)
                                        <button class="label label-danger" type="submit" name="del-all"
                                                style="position: absolute;top: 0px;left: 45px;">حذف کلی
                                        </button>
                                    @endif
                                </li>
                        </ul>
                    </div>

                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                @if($admin)
                                    <th><input type="checkbox" class="select-all"></th>
                                @endif
                                <th>شماره سفارش</th>
                                <th>شماره تراکنش</th>
                                <th>تاریخ پرداخت</th>
                                <th>زمان پرداخت</th>
                                <th>نام کاربر</th>
                                <th>ایمیل</th>
                                <th>مبلغ</th>
                                <th>وضعیت</th>
                                @if($admin)
                                    <th>عملیات</th>
                                @endif
                            </tr>

                            </thead>
                            <tbody>
                            @if(isset($transactions) &&$transactions)
                                @foreach($transactions as $t)
                                    <tr>
                                        @if($admin)

                                        <th><input type="checkbox" class="checkbox" name="checkbox" value="{{$t->id}}">
                                        </th>
                                        @endif
                                        <th>{{$t->order_number}}</th>
                                        <th>{{$t->ref_number}}</th>
                                        <th>{{\Morilog\Jalali\Jalalian::forge($t->created_at)->format('Y-m-d')}}</th>
                                        <th>{{\Morilog\Jalali\Jalalian::forge($t->created_at)->format('H:i:s')}}</th>
                                        <th>{{$t->User->FullName()}}</th>
                                        <th>{{$t->User->email}}</th>
                                        <th>{{number_format($t->price)}}تومان</th>
                                        <th>
                                            @if(intval($t->status == 1))
                                                <span class="label label-success">موفق</span>
                                            @else
                                                <span class="label label-danger">نا موفق</span>
                                            @endif
                                        </th>
                                        @if($admin)

                                        <th>
                                            <a href="{{route('admin.transaction.delete',$t->id)}}"
                                               class="label label-danger">حدف</a>
                                            <a href="{{route('admin.transaction.edit',$t->id)}}"
                                               class="label label-success">ویرایش</a>
                                        </th>
                                            @endif
                                    </tr>
                                @endforeach
                            @endif


                            </tbody>

                        </table>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12" style=" text-align: center;">
                @if(isset($transactions) &&$transactions)
                    {{$transactions->links()}}
                @endif
            </div>
        </div>
    </div></div>
@endsection