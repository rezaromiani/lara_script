@extends('layouts.admin')

@section('load_file')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title">ویرایش تراکنش</h4>
            <a class="btn btn-warning" href="{{route('admin.transaction.index')}}">برگشت</a>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="panel box box-success">
                <div class="panel-heading">
                    کاربر جدید
                </div>
                @include('admin.partials.error')
                <div class="panel-body">
                    <form method="post" action="">
                        <div class="form-group">
                            <label>شماره تراکنش</label>
                            <input type="text" name="ref_number" class="form-control" placeholder="نام..."
                                   value="{{old('ref_number',isset($transaction)?$transaction->ref_number:'')}}">
                        </div>


                            {{csrf_field()}}
                            <label style="display: block">وضعیت</label>
                            <select name="status" class="my-custom-select" >
                                <option value="1" {{isset($transaction) && $transaction->status == 1 ?'selected':''}}>
                                    موفق
                                </option>
                                <option value="0" {{isset($transaction) && $transaction->status == 0 ?'selected':''}}>
                                    ناموفق
                                </option>
                                <
                            </select>
                        </div>
                        <input type="submit" name="btn_submit" class="btn btn-success center-block" value="ثبت">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection