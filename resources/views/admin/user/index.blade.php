@extends('layouts.admin')


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1> کاربران</h1>
        </div>

    </div>
    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-warning">
                <div class="panel-heading">
                    <ul class="list-inline user-page-admin" style="position: relative">
                        <li><a href="{{route('admin.user.index')}}">همه</a></li>
                        @if(isset($roles) && $roles)
                            @foreach($roles as $r)
                        <li><a href="{{route('admin.user.index.order',$r->name)}}">{{$r->title}}</a></li>

                            @endforeach
                        @endif
                        <form method="post">
                            {{csrf_field()}}
                            <li style="float: left">
                                <button class="label btn btn-danger" type="submit" name="del-all"
                                        style="position: absolute;top: 0px;left: 45px;">حذف کلی
                                </button>
                            </li>
                    </ul>
                    <br><br>
                    @include('admin.partials.sweetAlert')
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="select-all"></th>
                                <th>نام و نام خانوادگی</th>
                                <th>نام کاربری</th>
                                <th>ایمیل</th>
                                <th>تعداد پست ها</th>
                                <th>نوع کاربری</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($users as $user)
                                <tr>
                                    <th><input type="checkbox" class="checkbox" name="checkbox[]" value="{{$user->id}}">
                                    </th>
                                    <th>{{$user->first_name .' '.$user->last_name}}</th>
                                    <th>{{$user->username}}</th>
                                    <th>{{$user->email}}</th>
                                    <th>{{ $user->post->count() }}</th>
                                    <th> {{ App\Utility\Users::getRole($user->getRoles()[0]) }}
                                    </th>
                                    <th>
                                        <a href="{{route('admin.user.delete',$user->id)}}" class="label label-danger">حدف</a>
                                        <a href="{{route('admin.user.edit',$user->id)}}" class="label label-success">ویرایش</a>
                                    </th>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12" style=" text-align: center;">
                @if(isset($users) &&$users)
                    {{$users->links()}}
                @endif
            </div>
        </div>
    </div>
@endsection