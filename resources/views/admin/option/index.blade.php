@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title">تنظیمات عمومی سایت</h4>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-success">
                @include('admin.partials.error')
                <div class="panel-body">
                    <form method="post" action="">
                        <div class="col-md-5">
                            <h3 class="options-head">شبکه های اجتماعی</h3>
                            <div class="form-group">
                                <label>تلگرام</label>
                                <input type="text" name="options[social][telegram]" class="form-control ltr"
                                       placeholder=""
                                       value="{{isset($options['social']['telegram'])?$options['social']['telegram']:''}}">
                            </div>
                            <div class="form-group">
                                <label>اینستاگرام</label>
                                <input type="text" name="options[social][instagram]" class="form-control ltr"
                                       placeholder=""
                                       value="{{isset($options['social']['instagram'])?$options['social']['instagram']:''}}">
                            </div>
                            <div class="form-group">
                                <label>گوگل پلاس</label>
                                <input type="text" name="options[social][google]" class="form-control ltr"
                                       placeholder=""
                                       value="{{isset($options['social']['google'])?$options['social']['google']:''}}">
                            </div>
                            <div class="form-group">
                                <label>تویتر</label>
                                <input type="text" name="options[social][twitter]" class="form-control ltr"
                                       placeholder=""  value="{{isset($options['social']['twitter'])?$options['social']['twitter']:''}}">
                            </div>
                            <div class="form-group">
                                <label>فیسبوک</label>
                                <input type="text" name="options[social][facebook]" class="form-control ltr"
                                       placeholder=""  value="{{isset($options['social']['facebook'])?$options['social']['facebook']:''}}">
                            </div>
                            <hr>
                            <h3 class="options-head"> صفحه اصلی</h3>
                            <div class="form-group">
                                <label>متن خوش آمد(از سرنویس 2 استفاده کنید)</label>
                                <textarea name="options[main][welcome]" class="form-control ckeditor"
                                          style="direction: rtl"
                                          placeholder="محتوای مورد نظر">{!! isset($options['main']['welcome'])?trim($options['main']['welcome'],'"'):'' !!}</textarea>
                            </div>
                            <div class="form-group">
                                <label>متن تاپ بار سایت</label>
                                <input type="text" name="options[main][top-bar]" class="form-control " placeholder="" value="{{isset($options['main']['top-bar'])?$options['main']['top-bar']:''}}">
                            </div>
                            <hr>
                            <h3 class="options-head">فوتر</h3>
                            <div class="form-group">
                                <label>متن در باره ما</label>
                                <textarea name="options[footer][about-us]" class="form-control ckeditor"
                                          style="direction: rtl"
                                          placeholder="محتوای مورد نظر">{!! isset($options['footer']['about-us'])?$options['footer']['about-us']:'' !!}</textarea>
                            </div>
                            <div class="form-group">
                                {{csrf_field()}}
                            </div>
                            <input type="submit" name="btn_submit" class="btn btn-success" value="ثبت">

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection