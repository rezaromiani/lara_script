@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3> ایجاد دسته</h3>
        </div>
    </div>
    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-info">
                <div class="panel-heading">
                    ایجاد دسته
                </div>
                <div class="panel-body">

                    <form method="post" action="">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label>عنوان*</label>
                            <input type="text" name="title" class="form-control" placeholder="عنوان دسته..."
                                   value="{{isset($cat)?$cat->cat_name:''}}">
                            @if(isset($cat))
                                <input type="hidden" name="cat_id" value="{{$cat->id}}">
                            @endif
                        </div>
                        <input type="submit" name="btns" class="btn btn-success center-block" value="ثبت">
                    </form>
                </div>
            </div>
        </div>


    </div>

    <div class="inner" style="min-height: 700px;">
        <div class="row">

        </div>
        <hr/>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel box box-warning">
                    <div class="panel-heading">
                        ویرایش دسته ها
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><input type="checkbox" class="select-all"></th>
                                    <th>عنوان دسته</th>
                                    <th>عملیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($cats))
                                    @foreach($cats as $cat)
                                        <tr>
                                            <th><input type="checkbox" class="checkbox" name="checkbox"
                                                       value="{{$cat->id}}"></th>
                                            <th>{{$cat->cat_name}}</th>
                                            <th>
                                                <a href="{{route('admin.categories.delete',$cat->id)}}"
                                                   class="label label-danger">حدف</a>
                                                <a href="{{route('admin.categories.index',$cat->id)}}"
                                                   class="label label-success">ویرایش</a>
                                            </th>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        دسته بندی موجود نیست.
                                    </tr>
                                @endif
                                </tbody>

                            </table>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection