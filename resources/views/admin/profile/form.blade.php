@extends('layouts.admin')

@section('load_file')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h4 class="page-title">{{$page_title}}</h4>
    </div>
</div>
<hr />
<div class="row">
    <div class="col-lg-8 col-lg-offset-2">
        <div class="panel box box-success">
            @include('admin.partials.sweetAlert')
            @include('admin.partials.error')
            <div class="panel-body">
                <form method="post" action="">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>نام*</label>
                        <input type="text" name="firstName" class="form-control" placeholder="نام..."
                            value="{{old('firstName',isset($user)?$user->first_name:'')}}">
                    </div>
                    <div class="form-group">
                        <label>نام خانوادگی*</label>
                        <input type="text" name="lastName" class="form-control" placeholder="نام خانوادگی..."
                            value="{{old('lastName',isset($user)?$user->last_name:'')}}">
                    </div>
                    <div class="form-group">
                        <label>نام کاربری*</label>
                        <input type="text" name="username" class="form-control" placeholder="نام کاربری..."
                            value="{{old('username',isset($user)?$user->username:'')}}">
                    </div>
                    <div class="form-group">
                        <label>کلمه عبور</label>
                        <input type="password" name="password" class="form-control" placeholder="کلمه عبور...">
                    </div>
                    <div class="form-group">
                        <label>تکرار کلمه عبور</label>
                        <input type="password" name="password_confirmation" class="form-control"
                            placeholder="تکرار کلمه عبور...">
                    </div>
                    <div class="form-group">
                        <label>ایمیل*</label>
                        <input type="email" name="email" class="form-control" placeholder="ایمیل..."
                            value="{{old('email',isset($user)?$user->email:'')}}">
                    </div>
                    <div class="form-group">
                        <label>تکرار ایمیل*</label>
                        <input type="email" name="email_confirmation" class="form-control" placeholder="تکرار ایمیل..."
                            value="{{old('email',isset($user)?$user->email:'')}}">
                    </div>
                    <div class="form-group">
                        <label>شماره موبایل*</label>
                        <input type="text" name="mobile" class="form-control" placeholder="شماره موبایل..."
                            value="{{old('mobile',isset($user)?$user->mobile:'')}}">
                    </div>
                    
                    <div class="form-group">
                        <label for="">رزومه</label>
                        <textarea name="Resumes" id="" cols="30" rows="10" class="form-control">{{old('Resumes',isset($user)?$user->Resumes:'')}}</textarea>
                    </div>
                    <input type="submit" name="btn_submit" class="btn btn-success center-block" value="ثبت">
                </form>
            </div>
        </div>
    </div>
</div>
@endsection