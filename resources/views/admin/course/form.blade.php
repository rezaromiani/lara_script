@extends('layouts.admin')

@section('load_file')
@parent
<link rel="stylesheet" href="/css/admin/dropzone.css">
<script src="/js/admin/dropzone.js"></script>

@endsection
@section('content')
<div class="row">
    <div class="panel-heading">
        ایجاد دوره
    </div>
</div>
<hr />
<script>

</script>
<div class="row">
    <div class="col-lg-12">
        @if(isset($tuts))
        {{var_dump($tuts)}}
        @endif
        <div class="panel box box-warning">
            <div class="panel-heading">
                نوشته جدید
            </div>
            <div class="col-md-12">
                @include('admin.partials.error')
                @include('admin.partials.notifi')
            </div>
            <div class="panel-body">
                <form method="post" action="">
                {{csrf_field()}}
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-xs-6" style="margin-bottom: 20px;">
                                <label>عنوان دوره*</label>
                                <input type="text" name="courseTitle" class="form-control" placeholder="عنوان دوره..."
                                value="{{ old('courseTitle',isset($course)?$course->title : null) }}"
                                >
                            </div>
                            <div class="col-xs-6" style="margin-bottom: 20px;">
                                <label>لینک دمو*</label>
                                <input type="text" name="courseDemo" class="form-control" placeholder="لینک دمو..."
                                value="{{ old('courseDemo',isset($course)?$course->demo_url : null) }}"
                                >
                            </div>
                            <div class="col-xs-6" style="margin-bottom: 20px;">
                                <label>طول دوره*</label>
                                <input type="text" name="courseLength" class="form-control" placeholder="طول دوره..."
                                value="{{ old('courseLength',isset($course)?$course->course_length : null) }}"
                                >
                            </div>
                            <div class="col-xs-6" style="margin-bottom: 20px;">
                                <label>زمان شروع دوره*</label>
                                <input type="text" name="courseStartAt" class="form-control"
                                    placeholder="زمان شروع دوره..." value="{{ old('courseStartAt',isset($course)?$course->start_data : null) }}">
                            </div>
                            <div class="col-xs-6" style="margin-bottom: 20px;">
                                <label>وضعیت دوره*</label>
                                <input type="text" name="courseStatus" class="form-control" placeholder="وضعیت دوره..." value="{{ old('courseStatus',isset($course)?$course->status : null) }}">
                            </div>
                            <div class="col-xs-6" style="margin-bottom: 20px;">
                                <label>قیمت دوره*</label>
                                <input type="text" name="coursePrice" class="form-control" placeholder="قیمت دوره..." value="{{ old('coursePrice',isset($course)?$course->price : null) }}">
                            </div>
                            <div class="col-xs-6" style="margin-bottom: 20px;">
                                <label>تخفیف میزان*</label>
                                <input type="text"  name="courseDiscount" class="form-control"
                                    placeholder="میزان تخفیف..." value="{{ old('courseDiscount',isset($course)? $course->discount : 0) }}">
                            </div>
                            <div class="col-xs-6" style="margin-bottom: 20px;">
                                <label>تصویر دوره*</label>
                                <div class="input-group">
                                    <input type="text" id="file_url" name="course_image" class="form-control" placeholder="تصویر دوره" value="{{ old('course_image',isset($course)?$course->image : null) }}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default file-manager-modal-btn" data-des="#file_url" data-toggle="modal" data-target="#myModal" type="button">انتخاب تصویر</button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-6" style="margin-bottom: 20px;">
                                <label for="teacher">مدرس دوره*</label>
                                <select name="teacher" id="teacher" class="form-control">
                                    @if($users)
                                    @foreach($users as $u)
                                    <option value="{{ $u->id }}">{{ $u->FullName() }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-xs-6" style="margin-bottom: 20px;">
                                <label>نامک*</label>
                                <input type="text"  name="slug" class="form-control"
                                       placeholder="لطفا کلمات رابا - از هم جدا کنید" value="{{ old('slug',isset($course)? $course->slug : null) }}">
                            </div>
                            <div class="col-xs-6" style="margin-bottom: 20px;">
                                <label>پیش نیاز های دوره*</label>
                                <textarea rows="8" name="coursePre" class="form-control"
                                    placeholder="پیش نیازهای دوره...">{{ old('coursePre',isset($course)?$course->prerequisites : null)  }}</textarea>
                            </div>
                            <div class="col-xs-12" style="margin-bottom: 20px;">
                                <label>سرفصل های دوره*</label>
                                <textarea name="courseReference" class="form-control ckeditor" style="direction: rtl;"
                                    placeholder="سرفصل ها ...">{{ old('courseReference',isset($course)?$course->Headlines : null)  }}</textarea>
                            </div>
                        </div>
                        <br>

                    </div>
                    <input type="submit" name="createCourse" class="btn btn-success center-block" value="ثبت">
                </form>
            </div>
        </div>
    </div>
    @include('admin.uploader.upload')

</div>
<!-- Modal -->

@endsection