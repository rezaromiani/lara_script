@extends('layouts.admin')


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3>دروه ها</h3>
        </div>
    </div>
    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-info">
                <div class="panel-heading">
                    <ul class="list-inline user-page-admin" style="position: relative">
                        <li>
                            <form class="navbar-form" method="get" action="post_search.php">
                                <div class="form-group">
                                    <input type="text" name="txt_search" class="form-control" placeholder="جستجو...">
                                </div>
                                <button type="submit" name="search" class="btn btn-info">بگرد</button>
                            </form>
                        </li>

                        <form method="post" action="{{ route('admin.course.delete.many') }}">
                            {{csrf_field()}}
                            <li style="float: left">
                                <button class="label label-danger" type="submit" name="del-all"
                                        style="position: absolute;top: 0px;left: 45px;">حذف کلی
                                </button>
                            </li>
                    </ul>

                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="select-all"></th>
                                <th>عنوان</th>
                                <th>مدرس</th>
                                <th>تعداد دانشجو</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($courses && count($courses)>0)
                                @foreach($courses as $p)
                                    <tr style="height: 100px">
                                        <td><input type="checkbox" class="checkbox" name="checkbox[]"
                                                   value="{{$p->id}}"></td>
                                        <td>{{$p->title}}</td>
                                        <td>{{$p->Teacher->username}}</td>
                                        <td>{{$p->Students->count()}}</td>
                                        <td>
                                            <a href="{{route('admin.course.delete.one',$p->id)}}"
                                               onclick="return confirm('آیا می خواهید این متلب را حذف کنید.')"
                                               class="label label-danger">حدف</a>
                                            <a href="{{route('admin.course.edit',$p->id)}}" class="label label-warning">ویرایش</a>
                                            <a href="{{route('admin.course.student',$p->id)}}" class="label label-info">مدیریت
                                                دانشجو ها</a>
                                            <a href="{{route('admin.course.sessions',$p->id)}}"
                                               class="label label-success">جلسات دوره</a>

                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    هیچ دوره ای ثبت نشده.
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12" style=" text-align: center;">
                @if(isset($courses) &&$courses)
                    {{$courses->links()}}
                @endif
            </div>
        </div>
    </div>
    </div></div>
@endsection