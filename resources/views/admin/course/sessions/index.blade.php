@extends('layouts.admin')


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3>جلسه های دوره - {{ $course->title }}</h3>
        </div>
    </div>
    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-info">
                <div class="panel-heading">
                    <ul class="list-inline user-page-admin" style="position: relative">
                        <li>
                            <form class="navbar-form" method="get" action="post_search.php">
                                <div class="form-group">
                                    <input type="text" name="txt_search" class="form-control" placeholder="جستجو...">
                                </div>
                                <button type="submit" name="search" class="btn btn-info">بگرد</button>
                            </form>
                        </li>
                        <li>
                            <a href="{{ route('admin.course.sessions.create',$course->id) }}" class="btn btn-success">افزودن قسمت جدید</a>
                        </li>
                        <form method="post" action="{{ route('admin.course.sessions.delete.all') }}">
                            {{csrf_field()}}
                            <li style="float: left">
                                <button class="label label-danger" type="submit" name="del-all"
                                        style="position: absolute;top: 0px;left: 45px;">حذف کلی
                                </button>
                            </li>
                    </ul>

                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="select-all"></th>
                                <th>عنوان</th>
                                <th><i class="fa icon-comments"></i></th>
                                <th>تاریخ</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($sessions && count($sessions)>0)
                                @foreach($sessions as $p)
                                    <tr style="height: 100px">
                                        <th><input type="checkbox" class="checkbox" name="checkbox[]" value="{{$p->id}}"></th>
                                        <th>{{$p->title}}</th>
                                        <th>23</th>
                                        <th>{{\Morilog\Jalali\Jalalian::forge($p->created_at)->format('%B %d، %Y')}}</th>
                                        <th>
                                            <a href="{{route('admin.course.sessions.delete',$p->id)}}" onclick="return confirm('آیا می خواهید این متلب را حذف کنید.')" class="label label-danger">حدف</a>
                                            <a href="{{route('admin.course.sessions.edit',[$course->id,$p->id])}}" class="label label-success">ویرایش</a>
                                        </th>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">جلسه ای برای این دوره ثبت نشده است.</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12" style=" text-align: center;">
                @if(isset($sessions) &&$sessions)
                    {{$sessions->links()}}
                @endif
            </div>
        </div>
    </div></div>
@endsection