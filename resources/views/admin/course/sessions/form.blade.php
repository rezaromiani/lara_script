@extends('layouts.admin')

@section('load_file')
    @parent
    <link rel="stylesheet" href="/css/admin/dropzone.css">
    <script src="/js/admin/dropzone.js"></script>

@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3>افزودن جسله دوره - {{ $course->title }}</h3>
        </div>
    </div>
    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-warning">
                <div class="panel-heading">
                    نوشته جدید
                </div>
                <div class="col-md-12">
                    @include('admin.partials.error')
                </div>
                <div class="panel-body">
                    <form method="post" action="" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" name="title" class="form-control"
                                       placeholder="عنوان محصول را اینجا وارد نمایید..."
                                       value="{{old('title',isset($post)?$post['title']:'')}}">
                            </div>
                            <div class="from-group" style="margin-bottom: 10px;">
                                <button type="button" id="file_upload_btn_general" class="btn btn-default"
                                        data-toggle="modal"
                                        data-target="#myModal">آپلود فایل
                                </button>
                            </div>
                            <div class="form-group">
                                        <textarea name="detail" class="form-control ckeditor" style="direction: rtl"
                                                  placeholder="محتوای مورد نظر">{{old('detail',isset($post)?$post['content']:'')}}</textarea>
                            </div>
                            <br>
                            <div class="panel box box-info">
                                <div class="panel-heading">نامک :</div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <input type="text" name="slug" class="form-control"
                                               placeholder="نامک را با - جدا کنید"
                                               value="{{old('slug',isset($post)?$post['slug']:'')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-info">
                                <div class="panel-heading">لینک ویدئو جلسه :</div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" id="video_url" name="course_video" class="form-control" placeholder="ویدئو جلسه" value="{{old('course_video',isset($post)?$post['video_url']:'')}}">
                                            <span class="input-group-btn">
                                        <button class="btn btn-default file-manager-modal-btn" data-des="#video_url" data-toggle="modal" data-target="#myModal" type="button">انتخاب ویدئو</button>
                                    </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="">زمان ویدئو</label>
                                        <input type="text" class="form-control" name="video_length" placeholder="زمان ویدیو به صورت عدد مثلا 20" id=" " value="{{old('video_length',isset($post)?$post['video_time']:'')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-info">
                                <div class="panel-heading">لینک فایل جلسه :</div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" id="file_name" name="course_file" class="form-control" placeholder="فایل جلسه" value="{{old('course_file',isset($post)?$post['file_url']:'')}}">
                                            <span class="input-group-btn">
                                            <button class="btn btn-default file-manager-modal-btn" data-tp="name" data-des="#file_name" data-toggle="modal" data-target="#myModal" type="button">انتخاب فایل</button>
                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="panel box box-info">
                                <div class="panel-heading">انتشار</div>
                                <div class="panel-body" style="max-height: 200px; overflow: auto;">
                                    <input type="submit" name="preview" class="btn btn-info btn-sm"
                                           style="float:left" value="پیش نمایش">
                                    <br>
                                    <br>
                                    <br><input type="hidden" name="session_id"
                                               value="{{old('session_id',isset($post)?$post['id']:0)}}">
                                    <span><i class="icon-calendar"></i> تاریخ انتشار :   </span>
                                    <br>
                                    @if(isset($post))
                                        {{$post['created_at']}}
                                    @endif
                                </div>
                                <div class="panel-footer">

                                    @if(isset($post) && intval($post['id']))
                                        <input type="submit" name="delete" class="btn btn-warning btn-sm"
                                               value="حذف" onclick="return confirm('می خواهید این پست را حذف کنید.')">

                                        <input type="submit" name="publish" class="btn btn-success btn-sm"
                                               value="انتشار">
                                    @endif
                                    @if(isset($post['status']) && intval($post['status']) )
                                    @else
                                        <input type="submit" name="pending" class="btn btn-success btn-sm"
                                               value="ذخیره پیش نویس">

                                    @endif
                                </div>
                            </div>
                            <div class="panel box box-info">
                                <div class="panel-heading">تصویر شاخص</div>
                                <div class="panel-body">
                                    <input type="text" class="form-control" name="file" id="post-img-input"
                                           value="{{old('file',isset($post)?$post['thumb']:'')}}">
                                </div>
                                <div class="panel-footer" style="    padding: 10px 11px;">

                                    <img src="{{isset($post['thumb']) ? asset('storage/upload/image/'.$post['thumb']):''}}"
                                         alt="" class="img-responsive img-thumbnail {{isset($post['thumb']) ? '':'dn'}}"
                                         id="post-img">


                                    <button type="button" id="img-thumb-modal-btn" class="btn btn-default"
                                            data-toggle="modal"
                                            data-target="#myModal">انتخاب تصویر
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
        @include('admin.uploader.upload')
    </div>
    <!-- Modal -->

@endsection