@extends('layouts.admin')

@section('load_file')
@parent
<link rel="stylesheet" href="/css/admin/dropzone.css">
<script src="/js/admin/dropzone.js"></script>

@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h3>دروه {{ $course->title }}</h3>
    </div>
</div>
<hr />

<div class="row">
    <div class="col-lg-12">
        <div class="panel box box-info">
            <div class="panel-heading">
                اضافه کردن دانشجو
            </div>
            <div class="panel-body">
                <form method="post" action="{{ route('admin.course.student',$course->id) }}">
                    <div class="form-group ">
                        <div class="row">
                        {{csrf_field()}}
                            <div class="col-xs-3">
                                <label>دانشجو *</label>
                                <select name="userID[]" class="form-control" multiple="multiple">
                                    @if(isset($Users))
                                    @foreach($Users as $u)
                                    <option value="{{ $u->id }}">{{ $u->FullName().' ('.$u->email.')' }}</option>
                                    @endforeach
                                    @else
                                    <option>کاربری یافت نشد.</option>
                                    @endif
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <label> </label>
                                <input name="btn-add-user" style="margin-top: 4px;" type="submit"
                                    class="form-control btn btn-success" value="ثبت">
                            </div>
                        </div>

                </form>

            </div>
        </div>
    </div>
</div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel box box-info">
            <div class="panel-heading">
                <ul class="list-inline user-page-admin" style="position: relative">
                    <li>
                        <form class="navbar-form" method="get" action="post_search.php">
                            <div class="form-group">
                                <input type="text" name="txt_search" class="form-control" placeholder="جستجو...">
                            </div>
                            <button type="submit" name="search" class="btn btn-info">بگرد</button>
                        </form>
                    </li>

                    <li style="float: left">
                        <button href="#" class="label label-info">چاپ</button>
                    </li>
                    <form method="post" action="{{ route('admin.course.student.delete.many',$course->id) }}">
                        {{csrf_field()}}
                        <li style="float: left">
                            <button class="label label-danger" type="submit" name="del-all"
                                style="position: absolute;top: 0px;left: 45px;">حذف کلی
                            </button>
                        </li>
                </ul>

            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th><input type="checkbox" class="select-all"></th>
                                <th>نام و نام خانوادگی</th>
                                <th>آی دی کاربر</th>
                                <th>ایمیل</th>
                                <th>تاریخ ثبت نام</th>
                                <th>عملیات</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($students) && count($students)>0)
                            @foreach($students as $p)
                            <tr style="height: 100px">
                                <th><input type="checkbox" class="checkbox" name="checkbox[]" value="{{$p->id}}"></th>
                                <th>{{$p->fullName()}}</th>
                                <th>{{$p->id}}</th>
                                <th>{{$p->email}}</th>
                                <th>{{ $p->pivot->created_at }}</th>
                                <th>
                                    <a href="{{route('admin.course.student.delete.one',[$course->id,$p->id])}}"
                                        onclick="return confirm('آیا می خواهید این کاربر را از این دوره  حذف کنید.')"
                                        class="label label-danger">حدف</a>
                                </th>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                هیچ دوره ای ثبت نشده.
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12" style=" text-align: center;">
            @if(isset($students) &&$students)
                {{$students->links()}}
            @endif
        </div>
    </div>
</div>
</div>
@endsection