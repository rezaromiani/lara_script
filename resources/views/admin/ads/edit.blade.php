@extends('layouts.admin')
@section('load_file')
    @parent
    <link rel="stylesheet" href="/css/admin/dropzone.css">
    <script src="/js/admin/dropzone.js"></script>

@endsection
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h3> ویرایش تبلیغ</h3>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-info">
                <div class="panel-heading">
                    افزودن تبلیغ جدید
                </div>
                <div class="panel-body">
                    @include('admin.ads.form')
                </div>
            </div>
        </div>
        @include('admin.uploader.upload')
    </div>
@endsection