<form method="post" action="">
    {{csrf_field()}}
    <div class="container-fluid">
        <div class="row">                @include('admin.partials.error')

        @if(session('success'))
                <div class="alert alert-success">
                    <p>تبلیغ با موفقیت ثبت شد.</p>
                </div>

            @endif
            @if(session('error'))
                <div class="alert alert-warning">
                    <p>خظایی هنگام ثبت پیش آمده لطفا بعدا تلاش کنید.</p>
                </div>
            @endif
            <div class="col-md-6">
                <div class="form-group">
                    <label>عنوان*</label>
                    <input type="text" name="title" class="form-control"
                           placeholder="عنوان تبلیغ..." value="{{isset($aid)?$aid->title:''}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>لینک*</label>
                    <input type="url" name="href" class="form-control" placeholder="لینک..." value="{{isset($aid)?$aid->href:''}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>تاریخ شروع*</label>
                    <input type="text" name="start_date" class="form-control date_picker"
                           placeholder="تاریخ شروع..." autocomplete="off" value="{{isset($aid)?\Morilog\Jalali\Jalalian::forge($aid->start_date)->format('Y/m/d'):''}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>تاریخ خاتمه*</label>
                    <input type="text" name="expire_date" class="form-control date_picker"
                           placeholder="تاریخ خاتمه..." autocomplete="off" value="{{isset($aid)?\Morilog\Jalali\Jalalian::forge($aid->expire_date)->format('Y/m/d'):''}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>تصویر*</label>
                    <input type="text" name="image" id="file_url" class="form-control"
                           placeholder="تصویر تبلیغ..." value="{{isset($aid)?$aid->img:''}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <br>
                    <button type="button" id="img-thumb-modal-btn" data-des="#file_url" class="btn btn-default file-manager-modal-btn"
                            data-toggle="modal"
                            data-target="#myModal">انتخاب تصویر
                    </button>
                </div>
            </div>
        </div>
    </div>


    <input type="submit" name="ads_submit" class="btn btn-success center-block" value="ثبت">
</form>
