@extends('layouts.admin')
@section('load_file')
    @parent
    <link rel="stylesheet" href="/css/admin/dropzone.css">
    <script src="/js/admin/dropzone.js"></script>

@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3> افزودن تبلیغ</h3>
        </div>
    </div>
    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-info">
                <div class="panel-heading">
                    افزودن تبلیغ جدید
                </div>
                <div class="panel-body">
                    @include('admin.ads.form')
                </div>
            </div>
        </div>
        @include('admin.uploader.upload')

    </div>

    <div class="inner" style="min-height: 700px;">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-title">ویرایش تبلیغات</h4>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel box box-success">
                    <div class="panel-heading">
                        <ul class="list-inline user-page-admin" style="position: relative">
                            <li>لیست تبلیغات ثبت شده</li>

                            <li style="float: left">
                                <button href="#" class="label label-info">چاپ</button>
                            </li>
                            <form method="post">
                                <li style="float: left">
                                    <button class="label label-danger" type="submit" name="del-all"
                                            style="position: absolute;top: 0px;left: 45px;">حذف کلی
                                    </button>
                                </li>
                        </ul>

                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><input type="checkbox" class="select-all"></th>
                                    <th>عنوان تبلیغ</th>
                                    <th>لینک تبلیغ</th>
                                    <th>تاریخ شروع</th>
                                    <th>تاریخ خاتمه</th>
                                    <th>عملیات</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if($ads)
                                    @foreach($ads as $a)
                                        <tr>
                                            <td><input type="checkbox" class="checkbox" name="item[]" value="{{$a->id}}"></td>
                                            <td>{{$a->title}}</td>
                                            <td><a href="{{$a->href}}">{{$a->href}}</a></td>
                                            <td>{{\Morilog\Jalali\Jalalian::forge($a->start_date)->format('Y/m/d')}}</td>
                                            <td>{{\Morilog\Jalali\Jalalian::forge($a->expire_date)->format('Y/m/d')}}</td>
                                            <td><a href="{{route('admin.ads.edit',$a->id)}}"
                                                   class="label label-success">ویرایش</a>
                                                <a href="{{route('admin.ads.del',$a->id)}}" onclick="return confirm('آیا می خواهید این تبلیغ را حذف کنید.')" class="label label-danger">حدف</a>

                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        هیچ تلبیغی وجود ندارد.
                                    </tr>
                                @endif
                                </tbody>

                            </table>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection