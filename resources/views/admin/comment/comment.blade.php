@extends('layouts.admin')

@section('load_file')
    @parent
    <link rel="stylesheet" href="/css/admin/dropzone.css">
    <script src="/js/admin/dropzone.js"></script>
    <script src="/js/admin/admin-custom.js"></script>

@endsection
@section('content')
    <div class="comment-status-loading">
        <p>لطفا صبر کنید...</p>
    </div>
    @if(session('success'))
        <div class="alert alert-warning">
            <p>{{session('success')}}</p>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <h3> نظرات کاربران</h3>
        </div>
    </div>
    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-danger">
                <div class="panel-heading">
                    <ul class="list-inline user-page-admin" style="position: relative">
                        <li>
                            <form class="navbar-form" method="get" action="">
                                <div class="form-group">
                                    <input type="text" name="txt_search" class="form-control"
                                           placeholder="جستجو...">
                                </div>
                                <button type="submit" name="search" class="btn btn-info">بگرد</button>
                            </form>
                        </li>
                        <form method="post">
                            <li style="float: left">
                                <button class="label label-danger" type="submit" name="del-all"
                                        style="position: absolute;top: 0px;left: 45px;">حذف کلی
                                </button>
                            </li>
                    </ul>

                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="select-all"></th>
                                <th>نویسنده</th>
                                <th>دیدگاه</th>
                                <th>در پاسخ به</th>
                            </tr>
                            </thead>
                            <tbody>


                    </div><!-- /.modal -->
                    @if($Comment)
                        @foreach( $Comment as $c)
                            <tr class="comment-row-show {{($c->comment_status==0)?'new-comment':''}}">
                                <th style="width: 1%"><input type="checkbox" class="checkbox" name="checkbox"
                                                             value="{{$c->id}}">
                                </th>
                                <th style="width: 15%">
                                    <div><img style="border: 3px solid #eee" class="img-circle"
                                              src="{{\App\Utility\Users::getUserAvatarUrl($c->User->email)}}"
                                              width="64" height="64"></div>
                                    <div>{{$c->User->username}}</div>
                                    <div><a href="mail:{{$c->User->email}}">{{$c->User->email}}</a></div>
                                </th>
                                <th>
                                    <div style="font-weight: 100; font-size: 13px;">
                                        {{\Morilog\Jalali\Jalalian::forge($c->created_at)->format('y/m/d  H:i:s')}}
                                    </div>
                                    <br>
                                    <div style="font-weight: normal">
                                        {!! $c->comment !!}
                                    </div>
                                    <div id="accordion-{{$c->id}}" role="tablist" aria-multiselectable="true">
                                        <br>
                                        <br>

                                        <div class="comment-operation">
                                            <a href="{{route('admin.comment.delete',$c->id)}}"
                                               class="label label-danger">حدف</a>
                                            <a class="label label-info collapsed" data-toggle="collapse"
                                               href="#reply-{{$c->id}}" data-parent="#accordion-{{$c->id}}"
                                               aria-expanded="false"
                                               aria-controls="collapseExample">پاسخ
                                                دادن</a>
                                            <a class="label label-primary collapsed" data-toggle="collapse"
                                               href="#edit-{{$c->id}}" data-parent="#accordion-{{$c->id}}"
                                               aria-expanded="false"
                                               aria-controls="collapseExample">ویرایش</a>
                                            @if($c->comment_status==0)
                                                <a href="#" class="label label-success comment-status"
                                                   data-status="publish"
                                                   data-id="{{$c->id}}">پدیرفتن</a>
                                            @else
                                                <a href="#" class="label label-warning comment-status"
                                                   data-status="pending"
                                                   data-id="{{$c->id}}"> نپدیرفتن</a>
                                            @endif
                                        </div>
                                        <div class="collapse" id="edit-{{$c->id}}">
                                            <form action="" method="post">
                                                {{csrf_field()}}
                                                <br>
                                                <div class="form-group">
                                                            <textarea name="comment-edit-text" class="form-control"
                                                                      id="comment-edit-text" cols="30"
                                                                      rows="10">{{$c->comment}}</textarea>
                                                    <input type="hidden" name="comment-edit-id"
                                                           value="{{$c->id}}">
                                                </div>
                                                <div class="form-group text-left">
                                                    <button class="btn btn-success" type="submit"
                                                            name="edit-post">ثبت
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="collapse" id="reply-{{$c->id}}">
                                            <form action="{{route('admin.comment.reply')}}" method="post">
                                                {{csrf_field()}}
                                                <br>
                                                <p>پاسخ به :</p>
                                                <div class="alert alert-info">
                                                    <p>{{$c->comment}}</p>
                                                </div>
                                                <div class="form-group">
                                                            <textarea name="text" class="ckeditor"
                                                                      id="comment-edit-text" cols="30"
                                                                      rows="10"></textarea>
                                                    <input type="hidden" name="parent_id"
                                                           value="{{$c->id}}">
                                                    <input type="hidden" name="post_id"
                                                           value="{{$c->commentable->id}}">
                                                    <input type="hidden" name="type"  value="{{$c->commentable_type}}">
                                                </div>
                                                <div class="form-group text-left">
                                                    <button class="btn btn-success" type="submit"
                                                            name="edit-post">پاسخ
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                </th>
                                <th style="width: 15%">


                                    <div>{{$c->commentable->title}}</div>

                                    <div style="text-align: center"><a
                                                href="{{route('post.single',[$c->commentable->id,$c->commentable->slug])}}"
                                                target="_blank">نمایش نوشته</a></div>
                                    <div class="badge " style="float: left">{{$c->commentable->Comment->count()}}</div>
                                </th>
                            </tr>
                        @endforeach
                    @endif
                </div>

                <script>
                    jQuery(document).ready(function ($) {
                        $(document).on('click', '.comment-status', function (ev) {
                            ev.preventDefault();
                            var $this = $(this);
                            var id = $(this).data('id');
                            var status = $(this).data('status');
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });
                            $.ajax({
                                url:{!! json_encode(route('admin.comment.status')) !!},
                                method: 'post',
                                data: {id: id, status: status},
                                beforeSend: function () {
                                    $('.comment-status-loading').fadeIn();
                                },
                                complete: function () {
                                    $('.comment-status-loading').fadeOut();
                                },
                                success: function (data) {
                                    if (data.success) {
                                        swal({
                                            title: "ممنون از شما!",
                                            text: 'تغییرات با موفقیت انجام شد.',
                                            icon: "success",
                                            button: "تایید",
                                        }).then((data) => {
                                            location.reload();
                                        });

                                    }
                                },
                                error: function (data) {
                                    console.log(data)
                                }
                            })
                        })
                    })
                </script>
                </tbody>

                </table>
            </div>
        </div>
        </form>
    </div>

    <!--pagination-->
    <div class="container">
        <div class="row">
            <div class="col-md-12" style=" text-align: center;">
                {{$Comment->links()}}
            </div>
        </div>
    </div>


@endsection