@extends('layouts.admin')

@section('load_file')
    @parent
    <link rel="stylesheet" href="/css/admin/dropzone.css">
    <script src="/js/admin/dropzone.js"></script>

@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title">مدیریت تصاویر اسلایدر</h4>
        </div>
    </div>
    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-info">
                <div class="panel-heading">
                    مدیریت اسلایدر
                </div>
                <div class="panel-body">
                    @include('admin.partials.error')
                    <p class="tab-pane" id="profile-user-all-tickets">


                    <form method="post" action="">
                        {{csrf_field()}}
                        <div class="col-xs-5">
                            <div class="input-group">
                                <input type="text" id="file_url" name="slide" class="form-control"
                                       placeholder="تصویر اسلایدر"
                                       value="{{ old('slide',isset($slider)?$slider->img : null) }}">
                                <span class="input-group-btn">
                                        <button class="btn btn-default file-manager-modal-btn" data-des="#file_url"
                                                data-toggle="modal" data-target="#myModal"
                                                type="button">انتخاب تصویر</button>
                                    </span>
                            </div>
                            <br>
                        </div>
                        <div class="col-xs-5">
                            <input type="text" class="form-control" name="slideUrl" placeholder="آدرس اجاع..."
                                   value="{{ old('sliderUrl',isset($slider)?$slider->url : null) }}">
                        </div>
                        <div class="col-xs-2">
                            <input type="submit" class="form-control btn btn-success" name="btn-submit-slider"
                                   value="ثبت">
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-success">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>تصویر اسلایدر</th>
                                <th>آدرس</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if(isset($slides) && $slides)
                                    @foreach($slides as $s)
                                        <tr>
                                            <td><img src="{{$s->img}}" class="img-responsive" width="200" alt=""></td>
                                            <td><a href="{{$s->url}}">لینک</a></td>
                                            <td>
                                                <a href="{{route('admin.slider.del',$s->id)}}" onclick="return confirm('آیا می خواهید این اسلاید را حذف کنید.')" class="label label-danger">حدف</a>
                                                <a href="{{route('admin.slider.edit',$s->id)}}" class="label label-warning">ویرایش</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
    @include('admin.uploader.upload')

@endsection
