<div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">آپلود</h4>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation"><a href="#home" aria-controls="home" role="tab"
                                               data-toggle="tab">آپلود فایل</a></li>
                    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab"
                                                              data-toggle="tab">تصاویر</a></li>
                    <li role="presentation"><a href="#video" aria-controls="profile" role="tab"
                                               data-toggle="tab">ویدیو</a></li>
                    <li role="presentation"><a href="#file" aria-controls="profile" role="tab"
                                               data-toggle="tab">فایل</a></li>

                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane " id="home">
                        <form id="my-awesome-dropzone" action="{{route('admin.file.upload')}}" class="dropzone">
                            <div class="fallback">
                                <input name="file" type="file" multiple/>
                            </div>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane active" id="profile">
                        <div class="row">
                            <div class="col-md-9" id="profile-content">
                                @if(count($image)>0)
                                    @foreach($image as $file)
                                        <img src="{{asset('storage/upload').'/'.$file}}"
                                             class="img-responsive img-thumbnail img-upload" alt="">
                                    @endforeach
                                @endif
                            </div>
                            <div class="col-md-3">
                                <form action="">
                                    <div class="form-group">
                                        <label for="fileUrl">آدرس فایل:</label>
                                        <input type="text" class="form-control" id="imgUrl" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">نام فایل</label>
                                        <input type="text" class="form-control" id="imgname" placeholder="">
                                    </div>

                                    <div class="form-group">
                                        <button class="btn btn-success" id="btn-thumbnile-add">انتخاب</button>
                                        <button class="btn btn-danger" id="btn-thumbnile-delete">حذف</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="video">
                        <div class="row">
                            <div class="col-md-9" id="video-content">
                                @if(count($video)>0)
                                    @foreach($video as $file)
                                        <div class="fn-file-container fn-file-container-video"
                                             data-src="{{asset('storage/upload').'/'.$file}}">
                                            <img src="/img/video.png"
                                                 class="img-responsive" alt="">
                                            <span>{{basename($file)}}</span>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="col-md-3">
                                <form action="">
                                    <div class="form-group">
                                        <label for="fileUrl">آدرس فایل:</label>
                                        <input type="text" class="form-control" id="videoUrl" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">نام فایل</label>
                                        <input type="text" class="form-control" id="videoname" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-success" id="btn-video-add">انتخاب</button>
                                        <button class="btn btn-danger" id="btn-video-delete">حذف</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane " id="file">
                        <div class="row">
                            <div class="col-md-9" id="file-content">
                                @if(count($files)>0)
                                    @foreach($files as $file)
                                        <div class="fn-file-container fn-file-container-file"
                                             data-src="{{asset('storage/upload').'/'.$file}}">
                                            @if(App\Utility\CommonUtility::getFileType($file) == 'zip')
                                                <img src="/img/zip.jpg" class="img-responsive" alt="">
                                            @else
                                                <img src="/img/pdf.jpg" class="img-responsive" alt="">
                                            @endif
                                            <span>{{basename($file)}}</span>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="col-md-3">
                                <form action="">
                                    <div class="form-group">
                                        <label for="fileUrl">آدرس فایل:</label>
                                        <input type="text" class="form-control" id="fileUrl" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">نام فایل</label>
                                        <input type="text" class="form-control" id="filename" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-success" id="btn-file-add">انتخاب</button>
                                        <button class="btn btn-danger" id="btn-file-delete">حذف</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('click', '.file-manager-modal-btn', function () {
        var $this = $(this);
        sessionStorage.setItem('file_manager_des', $this.data('des'));
    });
    $('#myModal').on('hidden.bs.modal', function (e) {
        sessionStorage.removeItem("file_manager_des")
    });
    $(document).on('click', '#btn-thumbnile-add', function (ev) {
        ev.preventDefault();
        var $fileurl = $('#imgUrl').val();
        var $filename = $('#imgname').val();
        if (!$fileurl || !$filename) {
            alert('لطفا یک تصویر اتخاب کنید.');
            return;
        }
        var des = sessionStorage.getItem("file_manager_des");
        if (des == null) {
            $('#post-img').attr('src', $fileurl).fadeIn();
            $('#post-img-input').val($filename);
        } else {
            $(des).val($fileurl);
        }
        $('#myModal').modal('hide');
        sessionStorage.removeItem("file_manager_des")
    });
    Dropzone.options.myAwesomeDropzone = {
        init: function () {
            this.on("success", function (file, response) {
                $('#profile-content').html(response.img);
                $('#video-content').html(response.video);
                $('#file-content').html(response.file);
            });
            this.on("error", function (file, response) {
                console.log(response);
                $(file.previewElement).find('.dz-error-message').text(response.file);
            });
        },
        addRemoveLinks: true,
        dictDefaultMessage: 'برای آپلود فایل را اینجا رها کنید.',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    };
    $(document).on('click', '.img-upload', function () {
        $('.img-upload').removeClass('active');
        var $this = $(this);
        var $src = $this.attr('src');
        var index = $src.lastIndexOf('/');
        $this.addClass('active');
        $('#imgUrl').val($src);
        $('#imgname').val($src.substr(index + 1));
    });

    function handelFileVideoItemClick($this, classname, urlinput, nameinput) {
        $('.' + classname).removeClass('active');

        var $src = $this.data('src');
        var index = $src.lastIndexOf('/');
        $this.addClass('active');
        $(urlinput).val($src);
        $(nameinput).val($src.substr(index + 1));
    }

    function handelFileVideoAddItemClick(urlinput, nameinput) {
        var $fileurl = $(urlinput).val();
        var $filename = $(nameinput).val();
        if (!$fileurl || !$filename) {
            alert('لطفا یک تصویر اتخاب کنید.');
            return;
        }
        var des = sessionStorage.getItem("file_manager_des");
        if (des == null) {
            alert('لطفا یک تصویر اتخاب کنید.');
        } else {
            if (des == '#file_name') {
                $(des).val($filename);
            } else {
                $(des).val($fileurl);
            }
            $('#myModal').modal('hide');
        }

        sessionStorage.removeItem("file_manager_des")
    }

    function handelDelete(input, type) {
        const fileEl = $('#' + input);
        let val = fileEl.val();
        if (val.length > 0) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: {!! json_encode(route('admin.file.delete')) !!},
                method: 'post',
                data: {
                    file: type + '/' + val
                },
                success: function (data) {
                    if (data.success){
                        switch (type) {
                            case 'image':
                                $('#profile-content .active').remove();
                                break;
                            case 'video':
                                $('#video-content .active').remove();
                                break;
                            case 'file':
                                $('#file-content .active').remove();
                                break;
                        }
                    }
                },
                error: function () {

                }
            })
        }
    }

    $(document).on('click', '.fn-file-container-video', function () {
        var $this = $(this);

        handelFileVideoItemClick($this, 'fn-file-container-video', '#videoUrl', '#videoname')
    });
    $(document).on('click', '.fn-file-container-file', function () {
        var $this = $(this);

        handelFileVideoItemClick($this, 'fn-file-container-file', '#fileUrl', '#filename')
    });
    $(document).on('click', '#btn-video-add', function (ev) {
        ev.preventDefault();
        handelFileVideoAddItemClick('#videoUrl', '#videoname')
    });
    $(document).on('click', '#btn-file-add', function (ev) {
        ev.preventDefault();
        handelFileVideoAddItemClick('#fileUrl', '#filename')
    });
    $(document).on('click', '#btn-file-delete', function (ev) {
        ev.preventDefault();
        handelDelete('filename', 'file')
    });
    $(document).on('click', '#btn-video-delete', function (ev) {
        ev.preventDefault();
        handelDelete('videoname', 'video')
    });
    $(document).on('click', '#btn-thumbnile-delete', function (ev) {
        ev.preventDefault();
        handelDelete('imgname', 'image')
    });
</script>
