@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1> افزودن نظرسنجی </h1>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-info">
                <div class="panel-heading">
                    ایجاد فرم نظرسنجی
                </div>

                <div class="panel-body">

                    <form method="post" action="">

                        <div class="form-group ">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-XS-12">
                                    <label>عنوان نظرسنجی*</label>
                                    <input type="text" name="question" class="form-control"
                                           placeholder="عنوان نظرسنجی...">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row">
                                <div class="col-md-4 input_fields_wrap">
                                    <button class="btn btn-info add_field_button">اضافه کردن فیلد برای سواات
                                        نظرسنجی
                                    </button>
                                    <br><br>
                                    <label class="poll-item-label">آیتم های نظرسنجی*</label>
                                    <br>
                                    <div><input type="text" class="form-control" name="item[]"
                                                placeholder="آیتم نظرسنجی..."></div>
                                    <br>

                                </div>

                                <div class="col-sm-12"></div>
                                <div class="col-md-4">
                                    <hr>
                                    <div class="form-group">
                                        <label for="day" class="poll-item-label">تعداد روز فعال بودن نظر سنجی</label>
                                        <input type="number" id="day" name="day" class="form-control" value="1">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <input type="submit" name="btn-poll" class="btn btn-success center-block" value="ثبت">
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel box box-success">
                    <div class="panel-heading">
                        <ul class="list-inline user-page-admin" style="position: relative">
                            <li>ویرایش نظرسنجی</li>
                        </ul>

                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            @include('admin.poll.pollstbl')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection