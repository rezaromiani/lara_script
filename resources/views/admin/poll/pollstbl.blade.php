<table class="table table-hover">
    <thead>
    <tr>
        <th>عنوان نظرسنجی</th>
        <th>وضعیت</th>
        <th>عملیات</th>
    </tr>
    </thead>
    <tbody>
    @if($polls)
        @foreach($polls as $poll)
            <tr>
                <td>{{$poll->poll_title}}</td>
                <td>
                    <span class="label {{intval($poll->status)?'label-success':'label-danger'}}">{{$poll->pollStatus()}}</sp>
                </td>
                <td>
                    <a href="{{route('admin.poll.delete',$poll->id)}}" onclick="return confirm('آیا می خواهید این نظر سنجی را حذف کنید.')" class="label label-danger">حذف</a>
                    <a href="{{route('admin.poll.edit',$poll->id)}}" class="label label-primary">ویرایش</a>
                </td>
            </tr>
        @endforeach
    @else
        <tr>هیچ نظر سنجی ثبت نشده .</tr>
    @endif
    </tbody>
</table>
