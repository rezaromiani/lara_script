@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1> ویرایش نظر سنجی </h1>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel box box-success">
                    <div class="panel-heading">
                        <ul class="list-inline user-page-admin" style="position: relative">
                            <li>ویرایش نظرسنجی</li>

                        </ul>
                    </div>
                    <div class="panel-body">
                        <form method="post" action="">
                            {{csrf_field()}}
                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-XS-12">
                                        <label>عنوان نظرسنجی*</label>
                                        <input type="text" name="question" class="form-control"
                                               placeholder="عنوان نظرسنجی..." value="{{$poll->poll_title}}">
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-4 input_fields_wrap">
                                        <button class="btn btn-info add_field_button">اضافه کردن فیلد برای سواات
                                            نظرسنجی
                                        </button>
                                        <br><br>
                                        <label class="poll-item-label">آیتم های نظرسنجی*</label>
                                        <br>
                                        @if($pollItem)
                                            @foreach($pollItem as $i)
                                                <div><input type="text" class="form-control" name="olditem[{{$i->id}}]"
                                                            placeholder="آیتم نظرسنجی..." value="{{$i->title}}">
                                                    <a href="#" class="remove_edit_field"
                                                       data-pollid="{{$i->id}}">حذف</a>
                                                </div>
                                                <br>
                                            @endforeach
                                        @else
                                            <div><input type="text" class="form-control" name="item[]"
                                                        placeholder="آیتم نظرسنجی..."></div>
                                            <br>
                                        @endif
                                    </div>

                                </div>
                            </div>
                            <input type="submit" name="btn-poll" class="btn btn-success center-block" value="ذخیره">
                        </form>

                    </div>
                    </form>
                </div>
                <script>
                    jQuery(document).ready(function ($) {
                        $(document).on('click', '.remove_edit_field', function (ev) {
                            ev.preventDefault();
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });
                            var $this = $(this);
                            var item_id = $this.data('pollid');
                            $.ajax({
                                url:{!! json_encode(route('admin.pollitem.ajax')) !!},
                                method: 'post',
                                data: {item_id: item_id, action: 5877},
                                success: function (data) {
                                    if (data.success) {
                                        $this.parent('div').remove();
                                    }
                                },
                                error: function (data) {

                                }
                            })
                        });
                    });
                </script>
            </div>
        </div>
    </div>

@endsection