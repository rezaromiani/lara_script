@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1> داشبورد </h1>
        </div>
    </div>
    <hr/>
    <!--BLOCK SECTION -->
    <div class="row">
        <div class="col-lg-12">
            <div style="text-align: center;">

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="material-icons">supervisor_account</i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">کل کاربران : </span>
                            <span class="info-box-number">{{$statistics['user_count']}} نفر</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="material-icons">description</i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">کل مطالب : </span>
                            <span class="info-box-number">{{$statistics['post_count']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="material-icons">comment</i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">کل نظرات : </span>
                            <span class="info-box-number">{{$statistics['comment_count']}} </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="material-icons">visibility</i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">کل بازدیدها : </span>
                            <span class="info-box-number">{{$statistics['view_count']}} </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

            </div>

        </div>

    </div>
    <!--END BLOCK SECTION -->

    <!-- CHART & CHAT  SECTION -->
    <div class="row">
        <div class="col-lg-12">
            <div class="col-md-3">
                <!-- USERS LIST -->
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title"> میزان فروش</h3>

                        <div class="box-tools pull-right">
                            <a data-toggle="collapse" href="#min-sale"><i class="material-icons">more_horiz</i>
                            </a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div id="min-sale" class="collapse in box-body no-padding alert">
                        <ul class="list-group" style="line-height: 32px">
                            <li><span>فروش امروز :</span><span style="float: left">{{number_format($statistics['today_sale'])}} تومان</span>
                            </li>
                            <li><span>فروش کل :</span><span style="float: left">{{number_format($statistics['total_sale'])}} تومان</span>
                            </li>

                        </ul>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!--/.box -->
            </div>
            <div class="col-md-5">
                <!-- USERS LIST -->
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">اعضای جدید </h3>

                        <div class="box-tools pull-right">

                            <a data-toggle="collapse" href="#min-comments"><i class="material-icons">more_horiz</i>
                            </a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div id="min-comments" class="collapse in box-body no-padding alert " style="padding: 0;
    margin: 6px;;">
                        <div class="row">
                            @if(isset($lsat_users) && $lsat_users)
                                @foreach($lsat_users as $lu)
                                    <div class="col-sm-3 col-md-3 col-lg-3">
                                        <div class="panel panel-default custom-user-panel">
                                            <img src="{{\App\Utility\Users::getUserAvatarUrl($lu->email)}}"
                                                 class="img-circle" alt="User Image" width="64"
                                                 height="64">
                                            <div class="new-user-name">{{$lu->FullName()}}</div>
                                            <div class="users-list-date">{{\Morilog\Jalali\Jalalian::forge($lu->created_at)->format('Y-m-d')}}</div>
                                        </div>

                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!--/.box -->
            </div>
            <div class="col-md-4">
                <!-- USERS LIST -->
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">جدیدترین نظرات کاربران </h3>

                        <div class="box-tools pull-right">

                            <a data-toggle="collapse" href="#min-user-comments"><i class="material-icons">more_horiz</i>
                            </a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div id="min-user-comments" class="collapse in box-body no-padding alert " style="padding: 0;
    margin: 6px;;">
                        <ul class="list-group admin-panel-comments" style="padding: 5px 10px;">
                            @if(isset($last_comment) && $last_comment)
                                @foreach($last_comment as $lc)
                                    <li><img src="{{\App\Utility\Users::getUserAvatarUrl($lc->User->email)}}"
                                             class="img-thumbnail" width="36" height="36"> <a href="#">با
                                            {{\Illuminate\Support\Str::words($lc->comment,9,'...')}}
                                        </a></li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!--/.box -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <!-- USERS LIST -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">نمودار انتشار مطالب</h3>

                    <div class="box-tools pull-right">

                        <a data-toggle="collapse" href="#min-topic-bar"><i class="material-icons">more_horiz</i>
                        </a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div id="min-topic-bar" class="collapse in box-body no-padding alert " style="padding: 0;
    margin-bottom: 0;">
                    <div class="row">

                        <canvas id="buyers" width="960" min-height="325px"></canvas>

                        <script>
                            var ctx = document.getElementById('buyers').getContext('2d');
                            var chart = new Chart(ctx, {
                                // The type of chart we want to create
                                type: 'line',

                                // The data for our dataset
                                data: {
                                    labels: {!! json_encode(array_keys($post_statistics)) !!},
                                    datasets: [{
                                        label: "مطالب منتشر شده",
                                        backgroundColor: 'rgba(255, 99, 132,.2)',
                                        borderColor: 'rgb(255, 99, 132)',
                                        data: {!! json_encode(array_values($post_statistics)) !!},
                                    }]
                                },

                                // Configuration options go here
                                options: {
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero: true,
                                            }
                                        }],
                                    }
                                }
                            });
                        </script>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <!--/.box -->
        </div>
        <div class="col-md-6">
            <!-- USERS LIST -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">نمودار فروش </h3>

                    <div class="box-tools pull-right">

                        <a data-toggle="collapse" href="#sales-chart"><i class="material-icons">more_horiz</i>
                        </a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div id="sales-chart" class="collapse in box-body no-padding alert " style="padding: 0;
    margin-bottom: 0;">
                    <div class="row">

                        <canvas id="sales" width="960" min-height="325px"></canvas>

                        <script>
                            var ctx = document.getElementById('sales').getContext('2d');
                            var chart = new Chart(ctx, {
                                // The type of chart we want to create
                                type: 'line',

                                // The data for our dataset
                                data: {
                                    labels: {!! json_encode(array_keys($transaction_statistics)) !!},
                                    datasets: [{
                                        label: "میزان فروش ( تومان )",
                                        backgroundColor: 'rgba(255, 99, 132,.0)',
                                        borderColor: 'rgb(15, 161, 239)',
                                        data: {!! json_encode(array_values($transaction_statistics)) !!},
                                    }]
                                },

                                // Configuration options go here
                                options: {
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero: true,
                                            }
                                        }],
                                    }
                                }
                            });
                        </script>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <!--/.box -->
        </div>
    </div>

@endsection