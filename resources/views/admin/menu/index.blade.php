@extends('layouts.admin')
@section('load_file')
    @parent
    <script src="/js/admin/jquery.nestable.js"></script>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1> مدیریت منو ها </h1>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-info">
                <div class="panel-heading">ایجاد منوی اصلی
                </div>

                <div class="panel-body">
                    @include('admin.partials.notifi')
                    <form method="post" action="">
                        {{csrf_field()}}
                        <i class="icon-minus"></i>
                        <div class="form-group ">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label>عنوان منو*</label>
                                    <input type="text" name="title" class="form-control"
                                           placeholder="عنوان منو...">
                                </div>
                                <div class="col-xs-4">
                                    <label>لینک منو*</label>
                                    <input type="text" name="href" class="form-control"
                                           placeholder="لینک منو...">
                                </div>
                                <div class="col-xs-2">
                                    <label> </label>
                                    <input name="btn-create-menu" style="margin-top: 4px;" type="submit"
                                           class="form-control btn btn-success" value="ثبت">
                                </div>
                            </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-success">
                <div class="panel-heading">
                    <ul class="list-inline user-page-admin" style="position: relative">
                        <li>ویرایش منو</li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">

                        <div class="col-md-6">
                            <div id="nestable2" class="dd">
                                @if(isset($menus[0]))
                                    @include('admin.menu.list',['items'=>$menus[0]])
                                    @else
                                    <div class="alert alert-warning">
                                        <p>هیچ منویی وجود ندارد.</p>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="list-group">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#nestable2').nestable({
            group: 1
        }).on('change', function (e) {
            var list = e.length ? e : $(e.target), output = list.nestable('serialize');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:{!! json_encode(route('admin.menu.ajax')) !!},
                method: 'post',
                dataType: 'json',
                data: {list: output},
                success: function (data) {

                },
                error: function (data) {

                }
            })
        });

    </script>
@endsection