@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1> داشبورد </h1>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel box box-info">
                <div class="panel-heading">ویرایش منوی
                </div>

                <div class="panel-body">
                    @include('admin.partials.notifi')
                    <form method="post" action="">
                        {{csrf_field()}}
                        <i class="icon-minus"></i>
                        <div class="form-group ">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label>عنوان منو*</label>
                                    <input type="text" name="title" class="form-control"
                                           placeholder="عنوان منو..." value="{{$menu->title}}">
                                </div>
                                <div class="col-xs-4">
                                    <label>لینک منو*</label>
                                    <input type="url" name="href" class="form-control"
                                           placeholder="لینک منو..." value="{{$menu->href}}">
                                </div>
                                <div class="col-xs-2">
                                    <input type="hidden" name="menu_id" value="{{$menu->id}}">
                                    <label> </label>
                                    <input name="btn-create-menu" style="margin-top: 4px;" type="submit"
                                           class="form-control btn btn-success" value="ثبت">
                                </div>
                            </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection