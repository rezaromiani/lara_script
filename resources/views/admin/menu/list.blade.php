<ol class="dd-list">
    @foreach($items as $i)
        <li data-id="{{$i->id}}" class="dd-item dd3-item">
            <div class="dd-handle dd3-handle"></div>
            <a href="{{route('admin.menu.delete',$i->id)}}" class="btn btn-danger" data-id="{{$i->menu}}">
                <i class="icon-trash" aria-hidden="true"></i>
            </a>
            <a href="{{route('admin.menu.edit',$i->id)}}" class="btn btn-edit" data-id="{{$i->menu}}">
                <i class="icon-edit" aria-hidden="true"></i>
            </a>
            <div class="dd3-content">

                {{$i->title}}</div>

        @if(isset($menus[$i->id]))
            @include('admin.menu.list',['items'=>$menus[$i->id]])
        @endif
        </li>
    @endforeach
</ol>