@extends('layouts.payment')
@section('content')
    <div class="col-4">
        @if($res)
        <div class="checkout">
            <div class="checkout-header header-success">تراکنش موفقیت آمیز</div>
            <div class="checkout-body">
                <div class="item-row">
                    پرداخت شما با موفقیت انجام شد.
                </div>
                <div class="item-row">
                    <span>شماره سفارش:</span>
                    {{$order_number}}
                </div>
                <div class="item-row">
                    <span>شماره تراکنش:</span>
                    {{$res}}
                </div>
                <div class="submit-container">
                    <a href="{{route('user.home')}}" class="btn btn-primary">
                        بازگشت به {{config('app.name')}}
                    </a>
                    <a href="{{route('course.single',[$c_id,$c_title])}}" class="btn btn-primary">
                        رفتن به صفحه دوره
                    </a>
                </div>
            </div>
        </div>
            @else
            <div class="checkout">
                <div class="checkout-header header-danger">تراکنش ناموفق</div>
                <div class="checkout-body">
                    <div class="item-row">
                        پرداخت شما ناموفق بود لطفا دوباره تلاش کنید یا با پشتیبانی سایت تماس بگیرید.
                    </div>
                    <div class="submit-container">
                        <a href="{{route('user.home')}}" class="btn btn-primary">
                            بازگشت به {{config('app.name')}}
                        </a>
                    </div>
                </div>
            </div>
        @endif
    </div>

@endsection