@extends('layouts.payment')
@section('content')
    <div class="col-4">
        <div class="checkout">
            <div class="checkout-header">تایید نهایی</div>
            <div class="checkout-body">
                <form method="post" action="">
                    {{csrf_field()}}
                    <input type="hidden" name="course" value="{{$course->id}}" >
                <div class="item-row">
                    <span>نام دوره:</span>
                    {{ $course->title }}
                </div>
                @if($course->discount > 0)
                    <div class="item-row">
                        <span>تخفیف:</span>
                        {{$course->discount}}%
                    </div>
                @endif
                <div class="item-row">
                    <span>قیمت:</span>
                    @if($course->discount > 0)
                        <strike class="main-price"> {{number_format($course->price)}} </strike>
                        <span> {{number_format(\App\Utility\CourseUtility::calcPrice($course))}} </span>
                    @else
                        <span> {{number_format($course->price)}} </span>
                    @endif
                    تومان
                </div>
                <div class="submit-container">
                    <a href="{{route('user.home')}}" class="btn btn-danger">
                        لغو
                    </a>
                    <button type="submit" class="btn btn-success">
                        پرداخت
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>

@endsection