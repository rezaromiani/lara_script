<ul class="{{intval($child)?'dropdown-menu':'nav navbar-nav'}}">
    @foreach($items as $item)
        <li class="{{isset($menus[$item->id])?'dropdown':''}}">
            @if(isset($menus[$item->id]))
                <a href="{{$item->href}}" class="dropdown-toggle" data-toggle="dropdown" role="button"
                   aria-haspopup="true"
                   aria-expanded="false">{{$item->title}}
                    @if($child)
                        <i class="material-icons top-menu-arrow-left">
                            keyboard_arrow_left
                        </i>
                    @else
                        <i class="material-icons top-menu-arrow-down">
                            keyboard_arrow_down
                        </i>
                    @endif

                </a>

                @include('user.partials.top-menu-item',['items'=>$menus[$item->id],'child'=>true])
            @else
                <a href="{{$item->href}}">{{$item->title}}</a>
            @endif
        </li>
    @endforeach
</ul>
