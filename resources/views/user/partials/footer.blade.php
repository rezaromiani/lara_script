<div class="container-fluid padding-reset" id="fooer-section">
    <div class="row" id="custom-footer-row">
        <div class="col-sm-12 col-md-4 col-lg-4 padding-reset">
            <div id="aboutus-footer"><p>درباره ما</p></div>
            <div class="aboutus-text">
                {!! isset($options['footer']['about-us'])?$options['footer']['about-us']:'' !!}
            </div>

        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 padding-reset">
            <div id="sign-footer"><p>نمادها</p></div>
            <ul class="list-inline" id="nemad-style">
                <li><a href="#"><img src="img/b1.png"></a></li>
                <li><a href="#"><img src="img/b3.png"></a></li>
                <li><a href="#"><img src="img/b5.png"></a></li>
            </ul>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 padding-reset">
            <div id="comment-fooer"><p>نظـرات کاربران</p></div>
            <div id="footer-comment-slider" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @if(isset($comments) && !empty($comments))
                        @foreach($comments as $comment)
                            <div class="item {{$loop->first ? 'active': null}}">
                                <img class="img-circle user-avatar margin-center"
                                     src="{{\App\Utility\Users::getUserAvatarUrl($comment->User->email)}}" width="60"
                                     height="60">
                                <p class="slider-random-comment">
                                    {{$comment->comment}}
                                </p>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
    <row>
        <div class="col-sm-12 col-md-12 col-lg-12 padding-reset">
            <p class="copy-right-text"><i class="material-icons">copyright</i>کليه حقوق محصولات و محتوای اين
                سایت متعلق به {{config('app.name')}} می باشد و هر گونه کپی برداری از محتوا و محصولات سایت غیر مجاز و
                بدون رضایت ماست.
            </p>
        </div>
    </row>
</div>
