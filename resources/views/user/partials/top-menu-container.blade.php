<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    @if(isset($menus[0]))
        @include('user.partials.top-menu-item',['items'=>$menus[0],'child'=>false])
    @endif
</div>
<script>
    $(document).ready(function(){
        $('.dropdown > a').on("click", function(e){
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
    });
</script>