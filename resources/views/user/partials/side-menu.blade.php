<div id="slide-nav">
    <div class="user">
        <img class="img-circle user-avatar-side-nav" src="{{ \App\Utility\Users::getUserAvatarUrl(Auth::user()->email) }}" width="100" height="100">
        <div class="welcome">
            <div class="text-center">خوش آمدید : <span>{{ Auth::user()->FullName() }}</span></div>
        </div>
    </div>
    <ul class="list-group" id="slide-list">
        <li><i class="material-icons">dashboard</i><a href="{{ route('admin.dashboard') }}">داشبورد</a></li>
        <li><i class="material-icons">person</i><a href="{{ route('user.account.index') }}">پنل کاربری</a></li>
        <li><i class="material-icons">border_color</i><a href="{{ route('admin.post.create') }}">ارسال مطلب</a></li>
        <li><i class="material-icons">forum</i><a href="{{ route('admin.comment.index') }}">نظرات</a>
            <span class="badge badge-slide-nav">{{\App\Utility\PostUtility::getNewCommentCount()}}</span>
        </li>
        <li><i class="material-icons" id="exit-icon">power_settings_new</i><a href="{{route('logOut')}}">خروج</a></li>
    </ul>
</div>
