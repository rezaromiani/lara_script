<div class="container-fluid" id="social-container">
    <div class="row">
        <div class="col-sm-6 col-md-6 col-lg-6 hidden-xs">
            <div id="ticker-fade" class="ticker">
                <ul>
                    <li>{{isset($options['main']['top-bar'])?$options['main']['top-bar']:''}}</li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 col-md-6 clo-lg-6">
            <ul class="list-inline text-left social-icon">
                @if(isset($options['social']))
                    @foreach($options['social'] as $key=>$val)
                        @if(!empty($val))
                            <li><a href="{{$val}}"><img src="img/{{$key}}.png" height="30" width="30"/></a></li>
                        @endif
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid" id="topbar-link-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <a href="{{route('user.home')}}"><img src="img/logo.png" height="50" width="120" style="width: auto;
    height: 40px;
    margin-top: 5px;" alt="logo"/></a>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6">
                <ul class="list-inline text-left" id="topbar-link">
                    <li><i class="material-icons">perm_identity</i>
                        @if(\Illuminate\Support\Facades\Auth::check())
                            <a href="{{route('user.account.index')}}">پنل کاربری</a> | <a href="{{route('logOut')}}">خروج</a>
                        @else
                            <a href="{{route('auth')}}">ورود</a> | <a href="{{route('auth')}}">عضویت</a>
                        @endif

                    </li>
                    <li><i class="material-icons">contact_mail</i> <a href="#">تماس با ما</a></li>
                    <li><i class="material-icons">phone</i><span class="ltr">0910 542 20 29</span></li>
                </ul>
            </div>
        </div>
    </div>

</div>