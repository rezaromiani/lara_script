<script>
    jQuery(document).ready(function ($) {
        $(document).on('click', '.likebtn', function (event) {
            event.preventDefault();
            $this = $(this);
            var action=1;
            if ($this.hasClass('fas')){
                action=2;
            }
            var post_id = $this.data('postid');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: "post",
                url:{!! json_encode(route('post.like.action')) !!},
                data: {post_id: post_id, action: action},
                beforeSend: function () {
                    @if(!\Illuminate\Support\Facades\Auth::check())
                    swal({
                        title: "هشدار!",
                        text: "لطفا برای ثبت نظر وارد سایت شوید.",
                        icon: "warning",
                        button: "تایید"
                    });
                    return false;
                    @endif
                },
                success: function (data) {
                    if (data.success) {
                        if (action==1){
                            $this.removeClass('far');
                            $this.addClass('fas');
                        } else if (action==2){
                            $this.removeClass('fas');
                            $this.addClass('far');
                        }
                        $this.siblings('span.dislike-result').text(data.dislikecount);
                        $this.siblings('span.like-result').text(data.likecount);
                        $this.siblings('i.dislikebtn').removeClass('fas').addClass('far');

                        swal({
                            title: "موفقیت!",
                            text: data.msg,
                            icon: "success",
                            button: "تایید"
                        });
                    }
                    if (data.error) {
                        swal({
                            title: "هشداز!",
                            text: data.msg,
                            icon: "warning",
                            button: "تایید"
                        });
                    }
                }
            })
        });
        $(document).on('click', '.dislikebtn', function (event) {
            event.preventDefault();
            $this = $(this);
            var action=0;
            if ($this.hasClass('fas')){
                action=2;
            }
            var post_id = $this.data('postid');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: "post",
                url:{!! json_encode(route('post.like.action')) !!},
                data: {post_id: post_id, action: action},
                beforeSend: function () {
                    @if(!\Illuminate\Support\Facades\Auth::check())
                    swal({
                        title: "هشدار!",
                        text: "لطفا برای ثبت نظر وارد سایت شوید.",
                        icon: "warning",
                        button: "تایید"
                    });
                    return false;
                    @endif
                },
                success: function (data) {
                    if (data.success) {
                        if (action==0){
                            $this.removeClass('far');
                            $this.addClass('fas');
                        } else if (action==2){
                            $this.removeClass('fas');
                            $this.addClass('far');
                        }
                        $this.siblings('span.dislike-result').text(data.dislikecount);
                        $this.siblings('span.like-result').text(data.likecount);
                        $this.siblings('i.likebtn').removeClass('fas').addClass('far');
                        swal({
                            title: "موفقیت!",
                            text: data.msg,
                            icon: "success",
                            button: "تایید"
                        });
                    }
                    if (data.error) {
                        swal({
                            title: "هشداز!",
                            text: data.msg,
                            icon: "warning",
                            button: "تایید"
                        });
                    }
                }
            })
        })

    });
</script>
