@section('load-script')
    <script>
        var url={!! json_encode(route('post.single.ajax',[$post->id,$post->slug])) !!}
    </script>
@endsection
@extends('layouts.user')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12" id="single-landing-layer-container">
                <div class="single-landing-layer">
                    <div class="col-sm-12 col-md-8 col-md-8 col-md-offset-2 col-lg-offset-2">
                        <img class="img-responsive img-circle" id="post-author-image" src="{{\App\Utility\Users::getUserAvatarUrl($post->User->email)}}"
                             width="150" height="150"/>
                        <h1><a href="#">{{$post->title}}</a></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--single content section-->
    <div class="container wrapper-margin-top">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="single-content wrapper-free-styles">
                    <div class="single-post-meta">
                        <ul class="list-inline">
                            <li><i class="material-icons">perm_identity</i>{{$post->User->username}}</li>
                            <li>
                                <i class="material-icons">date_range</i>{{ \Morilog\Jalali\Jalalian::forge($post->created_at)->format('Y-m-d') }}
                            </li>
                            <li><i class="material-icons">remove_red_eye</i> {{$post->view}}</li>
                            <li><i class="material-icons">comment</i>{{$post->Comment()->count()}}</li>
                            <li><a href="#"><i class="material-icons">border_color</i></a></li>
                        </ul>
                    </div>
                    <div class="all-content">
                        {!! $post->content !!}
                    </div>
                    @if($tag)
                        <div class="tags-section">
                            <ul class="list-inline tags">
                                @foreach($tag as $t)
                                    <li><a href="{{ route('all.post.tag',$t) }}" class="tag">{{$t}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                @include('user.post.related',$relatedPost)
                @include('user.Comment.comment',['allPostComment'=>$allPostComment,'type'=>'post'])
            </div>
        </div>
    </div>
@endsection