<div class="reated-post wrapper-free-styles wrapper-margin-top">
    <div class="related-post-section-title text-center">
        مطالب مرتبط
    </div>
    <div class="related-post-slider">
        @if($relatedPost)
            <div class="owl-carousel owl-theme" id="custom-owl-carousel">
                @foreach($relatedPost as $post)
                <div class="item">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default post-c">
                            <div class="panel-post ">
                                <img class="img-responsive" src="{{\App\Utility\PostUtility::getImgLink($post)}}">
                                <span class="i-date">{{\Morilog\Jalali\Jalalian::forge($post->created_at)->format('%B %d، %Y')}}<i
                                            class="material-icons">date_range</i></span>
                                <div class="overlay">
                                    <div class="text-ecxerpt">{{mb_substr(trim($post->content,'<p>'), 0, 80, 'utf-8').'...'}}
                                    </div>
                                    <ul class="list-inline" id="post-detail">
                                        <li>
                                            <i class="material-icons">perm_identity</i><span>{{$post->User->username}}</span>
                                        </li>
                                        <li><i class="material-icons">remove_red_eye</i><span> 1234 </span>
                                        </li>
                                        <li><i class="material-icons">comment</i> <span> 1234 </span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="post-link"><a href="{{route('post.single',[$post->id,$post->slug])}}">{{$post->title}}</a>
                            </div>
                            <div class="post-type-like">
                                            <span class="post-type"><i
                                                        class="material-icons">view_headline</i>رایگان</span>
                                <span class="like-btn">
                                <i class="{{(\App\Utility\PostUtility::isUserDisLikePost($post))?'fas':'far'}} fa-thumbs-down dislike-icon dislikebtn"
                                   id=""
                                   data-postid="{{$post->id}}"></i>
                            <span class="dislike-result">
                            {{$post->Like->where('action',0)->count()}}
                            </span>
                            <span class="like-result">
                            {{$post->Like->where('action',1)->count()}}
                            </span>
                        <i class="{{(\App\Utility\PostUtility::isUserLikePost($post))?'fas':'far'}} fa-thumbs-up like-icon likebtn"
                           id="" data-postid="{{$post->id}}"></i>
                    </span>
                            </div>
                        </div>
                    </div>
                </div>
                    @endforeach
            </div>
            @include('user.post.postLikeAction')
        @else
            <div class="" style="padding: 15px">
            <div class="alert alert-warning">
            <p>هیچ مطلب وجود ندارد.</p>
            </div>
            </div>
        @endif
    </div>
</div>
