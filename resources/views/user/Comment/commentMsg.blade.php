<div class="comment-parent">
    @foreach($items as $parent)
        <div id="comment-{{$parent->id}}"
             class="media  {{isset($answer) && intval($answer) ? 'comment-answer-section' :''}}">
            <div class="media-right" id="custom-media-right">
                <img class="media-object img-circle" id="custom-media-object"
                     src="{{\App\Utility\Users::getUserAvatarUrl($parent->User->email)}}"
                     width="50" height="50" alt="commenter">
                <div class="comment-line"></div>
            </div>
            <div class="media-body user-comment {{isset($answer) && !intval($answer) ? 'comment-question' :''}}">
                <div class="user-comment-title">
                    <ul class="list-inline">
                        <li><i class="material-icons">perm_identity</i>{{$parent->User->username}}</li>
                        <li>
                            <i class="material-icons">date_range</i>{{\Morilog\Jalali\Jalalian::forge($parent->created_at)->format('Y/m/d')}}
                        </li>
                        @if(isset($answer) && !intval($answer))
                        <li class="comment-answer"><a href="#" class="comment-reply" data-id="{{$parent->id}}">پاسخ</a>
                        </li>
                        @endif
                        <li class="comment-edit"><a href="#">ویرایش</a></li>
                        <li></li>
                    </ul>

                </div>
                <p class="comment-content">{{$parent->comment}}</p>
            </div>
        </div>
        @if(isset($allPostComment[$parent->id]))
            @include('user.Comment.commentMsg',['items'=>$allPostComment[$parent->id],'answer'=>true])
        @endif
    @endforeach
</div>

