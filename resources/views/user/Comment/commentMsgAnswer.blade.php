@foreach($items as $c)
    <div class="media comment-answer-section">
        <div class="media-right" id="custom-media-right">
            <img class="media-object img-circle" id="custom-media-object" src="assets/img/gravatar.png"
                 width="50" height="50" alt="commenter">
            <div class="comment-line"></div>
        </div>
        <div class="media-body user-comment">
            <div class="user-comment-title">
                <ul class="list-inline">
                    <li><i class="material-icons">perm_identity</i>{{$c->User->username}}</li>
                    <li>
                        <i class="material-icons">date_range</i>{{\Morilog\Jalali\Jalalian::forge($c->created_at)->format('Y/m/d')}}
                    </li>
                    <li class="comment-edit"><a href="#">ویرایش</a></li>
                </ul>

            </div>
            <p>{{$c->comment}}</p>
        </div>
    </div>
@endforeach