<div class="comment-section wrapper-free-styles wrapper-margin-top">
    <div class="comment-section-title text-center">
        دیدگاه کاربران
    </div>
    @if(isset($allPostComment[0]) && !empty($allPostComment))
        @include('user.Comment.commentMsg',['items'=>$allPostComment[0],'answer'=>false])
    @else
        <div class="" style="padding: 15px">
        <div class="alert alert-info">
            <p>اولین دیدگاه رو تو بنویس !</p>
        </div>
        </div>
    @endif
    <div class="send-comment">
        @include('user.Comment.commentForm')
    </div>
</div>