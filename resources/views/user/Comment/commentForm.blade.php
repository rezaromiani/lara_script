@if(\Illuminate\Support\Facades\Auth::check())
    <div class="panel panel-primary">
        <div class="panel-heading"><i class="material-icons">mode_comment</i>ارسال دیدگــاه</div>
        <div class="panel-body">

            <p>{{\Illuminate\Support\Facades\Auth::user()->username}} عزیز، شما لاگین هستید و می توانید سوال و یا دیدگاه
                خود را برای ما ارسال
                نمایید
                :</p>
            <p class="reply-text">


            </p>
            <form method="post" id="commentForm">
                {{csrf_field()}}
                <textarea class="form-control" id="comment-text" name="comment" rows="8"
                          placeholder="متن دیدگاه شما..."> </textarea>
                <br>
                <input type="hidden" name="post_id" value="{{$post->id}}">
                <input type="hidden" name="parent_id" value="0" id="parent_id">
                <input type="hidden" name="type" value="{{ $type }}" >
                <button class="btn btn-success comment-btn-send">ارسال دیدگاه</button>
            </form>
        </div>

    </div>
@else
    <div class="alert alert-danger text-center">کاربر گرامی برای ارسال دیدگاه خود ابتدا باید در سایت
        <a
                href="#">لاگین</a> نمایید
    </div>
@endif