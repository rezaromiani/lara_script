@section('load-script')
    <script>
        var url ={!! json_encode(route('post.single.ajax',[$courseSession->id,$courseSession->slug])) !!}
    </script>
@endsection
@extends('layouts.user')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12" id="single-landing-layer-container">
                <div class="single-landing-layer">
                    <div class="col-sm-12 col-md-8 col-md-8 col-md-offset-2 col-lg-offset-2">
                        <img class="img-responsive img-circle" id="post-author-image" src="assets/img/gravatar.png"
                             width="150" height="150"/>
                        <h1><a href="#">{{$courseSession->title}}</a></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--single content section-->
    <div class="container wrapper-margin-top">
        <div class="row">
            <div class="col-sm-12 col-md-9 col-lg-9 ">
                <div class="single-content wrapper-free-styles">
                    <div class="single-post-meta">
                        <ul class="list-inline">
                            <li>
                                <i class="material-icons">perm_identity</i>{{$courseSession->Course->Teacher->fullName()}}
                            </li>
                            <li>
                                <i class="material-icons">date_range</i>{{ \Morilog\Jalali\Jalalian::forge($courseSession->created_at)->format('Y-m-d') }}
                            </li>
                            <li><i class="material-icons">remove_red_eye</i> {{$courseSession->view}}</li>
                            <li><i class="material-icons">comment</i>{{$courseSession->Comment()->count()}}</li>
                            <li><a href="#"><i class="material-icons">border_color</i></a></li>
                        </ul>
                    </div>
                    <div class="all-content">
                        {!! $courseSession->content !!}
                        @if(\App\Utility\CourseUtility::isUserCourseStudent($courseSession->course_id))
                        @if(isset($courseSession->video_url)&&!empty($courseSession->video_url))
                            <video
                                    id="my-player"
                                    class="video-js my-video-js-style"
                                    controls
                                    preload="auto"
                                    poster="http://phpscript.com/storage/upload/image/19a5c083683abe1cfd10d494cb898f23.jpeg"
                                    data-setup='{}'>
                                <source src="{{$courseSession->video_url}}" type="video/mp4"></source>
                                <p class="vjs-no-js">
                                    To view this video please enable JavaScript, and consider upgrading to a
                                    web browser that
                                    <a href="http://videojs.com/html5-video-support/" target="_blank">
                                        supports HTML5 video
                                    </a>
                                </p>
                            </video>
                        @endif
                        <div class="text-center">
                            <a href="{{route('file.download',$courseSession->file_url)}}" class="btn btn-info">دانلود
                                فایل جلسه</a>
                        </div>
                            @else
                            <div class="alert alert-warning">
                                این مطلب یک جلسه از آموزش لاراول مبتدی تا پیشرفته می باشد و برای مشاهده آن باید در دوره ثبت نام کنید.
                            </div>
                            @endif
                    </div>
                </div>
                @include('user.Comment.comment',['allPostComment'=>$allPostComment,'post'=>$courseSession,'type'=>'session'])
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3">
                <h2 class="sessions-single-header">جلسات دوره</h2>
                @if(isset($sessions) && $sessions)
                    <ol class="course-parts sessions-single">
                        @foreach($sessions as $s)
                            <li class="{{ $courseSession->id == $s->id ? 'active':null }}"><a
                                        href="{{ route('course.session.single',[$s->id,$s->slug]) }}">{{$s->title}}</a>
                            </li>
                        @endforeach
                    </ol>
                @endif


            </div>
        </div>
    </div>
@endsection