@section('load-script')
    <script>
        var url =
        {!! json_encode(route('post.single.ajax',[$course->id,$course->slug])) !!}
    </script>

@endsection
@extends('layouts.user')

@section('content')
    <!--single header section-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12" id="course-landing-layer-container">
                <div class="course-landing-layer">
                    <div class="col-sm-12 col-md-8 col-md-8 col-md-offset-2 col-lg-offset-2">
                        <h1><a href="#"><i class="material-icons">import_contacts</i>{{$course->title}}</a></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--single content section-->
    <div class="container wrapper-margin-top">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-8 ">
                <div class="single-content wrapper-free-styles">
                    <p class="course-detail-title"><i class="material-icons">ondemand_video</i>دموی دوره</p>
                    <div class="all-content">
                        <video
                                id="my-player"
                                class="video-js my-video-js-style"
                                controls
                                preload="auto"
                                poster="{{$course->image}}"

                                data-setup='{}'>
                            <source src="{{$course->demo_url}}" type="video/mp4"></source>
                            <p class="vjs-no-js">
                                To view this video please enable JavaScript, and consider upgrading to a
                                web browser that
                                <a href="http://videojs.com/html5-video-support/" target="_blank">
                                    supports HTML5 video
                                </a>
                            </p>
                        </video>

                        <!--tab navigation-->
                        <ul class="nav nav-tabs nav-tab-course" id="custom-nav-tabs">
                            <li class="active"><a href="#course-parts" data-toggle="tab">جلسات منتشر شده</a></li>
                            <li><a href="#reference" data-toggle="tab">سرفصل های دوره</a></li>
                            <li><a href="#students" data-toggle="tab">دانشجویان دوره</a></li>
                        </ul>
                        <!--tab contents   -->
                        <div class="tab-content" id="custom-tab-content">
                            <div class="tab-pane fade in active" id="course-parts">
                                @if(isset($sessions) && $sessions)
                                    <ol class="course-parts">
                                        @foreach($sessions as $s)
                                            <li>
                                                <a href="{{ route('course.session.single',[$s->id,$s->slug]) }}">{{$s->title}}</a><span
                                                        class="part-duration">{{$s->video_time}} دقیقه</span></li>
                                        @endforeach
                                    </ol>
                                @else
                                    <div class="alert alert-info">
                                        هنوز جلسه برای این دوره منتشر نشده است
                                    </div>
                                @endif
                            </div>
                            <div class="tab-pane fade" id="reference">
                                {!! $course->Headlines !!}
                            </div>
                            <div class="tab-pane fade" id="students">
                                <ul class="list-inline" id="students-name">
                                    <li>وحید صالحی</li>
                                    <li>وحید صالحی</li>
                                    <li>وحید صالحی</li>
                                    <li>وحید صالحی</li>
                                    <li>وحید صالحی</li>
                                    <li>وحید صالحی</li>
                                    <li>وحید صالحی</li>
                                    <li>وحید صالحی</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <!--comments section-->
                @include('user.Comment.comment',['allPostComment'=>$allPostComment,'post'=>$course,'type'=>'Course'])
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="wrapper-free-styles" id="course-detail-sidebar">
                    <p>جزئیات دوره</p>
                    <ul class="list-group">
                        <li><i class="material-icons">timelapse</i>طول دوره<span>{{$course->course_length}}</span></li>
                        <li><i class="material-icons">supervisor_account</i>دانشجویان<span>{{$course->Students->count()}} نفر</span>
                        </li>
                        <li><i class="material-icons">widgets</i>وضعیت دوره<span
                                    class="course-status">{{$course->status}}</span>
                        </li>
                        <li><i class="material-icons">done</i>شروع دوره <span
                                    class="course-start-time">{{$course->start_data}}</span>
                        </li>
                    </ul>
                    @if(\App\Utility\CourseUtility::isUserCourseStudent($course->id))
                        <div class="alert alert-info course-alert">
                            <i class="material-icons">how_to_reg</i>
                            شما دانشجوی این دوره هستید.
                        </div>
                    @else
                        {!! App\Utility\CourseUtility::getPrice($course) !!}
                        <form method="post" id="course_form"
                              action="{{route('course.single',[$course->id,$course->title])}}">
                            {{csrf_field()}}
                            <input type="hidden" name="course" value="{{$course->id}}">
                            <input type="submit" class="btn btn-success btn-block" value="ثبت نام">
                        </form>
                    @endif
                </div>
                <div class="wrapper-free-styles" id="teacher-resume">
                    <p>مدرس دوره</p>
                    <div class="course-teacher">
                        <img src="{{\App\Utility\Users::getUserAvatarUrl($course->Teacher->email)}}"
                             class="img-responsive img-circle" height="100" width="100"/>
                    </div>
                    <div class="course-teacher-name text-center">{{$course->Teacher->fullName()}}</div>
                    <span class="about-teacher">
                        {{$course->Teacher->Resumes}}
                    </span>
                </div>
                <div class="wrapper-free-styles" id="prerequisite-course">
                    <p>پیشنیازها</p>
                    <span class="prerequisite">{{$course->prerequisites}} </span>
                </div>

            </div>
        </div>
        @if(!\Illuminate\Support\Facades\Auth::check())
            <script>
                $(document).ready(function () {
                    $(document).on('submit', '#course_form', function (ev) {
                        ev.preventDefault();
                        swal({
                            text: "برای ثبت نام در این دوره لطفا وارد سایت شوید",
                            icon: "error",
                            button: "تایید",
                        });
                    });
                });
            </script>
        @endif
    </div>
@endsection