<div class="tab-pane" id="profile-course-list">
    <ul class="list-group">
        @if(isset($course) && $course)
            @foreach($course as $c)
                <li><a href="{{route('course.single',[$c->id,$c->slug])}}"><i class="material-icons">import_contacts</i>{{$c->title}}</a>
                </li>
            @endforeach
        @endif
    </ul>
</div>