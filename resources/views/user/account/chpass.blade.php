<div class="tab-pane" id="profile-password">

    <br>
    <br>
    <br>
    <form class="form-horizontal " method="post" action="{{route('user.account.chPassword')}}">
        {{csrf_field()}}
        <div class="form-group input-container">

            <div class="col-sm-10 col-sm-offset-1 input_container">
                <input type="password" class="form-control input-lg" name="password"
                       placeholder="کلمه عبور  خود را وارد نمایید..." >

            </div>
        </div>

        <div class="form-group input-container">

            <div class="col-sm-10 col-sm-offset-1 input_container">
                <input type="password" class="form-control input-lg" name="password_confirmation"
                       placeholder="تکرار کلمه عبور..."
                       >

            </div>
        </div>


        <div class="form-group input-container">
            <div class=" col-sm-12 text-center" style="margin-top: 20px;margin-bottom: 5px;">
                <div class="col-sm-6 col-sm-offset-3">
                    <button type="submit" class="btn btn-success btn-block">بروزرسانی</button>
                </div>
            </div>
        </div>
    </form>
</div>
