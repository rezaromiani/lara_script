@extends('layouts.user')
@section('load-script')
    <script src='https://www.google.com/recaptcha/api.js?hl=fa'></script>
@endsection
@section('content')
    <div class="container wrapper-free-styles wrapper-margin-top">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12" id="profile-content">
                <h3>پروفایل کاربری</h3>
                <div class="col-sm-2 col-md-2 col-lg-2" id="profile-menu">
                    <ul class="nav nav-tabs tabs-left" id="profile-nav">
                        <li class="active"><a href="#home-profile" data-toggle="tab"><i
                                        class="material-icons">perm_identity</i>شناسنامه شما</a></li>
                        <li><a href="#profile-course-list" data-toggle="tab"><i class="material-icons">import_contacts</i>دوره
                                های شما</a>
                        </li>
                        <li><a href="#profile-password"  data-toggle="tab"><i class="material-icons">vpn_key</i>تغییر رمز</a></li>
                        <li><a href="#profile-user-tickets" data-toggle="tab"><i class="material-icons">receipt</i>ارسال
                                تیکت</a></li>
                        <li><a href="#profile-user-all-tickets" data-toggle="tab"><i class="material-icons">record_voice_over</i>پیام
                                های
                                شما</a></li>
                        <li><a href="#profile-user-transaction" data-toggle="tab"><i class="material-icons">monetization_on</i>تراکنش
                                های شما</a></li>
                    </ul>
                </div>
                <div class="col-sm-10 col-md-10 col-lg-10" id="tab-divider">
                    <div class="tab-content" style="min-height: 535px">
                        @include('user.account.profile')
                        @include('user.account.course')
                        @include('user.account.tickets')
                        @include('user.account.alltickets')
                        @include('user.account.transactions')
                        @include('user.account.chpass')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection