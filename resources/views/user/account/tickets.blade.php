<div class="tab-pane" id="profile-user-tickets">
    <div class="lod " id="lod-ticket">
        <p>بارگذاری...</p>
    </div>
    <form id="tickets_create" method="post">
        <div class="row">
            <div class="col-sm-8">
                <div class="form-group">
                    <label>عنوان تیکت*</label>
                    <input type="text" class="form-control" name="ticket_title" placeholder="عنوان تیکت...">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>اولویت*</label>
                    <select class="form-control" name="ticket_priority">
                        <option value="1">عادی</option>
                        <option value="2">مهم</option>
                        <option value="3">بسیار مهم</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label>پـیام شما*</label>
                    <textarea class="form-control" rows="10" name="ticket_text"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" name="ticket_create" value="ارسال" class="btn btn-success">
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    jQuery(document).ready(function ($) {
        $(document).on('submit', '#tickets_create', function (ev) {
            ev.preventDefault();
            let $this = $(this);
            let load = $('#lod-ticket');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:{!! json_encode(route('user.account.ticket.send')) !!},
                method: 'post',
                data: $this.serialize(),
                beforeSend: function () {
                    load.fadeIn();
                },
                success: function (data) {
                    if (data.success) {
                        swal("", "تیــکــت با مـوفـقیـت ثـبـت شــد.", "success");
                        $this[0].reset();
                        $('#ticket-all-container').html(data.view)
                    }
                    if (data.error) {
                        swal("خـطـا!", "خطایـی هنـگـام عمـیـلـیـات پـیـش آمـده.", "error")
                    }

                },
                error: function () {
                    swal("خـطـا!", "خطایـی هنـگـام عمـیـلـیـات پـیـش آمـده.", "error")
                },
                complete: function () {
                    load.fadeOut()
                }
            })
        });
    })
</script>