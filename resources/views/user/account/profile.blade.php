<div class="tab-pane active" id="home-profile">
    <div class="lod">
        <p>بارگذاری...</p>
    </div>
    <div class="profile-img-holder text-center">
        <img class="img-circle" src="{{ \App\Utility\Users::getUserAvatarUrl($user->email) }}" width="130" height="130">
    </div>
    <form class="form-horizontal" id="profile-form">
        <div class="form-group input-container">

            <div class="col-sm-10 col-sm-offset-1 input_container">
                <input type="text" class="form-control input-lg" name="fname"
                       placeholder="نام خود را وارد نمایید..." value="{{$user->first_name}}">
                <i class="material-icons input-icon">perm_identity</i>
            </div>
        </div>

        <div class="form-group input-container">

            <div class="col-sm-10 col-sm-offset-1 input_container">
                <input type="text" class="form-control input-lg" name="lname"
                       placeholder="نام خانوادگی خود را وارد نمایید..."
                       value="{{$user->last_name}}">
                <i class="material-icons input-icon">supervisor_account</i>
            </div>
        </div>
        <div class="form-group input-container">

            <div class="col-sm-10 col-sm-offset-1 input_container">
                <input type="text" class="form-control input-lg" name="uname"
                       placeholder="نام کاربری خود را وارد نمایید..."
                       value="{{$user->username}}"
                >
                <i class="material-icons input-icon">face</i>
            </div>
        </div>
        <div class="form-group input-container">

            <div class="col-sm-10 col-sm-offset-1 input_container">
                <input type="email" class="form-control input-lg" name="email"
                       placeholder="ایمیل خود را وارد نمایید..."
                       value="{{$user->email}}" disabled
                >
                <i class="material-icons input-icon">email</i>
            </div>
        </div>

        <div class="form-group input-container">
            <div class=" col-sm-12 text-center" style="margin-top: 20px;margin-bottom: 5px;">
                <div class="col-sm-6 col-sm-offset-3">
                    <button type="submit" class="btn btn-success btn-block">بروزرسانی</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    jQuery(document).ready(function ($) {
        $(document).on('submit', '#profile-form', function (event) {
            event.preventDefault();
            let $this = $(this);
            let load = $('.lod');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:{!! json_encode(route('user.account.edit')) !!},
                method: 'post',
                data: $this.serialize(),
                beforeSend: function () {
                    load.fadeIn();
                },
                success: function (data) {
                    if (data.success) {
                        swal("", "تغییـرات با مـوفـقیـت ثـبـت شــد.", "success")
                    }
                    if (data.error) {
                        swal("خـطـا!", "خطایـی هنـگـام عمـیـلـیـات پـیـش آمـده.", "error")
                    }

                },
                error:function(){
                    swal("خـطـا!", "خطایـی هنـگـام عمـیـلـیـات پـیـش آمـده.", "error")
                },
                complete: function () {
                    load.fadeOut()
                }
            })
        });
    });
</script>