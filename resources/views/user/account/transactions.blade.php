<div class="tab-pane " id="profile-user-transaction">
    <table class="table table-bordered table-hover table-responsive" id="user-transaction-table">
        <tr>
            <th>شماره سفارش</th>
            <th>شماره تراکنش</th>
            <th>تاریخ پرداخت</th>
            <th>تاریخ پرداخت</th>
            <th>مبلغ</th>
            <th>وضعیت</th>
        </tr>
        @if(isset($transactions) && $transactions)
            @foreach($transactions as $t)
                <tr>
                    <td>{{$t->order_number}}</td>
                    <td>{{$t->ref_number}}</td>
                    <td>{{\Morilog\Jalali\Jalalian::forge($t->created_at)->format('Y-m-d')}}</td>
                    <td>{{\Morilog\Jalali\Jalalian::forge($t->created_at)->format('H:i:s')}}</td>
                    <td>{{number_format($t->price)}}</td>
                    <td>
                        @if($t->status ==1)
                        <spna class="text-success">پرداخت شده</spna>
                        @else
                            <span class="text-danger">پرداخت ناموفق</span>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif


    </table>
</div>