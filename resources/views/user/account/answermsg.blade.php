@foreach($ti->ticketMsg as $msg)
    <div>
        <img class="img-circle sender-ticket-image" src="{{\App\Utility\Users::getUserAvatarUrl($msg->user->email)}}"><span>ارسال شده در تاریخ : {{\Morilog\Jalali\Jalalian::forge($ti->created_at)->format('Y-m-d')}}</span>

        <div class="ticket-text">
            <p>
                {{$msg->content}}
            </p>
        </div>
    </div>
@endforeach