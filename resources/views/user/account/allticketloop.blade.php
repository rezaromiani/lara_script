@if(isset($tickets)&& $tickets)
    @foreach($tickets as $ti)
        <div class="panel panel-info">
            <div class="load-t lod " >
                <p>بارگذاری...</p>
            </div>
            <div class="panel-heading" id="">
                <a href="#section-{{$ti->id}}" data-toggle="collapse" data-parent="#accordion">موضوع تیکت
                    :{{$ti->title}}</a>
                <span class="btn btn-warning">{{$ti->ticket_format()}}</span>
                <span class="pull-left btn btn-info"
                      style="margin-right: 5px">شماره تیکت : {{$ti->id}}</span>
                <span class="pull-left btn btn-success">پیام جدید : {{'0'}}</span>
            </div>
            <div id="section-{{$ti->id}}" class="panel-collapse collapse  custom-panel-collapse">
                <div class="ticket-details-wrapper">
                    <div id="ticket-msg-container-{{$ti->id}}">
                    @include('user.account.answermsg')
                    </div>
                    <div class="ticket-answer" id="">
                        <form class="ticket-answer-frm" method="post">
                            <div class="form-group">
                                <textarea class="form-control" name="ticket_text" rows="10"></textarea>
                            </div>
                            <input type="hidden" name="ticket_id" value="{{$ti->id}}">

                            <div class="form-group text-left">
                                <input type="submit" class="btn btn-success" value="ارسال پیام">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

@else
    <div class="alert alert-danger text-center">در حال حاضر پیامی ندارید!</div>
@endif
