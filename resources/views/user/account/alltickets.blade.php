<div class="tab-pane " id="profile-user-all-tickets">
    <div class="panel-group" id="accordion">
        <div id="ticket-all-container">
            @include('user.account.allticketloop')
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function ($) {
        $(document).on('submit', '.ticket-answer-frm', function (ev) {
            ev.preventDefault();
            let $this = $(this);
            let id = $this.find('input[name="ticket_id"]').val();
            let load = $('.load-t');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:{!! json_encode(route('user.account.ticket.answer')) !!},
                method: 'post',
                data: $this.serialize(),
                beforeSend: function () {
                    load.fadeIn();
                },
                success: function (data) {
                    if (data.success) {
                        swal("", "پـیــام با مـوفـقیـت ثـبـت شــد.", "success");
                        $this[0].reset();
                        $('#ticket-msg-container-'+id).html(data.view)
                    }
                    if (data.error) {
                        swal("خـطـا!", "خطایـی هنـگـام عمـیـلـیـات پـیـش آمـده.", "error")
                    }

                },
                error: function () {
                    swal("خـطـا!", "خطایـی هنـگـام عمـیـلـیـات پـیـش آمـده.", "error")
                },
                complete: function () {
                    load.fadeOut()
                }
            })
        });
    })

</script>