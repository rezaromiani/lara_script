@extends('layouts.user')
@section('load-script')
@endsection
@section('content')
    <div class="container wrapper-free-styles wrapper-margin-top">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="col-sm-126">
                    <div class="panel panel-info wrapper-margin-top">
                        <div class="panel-heading" id="panel-login-heading">فعالسازی حساب کاربری</div>
                        <div class="panel-body text-center">
                            <p style="font-size: 15px;"><i class="material-icons" style="font-weight: 600;
    vertical-align: -3px;
    margin-right: 0px;color: #5cb85c;
    margin-left: 4px;">done</i>کاربر گرامی {{$user->username}} حساب کاربری شما فعال گردید . از طریق لینک زیر می توانید وارد سایت شوید.</p>
                            <a href="{{route('auth')}}" class="btn btn-success">ورود به سایت</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection