@extends('layouts.user')
@section('load-script')
    <script src='https://www.google.com/recaptcha/api.js?hl=fa'></script>
@endsection
@section('content')
    <div class="container wrapper-free-styles wrapper-margin-top" style="margin-top: 50px;">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-info wrapper-margin-top">
                        <div class="panel-heading" id="panel-login-heading">بازیابی کلمه عبور</div>
                        <div class="panel-body">
                            <form method="post" action="" class="form-horizontal" id="login-register-form">
                                @if(isset($error))
                                    <div class="alert alert-warning">
                                        <p>
                                            {{$error}}
                                        </p>
                                    </div>
                                    @endif
                                    @if(isset($success))
                                        <div class="alert alert-success">
                                            <p>
                                                {{$success}}
                                            </p>
                                        </div>
                                    @endif
                                <div class="form-group input-container">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <input type="email" name="email" class="form-control input-lg"
                                               placeholder="ایمیل خود را وارد نمایید...">
                                        <i class="material-icons input-icon">email</i>
                                    </div>

                                </div>
                                <div class="form-group input-container">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        من ربات نیستم
                                        {{csrf_field()}}
                                        <div class="g-recaptcha"
                                             data-sitekey="6Lc1IYwUAAAAADpl5ggwpjrF4U7UUx32cF-198vm"></div>
                                    </div>

                                </div>

                                <div class="form-group input-container">
                                    <div class="col-sm-6 col-sm-offset-3 text-center">
                                        <button type="submit" name="btn_change_password" class="btn btn-info btn-block">
                                            بازیابی کلمه عبور
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
@endsection