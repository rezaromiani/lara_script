@extends('layouts.user')
@section('load-script')
    <script src='https://www.google.com/recaptcha/api.js?hl=fa'></script>
@endsection
@section('content')
    <div class="container wrapper-free-styles wrapper-margin-top">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                @if(session('success'))
                    <div class="alert alert-success">
                        <p>{{session('success')}}</p>
                    </div>
                @endif
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="panel panel-info wrapper-margin-top">
                        <div class="panel-heading" id="panel-login-heading">ورود به سایت</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="post" id="login-register-form">
                                {{csrf_field()}}
                                @if(isset($error_login))
                                    <div class="alert alert-warning">
                                        <p>{{$error_login}}</p>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-warning">
                                        <p><i class="material-icons">
                                                warning
                                            </i>
                                            هشدار
                                        </p>
                                        <br>
                                        @foreach($errors->all() as $error)
                                            <p>{{$error}}</p>
                                        @endforeach
                                    </div>
                                @endif
                                <div class="form-group input-container">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        @if ($errors->has('email'))
                                            asdas
                                        @endif
                                        <input type="email" class="form-control input-lg has-error" name="email"
                                               placeholder="ایمیل خود را وارد نمایید..." value="{{old('email')}}">
                                        <i class="material-icons input-icon">email</i>
                                    </div>

                                </div>
                                <div class="form-group input-container">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <input type="password" id="password" name="password" data-toggle="password"
                                               class="form-control input-lg"
                                               placeholder="کلمه خود را وارد نمایید...">
                                        <i class="material-icons input-icon">lock_open</i>
                                    </div>

                                </div>
                                <div class="form-group input-container">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="checkbox">
                                            <label class="checkbox-container">مرا بخاطر بسپار
                                                <input type="checkbox" name="remmemberMe">
                                                <span class="checkmark-login"></span>
                                            </label>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group input-container">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        من ربات نیستم
                                        <div class="g-recaptcha"
                                             data-sitekey="6Lc1IYwUAAAAADpl5ggwpjrF4U7UUx32cF-198vm"></div>
                                    </div>

                                </div>
                                <div class="form-group input-container">
                                    <div class="col-sm-6 col-sm-offset-3 text-center">
                                        <button type="submit" name="doLog" value="h" class="btn btn-info btn-block">
                                            ورود
                                        </button>
                                    </div>
                                </div>
                                <div class="form-group input-container">
                                    <div class="col-sm-6 col-sm-offset-3 text-center">
                                        <a href="{{route('resetPassword')}}" class="forget-password"><i class="material-icons">lock</i>بازیابی
                                            کلمه
                                            عبور</a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="panel panel-info wrapper-margin-top">
                        <div class="panel-heading" id="panel-register-heading">ثبت نام در سایت</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="post" id="login-register-form">
                                {{csrf_field()}}
                                @if(isset($success))
                                    <div class="alert alert-success">
                                        <p>{{$success}}</p>
                                    </div>
                                @endif
                                <div class="form-group input-container">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <input type="text" class="form-control input-lg"
                                               name="firstName"
                                               placeholder="نام خود را وارد نمایید...">
                                        <i class="material-icons input-icon">perm_identity</i>
                                    </div>

                                </div>
                                <div class="form-group input-container">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <input type="text" class="form-control input-lg"
                                               name="lastName"
                                               placeholder="نام خانوادگی خود را وارد نمایید...">
                                        <i class="material-icons input-icon">supervisor_account</i>
                                    </div>

                                </div>
                                <div class="form-group input-container">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <input type="text" class="form-control input-lg"
                                               name="username" placeholder="نام کاربری خود را وارد نمایید...">
                                        <i class="material-icons input-icon">face</i>
                                    </div>

                                </div>
                                <div class="form-group input-container">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <input type="email" class="form-control input-lg"
                                               name="email-reg" placeholder="ایمیل خود را وارد نمایید...">
                                        <i class="material-icons input-icon">email</i>
                                    </div>

                                </div>
                                <div class="form-group input-container">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <input type="email" class="form-control input-lg"
                                               name="email-reg_confirmation" placeholder="تکرار ایمیل...">
                                        <i class="material-icons input-icon">email</i>
                                    </div>

                                </div>
                                <div class="form-group input-container">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <input type="password" class="form-control input-lg" id="password-reg"
                                               data-toggle="password"
                                               name="password-reg"
                                               placeholder="کلمه عبور  خود را وارد نمایید...">
                                        <i class="material-icons input-icon">lock_open</i>
                                    </div>

                                </div>
                                <div class="form-group input-container">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <input type="password" class="form-control input-lg" id="password-reg-conf"
                                               data-toggle="password"
                                               name="password-reg_confirmation"
                                               placeholder="تکرار کلمه عبور...">
                                        <i class="material-icons input-icon">lock_open</i>
                                    </div>

                                </div>

                                <div class="form-group input-container">
                                    <div class="col-sm-6 col-sm-offset-3 text-center">
                                        <button type="submit" name="doReg" value="c" class="btn btn-success btn-block">
                                            ثبت نام
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection