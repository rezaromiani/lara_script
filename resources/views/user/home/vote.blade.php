<div class="section-title">
    <h5>نظرسنجی</h5>
</div>
<div class="vote">
    <div class="alert alert-info text-center">{{$vote->poll_title}}</div>
    <div class="vote-container">
        @if(\App\Utility\Users::isUserVote($vote))
            @include('user.home.vote-result')
        @else

            <form class="vote-frm">
                @if($vote->PollItem)
                    @foreach($vote->PollItem as $p)
                        <label class="label-container">{{$p->title}}
                            <input type="radio" name="radio" class="poll-item" value="{{$p->id}}">
                            <span class="checkmark"></span>
                        </label>
                    @endforeach
                @endif
                <div class="btn-vote text-center">
                    <input type="hidden" class="poll_id" name="poll_id" value="{{$vote->id}}">
                    <input type="submit" class="btn btn-success btn-sm" value="ثبت رای">
                </div>
            </form>
            <script>
                jQuery(document).ready(function ($) {
                    $(document).on('submit', '.vote-frm', function (ev) {
                        ev.preventDefault();
                        var poll_item = $('.poll-item:checked').val();
                        var poll = $(this).find('.poll_id').val();

                        @if(!\Illuminate\Support\Facades\Auth::check())
                        swal("", "لطفا برای ثبت نظر وارد سایت شوید.", "warning");
                        return 0;
                        @endif

                        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
                        $.ajax({
                            url:{!! json_encode(route('poll.item.ajax')) !!},
                            method: 'post',
                            data: {item: poll_item, poll: poll},
                            success: function (data) {
                                if (data.success) {
                                    swal("", "لطفا برای ثبت نظر وارد سایت شوید.", "success");
                                    $('.vote-container').html(data.result)
                                }
                                if (data.voted) {
                                    swal("", "شما قبلا در این نظر سنجی شرکت کرده اید.", "warning");

                                }
                            },
                            error: function (data) {

                            }
                        });
                    })

                });
            </script>
        @endif
    </div>

</div>
