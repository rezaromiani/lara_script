<div class="container wrapper-margin-top">
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-8">
            <div class="col-sm-12 col-md-12 col-lg-12 main-wrapper box-shadow">
                <div class="section-title">
                    <h5>دوره های آموزشی</h5>
                </div>
                @if(isset($course) && count($course))
                    @foreach($course as $c)
                        <div class="course-info">
                            <div class="course-thumbnail">
                                <img class="img-responsive" src="{{ $c->image }}">
                            </div>
                            <div class="course-detail">
                                <a href="{{route('course.single',[$c->id,$c->slug])}}"><h3
                                            class="text-center course-title">{{ $c->title }}</h3></a>
                                <a href="#course-meta-section-{{$c->id}}" data-toggle="collapse"
                                   class="course-info-tooltip"><i
                                            class="material-icons" data-toggle="tooltip" data-placement="top"
                                            title="جزئیات بیشتر...">more_horiz</i></a>
                                <div class="collapse course-meta" id="course-meta-section-{{$c->id}}">
                                    <ul class="list-inline text-center">
                                        <li><i class="material-icons">person</i> مدرس :
                                            <span>{{ $c->Teacher->FullName() }}</span></li>
                                        <li><i class="material-icons">alarm</i> طول دوره :
                                            <span>{{ $c->course_length }}</span></li>
                                        <li><i class="material-icons">clear_all</i> وضعیت دوره :
                                            <span>{{ $c->status }}</span></li>
                                        <li><i class="material-icons">school</i> دانشجویان :
                                            <span><span>{{$c->Students->count()}}</span> نفر </span>
                                        </li>
                                    </ul>
                                    <div class="text-center">
                                        <a href="{{route('course.single',[$c->id,$c->slug])}}" class="btn btn-success">خرید
                                            این دوره</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    @endforeach
                @else
                    <div class="alert alert-info">
                        دوره ای وجود ندارد.
                    </div>
                @endif


                <div class="text-center " id="more-course">
                    <a href="{{ route('all.post.Course') }}" class="btn btn-info">موارد بیشتر...</a>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4">
            <div class="col-sm-12 col-md-12 col-lg-12 main-wrapper box-shadow">
            @if($vote)
                <!--vote section-->
                @include('user.home.vote')
            @endif
            <!--tasks section-->
            @include('user.home.progress')
            @if($ads)
                <!--ads section-->
                    @include('user.home.ads')
                @endif
            </div>
        </div>
    </div>
</div>
