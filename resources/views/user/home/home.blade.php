@extends('layouts.user')
@section('content')
    <!--hero header-->
    @include('user.home.heroHeader')
    <!--slider & info section-->
    <div class="container" id="custom-slider-info-container">
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-4" id="info-section">
                <div class="wrapper-free-styles ">
                    @if(isset($notifications) && $notifications)
                        @foreach($notifications as $n)

                            <div class="info-box">
                                <div class="info-title-section">
                                    <i class="material-icons">info_outline</i>اطلاعیه ها
                                </div>
                                <div class="info-text">
                                    <span>{{$n->text}}</span>
                                    <span class="info-date">{{\Morilog\Jalali\Jalalian::forge($n->created_at)->format('Y-m-d')}}<i
                                                class="material-icons">date_range</i></span>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>

            </div>
            <div class="col-sm-12 col-md-8 col-lg-8">
                <div id="carousel" class="carousel slide" data-ride="carousel">
                @if(isset($slides) && $slides)
                    <!--slider-indicators-->
                        <ol class="carousel-indicators">
                            @for($i=0;$i<$slides->count();$i++)
                                <li data-target="#carousel" data-slide-to="{{$i}}"
                                    class="{{$i==0 ? 'active' : null}}"></li>
                            @endfor
                        </ol>
                        <!--image wrapper-->
                        <div class="carousel-inner" id="custom-carousel-inner">
                            @foreach($slides as $s)
                                <div class="item {{$loop->first ? 'active' : null}}">
                                    <a href="{{$s->url}}">

                                        <img src="{{{$s->img}}}" alt="image">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>
    <!--new topic and random topic section-->
    <div class="container wrapper-margin-top">
        <div class="row">
            <div class="col-sm-12 clo-md-12 col-lg-12">
                <div class="col-sm-12 clo-md-12 col-lg-12 main-wrapper wrapper-free-styles ">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <p class="lesson-title"><i class="material-icons">library_books</i>جدید ترین مطالب آموزشی</p>
                        <div class="post-publish">
                            <ul class="list-group post-publish-list">
                                @if($lastPost && !empty($lastPost))
                                    @foreach($lastPost as $l)
                                        <li><span class="list-icon"></span>
                                            <div>
                                                <a href="{{route('course.session.single',[$l->id,$l->slug])}}">{{$l->title}}</a>
                                            </div>
                                            <div class="post-publish-date ">
                                        <span class="hidden-xs">{{\Morilog\Jalali\Jalalian::forge($l->created_at)->format('Y-m-d')}}<i
                                                    class="material-icons">date_range</i></span>

                                            </div>

                                        </li>
                                    @endforeach
                                @else
                                    <li>هیچ مطلبی وجود ندارد</li>
                                @endif

                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <p class="random-title"><i class="material-icons">refresh</i>مطالب تصادفی</p>
                        <div class="post-random">
                            <ul class="list-group" id="vertical-ticker">
                                @if($randomPost && !empty($lastPost))
                                    @foreach($randomPost as $r)
                                        <li><a href="{{\App\Utility\PostUtility::getPostLink($r)}}"><span><img
                                                            class="img-thumbnail"
                                                            src="{{\App\Utility\PostUtility::getImgLink($r)}}"
                                                            height="60"
                                                            width="60"/></span>
                                                {{$r->title}} </a></li>
                                    @endforeach
                                @else
                                    <li>هیچ مطلبی وجود ندارد</li>
                                @endif

                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--main-content-section-->
    @include('user.home.post-container')
    <!--course & poll-section-->
    @include('user.home.cours&poll')
@endsection
