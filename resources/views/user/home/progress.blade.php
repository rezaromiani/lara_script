@if(isset($progress) && !empty($progress))
<div class="section-title">
    <h5>پیشرفت دورهاه</h5>
</div>
<div class="tasks-progressbar">
    @foreach($progress as $p)
    <p> {{$p->title}} </p>
    <div class="progress">
        <div class="progress-bar progress-bar-{{$p->color}} progress-bar-striped active" style="width: {{$p->percentage}}%">
            <span>{{$p->percentage}}% </span>
        </div>
    </div>
        @endforeach
</div>
    @endif
