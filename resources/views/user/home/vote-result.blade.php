@if($voteresult)
    @foreach($voteresult as $v)
        <div class="tasks-progressbar v-result">
            <p>{{$v->title}}</p>
            <div class="progress">
                <div class="progress-bar progress-bar-success"
                     style="width: {{intval($v->percentage)}}%">
                    <span>{{intval($v->percentage)}}% </span>
                </div>
            </div>
        </div>
    @endforeach
@endif