@if(isset($ads) && $ads->count()>0)
    <div class="section-title">
        <h5>تبلیــغــات</h5>
    </div>
    <div class="ads text-center">
        @foreach($ads as $a)
            <a href="{{$a->href}}"><img class="img-responsive center-block" src="{{$a->img}}" alt="{{$a->title}}"
                                        title="{{$a->title}}"/></a>
        @endforeach
    </div>
@endif