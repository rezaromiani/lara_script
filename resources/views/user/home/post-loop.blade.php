@if($post && !empty($post))
    @foreach($post as $p)
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
            <div class="panel panel-default post-c">
                <div class="panel-post">
                    <img class="img-responsive" src="{{\App\Utility\PostUtility::getImgLink($p)}}">
                    <span class="i-date">{{\Morilog\Jalali\Jalalian::forge($p->created_at)->format('%B %d، %Y')}}<i
                            class="material-icons">date_range</i></span>
                    <div class="overlay">
                        <div class="text-ecxerpt">
                            {{mb_substr(trim($p->content,'<p>'), 0, 80, 'utf-8').'...' }}
                        </div>
                        <ul class="list-inline" id="post-detail">
                            <li>
                                <i class="material-icons">perm_identity</i><span>{{$p->User->username}}</span>
                            </li>
                            <li><i class="material-icons">remove_red_eye</i><span> {{$p->view}} </span>
                            </li>
                            <li><i class="material-icons">comment</i> <span> {{$p->Comment->count()}} </span></li>
                        </ul>
                    </div>
                </div>
                <div class="post-link"><a href="{{\App\Utility\PostUtility::getPostLink($p)}}">{{$p->title}}</a>
                </div>
                <div class="post-type-like">
                                            <span class="post-type">
                                                <i class="material-icons">view_headline</i>
                                                رایگان</span>
                    <span class="like-btn">
                                <i class="{{(\App\Utility\PostUtility::isUserDisLikePost($p))?'fas':'far'}} fa-thumbs-down dislike-icon dislikebtn"
                                   id=""
                                   data-postid="{{$p->id}}"></i>
                            <span class="dislike-result">
                            {{$p->Like->where('action',0)->count()}}
                            </span>
                            <span class="like-result">
                            {{$p->Like->where('action',1)->count()}}
                            </span>
                        <i class="{{(\App\Utility\PostUtility::isUserLikePost($p))?'fas':'far'}} fa-thumbs-up like-icon likebtn"
                           id="" data-postid="{{$p->id}}"></i>
                    </span>
                </div>
            </div>
        </div>
    @endforeach
    @include('user.post.postLikeAction')
@else
    <p>هیچ مطلبی وجود ندارد</p>
@endif
