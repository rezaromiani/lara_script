<div class="container-fluid" id="hero-header">
    <div class="row">
        {!! isset($options['main']['welcome'])?trim($options['main']['welcome'],'"'):'' !!}
        @if(isset($dis_count) && $dis_count)
            <h2 class="pulsate">{{$dis_count->title}}</h2>
        @endif
    </div>
    <script>
        let startCountDown ="" ;
        let endCountDown = "";
        let nowCountDown = "";
    </script>
    @if(isset($dis_count) && $dis_count)
        <div class="row">
            <div class="bounce text-center"><i class="material-icons">keyboard_arrow_down</i></div>
        </div>
        <div class="row">
            <div class="countdown countdown-container container" id="countdown-custom">
                <div class="clock row">
                    <div class="clock-item clock-seconds countdown-time-value col-sm-6 col-md-3">
                        <div class="wrap">
                            <div class="inner">
                                <div id="canvas-seconds" class="clock-canvas"></div>

                                <div class="text">
                                    <p class="val">0</p>
                                    <p class="type-seconds type-time">ثانیه</p>
                                </div><!-- /.text -->
                            </div><!-- /.inner -->
                        </div><!-- /.wrap -->
                    </div><!-- /.clock-item -->
                    <div class="clock-item clock-minutes countdown-time-value col-sm-6 col-md-3">
                        <div class="wrap">
                            <div class="inner">
                                <div id="canvas-minutes" class="clock-canvas"></div>

                                <div class="text">
                                    <p class="val">0</p>
                                    <p class="type-minutes type-time">دقیقه</p>
                                </div><!-- /.text -->
                            </div><!-- /.inner -->
                        </div><!-- /.wrap -->
                    </div><!-- /.clock-item -->
                    <div class="clock-item clock-hours countdown-time-value col-sm-6 col-md-3">
                        <div class="wrap">
                            <div class="inner">
                                <div id="canvas-hours" class="clock-canvas"></div>

                                <div class="text">
                                    <p class="val">0</p>
                                    <p class="type-hours type-time">ساعت</p>
                                </div><!-- /.text -->
                            </div><!-- /.inner -->
                        </div><!-- /.wrap -->
                    </div><!-- /.clock-item -->


                    <div class="clock-item clock-days countdown-time-value col-sm-6 col-md-3">
                        <div class="wrap">
                            <div class="inner">
                                <div id="canvas-days" class="clock-canvas"></div>

                                <div class="text">
                                    <p class="val">0</p>
                                    <p class="type-days type-time">روز</p>
                                </div><!-- /.text -->
                            </div><!-- /.inner -->
                        </div><!-- /.wrap -->
                    </div><!-- /.clock-item -->

                </div><!-- /.clock -->
            </div><!-- /.countdown-wrapper -->
        </div>
        <script>
             startCountDown = {!! json_encode(strtotime($dis_count->created_at)) !!};
             endCountDown = {!! json_encode(strtotime($dis_count->end_at)) !!};
             nowCountDown = {!! json_encode(\Carbon\Carbon::now()->timestamp) !!};
        </script>
    @endif

    <div class="row" id="custom-row-search-filed">
        <div class="col-sm-6 col-md-6 clo-lg-6 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
            <form action="{{route('all.post.search')}}" method="get" >
                <div class="input-group">
                    <input type="text" class="form-control" name="s" placeholder="دنبال چی می گردی؟">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info">بگــرد</button>
                    </span>
                </div>
            </form>

        </div>
    </div>
    <div class="row hidden-xs" id="custom-row-statictis">
        <div class="col-sm-6 col-md-6 col-lg-6 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 ">
            <div class="col-sm-4 col-md-4 col-lg-4"><i
                        class="material-icons">library_books</i><span>{{$statistics['post_count']}} مقالات آموزشی </span>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4"><i
                        class="material-icons">supervisor_account</i><span>{{$statistics['user_count']}} دانشجوها </span>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4"><i
                        class="material-icons">access_time</i><span>{{$statistics['view_count']}} دقیقه آموزش</span>
            </div>
        </div>

    </div>
</div>
