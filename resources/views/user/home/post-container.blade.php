<div class="container wrapper-margin-top ">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 ">
            <div class="content-header">مطالب آموزشی</div>
            <div class="main-content wrapper-free-styles">
                <div class="dropdown " id="custom-dropdown">

                    <button id="btn-filter-content" class="btn btn-default dropdown-toggle" type="button"
                            id="content-filter" data-toggle="dropdown">
                        فیلتر عناوین آموزشی...
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="#" class="load-post" data-type="1">آخرین مطالب</a></li>
                        <li><a href="#" class="load-post" data-type="2">داغ ترین مطالب</a></li>
                        <li><a href="#" class="load-post" data-type="3">پربازدید ترین مطالب</a></li>
                    </ul>
                    <img id="loading" src="/img/loader.gif">
                </div>

                <div class="load-content">
                    <div class="row">
                        <div class="post-load-content">

                                    @include('user.home.post-loop')

                        </div>
                    </div>

                </div>
                <div class="all-posts">
                    <a href="{{route('all.post.archive')}}" class="btn btn-success">همه مطالب<i class="material-icons">arrow_back</i></a>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function ($) {
        $(document).on('click', '.load-post', function (event) {
            event.preventDefault();
            var data = $(this).data('type');
            var url = {!! json_encode(route('post.load.jqload','')) !!}
            var load_content = $('.load-content');
            $('#loading').show();
            load_content.css("opacity", 0.5);
            $('.post-load-content').load(url + '/' + data,function (responseTxt, statusTxt, xhr) {
                $('#loading').hide();
                load_content.css("opacity", 1);
                if(statusTxt == "error")
                    console.log("Error: " + xhr.status + ": " + xhr.statusText);
            });
        });
    })
</script>