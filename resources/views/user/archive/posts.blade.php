@extends('layouts.user')
@section('content')
    <div class="container wrapper-margin-top ">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 ">
                <div class="content-header" id="content-header-all-post">{{$archive_title}}
                    <span class="count-topics">کل مطالب منتشر شده : {{$post->total()}}</span>
                </div>
                <div class="main-content wrapper-free-styles m-h-500">
                    <div class="load-content ">
                        <div class="row">
                            @include('user.home.post-loop')
                        </div>
                    </div>
                    <nav class="text-center active-pagination">

                       {{$post->links()}}

                    </nav>
                </div>

            </div>
        </div>
    </div>
@endsection