@extends('layouts.user')
@section('content')
    <div class="container wrapper-margin-top ">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 ">
                <div class="content-header" id="content-header-all-post">همه دوره های آموزشی

                </div>
                <div class="main-content wrapper-free-styles" style="min-height: 500px">
                    <div class="load-content ">
                        <div class="row">
                            @if($courses)
                                @foreach($courses as $p)
                                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                        <div class="panel panel-default post-c post-course">
                                            <div class="panel-post">
                                                <img class="img-responsive"
                                                     src="{{$p->image}}">
                                            </div>
                                            <div class="post-link"><a
                                                        href="{{route('course.single',[$p->id,$p->slug])}}">{{$p->title}}</a>
                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <nav class="text-center active-pagination">

                        {{$courses->links()}}

                    </nav>
                </div>

            </div>
        </div>
    </div>
@endsection