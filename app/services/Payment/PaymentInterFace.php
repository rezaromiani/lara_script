<?php


namespace App\services\Payment;


use Illuminate\Http\Request;

interface PaymentInterFace
{
    public function doPayment(array $params);

    public function verifyPayment(array $params,Request $request);
}