<?php


namespace App\services\Payment;


final class paymentFactory
{
    public static function factory(string $type): PaymentInterFace
    {
        if ($type == 'zarinpal') {
            return new ZarinPal();
        }
    }
}