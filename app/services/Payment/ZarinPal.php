<?php


namespace App\services\Payment;


use App\Model\Course;
use App\Model\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ZarinPal implements PaymentInterFace
{
    private $merchantID = 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX';

    public function doPayment(array $params)
    {
        $MerchantID = $this->merchantID; //Required
        $Amount = $params['price']; //Amount will be based on Toman - Required
        $Description = $params['description']; // Required
        $Email = $params['email']; // Optional
        $CallbackURL = $params['callback']; // Required


        $client = new \SoapClient('https://sandbox.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

        $result = $client->PaymentRequest(
            [
                'MerchantID' => $MerchantID,
                'Amount' => $Amount,
                'Description' => $Description,
                'Email' => $Email,
                'CallbackURL' => $CallbackURL,
            ]
        );


        if ($result->Status == 100) {
            Transaction::create([
                'user_id' => $params['user_id'],
                'course_id' => $params['course_id'],
                'price' => $params['price'],
                'order_number' => $params['order_number']
            ]);
            return redirect('https://sandbox.zarinpal.com/pg/StartPay/' . $result->Authority);
        } else {
            echo 'ERR: ' . $result->Status;
        }
    }

    public function verifyPayment(array $params, Request $request)
    {
        $MerchantID = $this->merchantID;
        $Amount = $params['price']; //Amount will be based on Toman
        $Authority = $request->query('Authority');

        if ($request->query('Status') == 'OK') {

            $client = new \SoapClient('https://sandbox.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

            $result = $client->PaymentVerification(
                [
                    'MerchantID' => $MerchantID,
                    'Authority' => $Authority,
                    'Amount' => $Amount,
                ]
            );

            if ($result->Status == 100) {
                Transaction::where([
                    ['user_id', '=', $params['user_id']],
                    ['course_id', '=', $params['course_id']],
                    ['order_number', '=', $params['order_number']],
                    ['price', '=', $params['price']],
                ])->update(['status' => 1, 'ref_number' => $result->RefID]);
                $course = Course::find($params['course_id']);
                $course->Students()->attach($params['user_id'],['created_at' => Carbon::now()]);
                return $result->RefID;
            } else {
                return false;
            }
        } else {

            echo 'Transaction canceled by user';
        }
    }
}