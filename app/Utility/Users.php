<?php
/**
 * Created by PhpStorm.
 * User: Reza
 * Date: 10/4/2018
 * Time: 4:20 PM
 */

namespace App\Utility;


use App\Model\Poll;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Users
{
    public static function getRole($role)
    {
       $arr = [
           'admin'=>'مدیر',
           'teacher'=>'مدرس',
           'writer'=>'نویسنده',
           'user'=>'مشارکت کننده',
        ];
        if(isset($arr[$role])){
            return $arr[$role];
        }
        return 'تعریف نشده';
    }

    public static function isUserVote(Poll $poll)
    {
        $pollch = $poll->whereHas('pollItemChoices', function ($query) {
            $query->where('user_id', '=', Auth::id());
        })->first();
        if ($pollch){
            return $pollch;
        }
        return false;
    }

    public static function getUserAvatarUrl($email, $s = 80)
    {
        $url = 'https://www.gravatar.com/avatar/';
        $url .= md5( strtolower( trim( $email ) ) );
        $url .= "?s=$s&d=mp&r=g";

        return $url;
    }
}