<?php
/**
 * Created by PhpStorm.
 * User: Reza
 * Date: 3/7/2019
 * Time: 2:02 PM
 */

namespace App\Utility;


use App\Model\Poll;
use Illuminate\Support\Facades\DB;

class Polls
{
    public static function getvoteRes(Poll $poll)
    {
        $res = DB::table('poll_items')
            ->select(
                DB::raw("poll_items.title,poll_items.id,COUNT(item_user_choices.id)*100/(
            SELECT COUNT(*) FROM item_user_choices WHERE item_user_choices.poll_id=" . $poll->id . ") AS percentage"))
            ->join('item_user_choices', 'poll_items.id', '=', 'item_user_choices.item_id')
            ->where('item_user_choices.poll_id', '=', $poll->id)
            ->groupBy('item_user_choices.item_id')
            ->get();
        return $res;

    }
}
