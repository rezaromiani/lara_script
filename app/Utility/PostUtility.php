<?php
/**
 * Created by PhpStorm.
 * User: Reza
 * Date: 2/1/2019
 * Time: 5:51 PM
 */

namespace App\Utility;


use App\Model\Comment;
use App\Model\Post;
use App\Model\PostLike;
use Illuminate\Support\Facades\Auth;

class PostUtility
{
    public static function getRelatedPost(Post $post)
    {
        $post = Post::where([
            ['cat_id', "=", $post->cat_id],
            ['id', "<>", $post->id],
        ])->orderBy('created_at', 'desc')->take(6)->get();
        if ($post)
            return $post;
        return false;
    }

    public static function getTag(Post $post)
    {
        $tag = explode(',', $post->tags);
        return $tag;
    }

    public static function getNewCommentCount()
    {
        return Comment::where('comment_status', 0)->count();
    }

    public static function getPostLink(Post $post)
    {
        return route('post.single', [$post->id, $post->slug]);
    }

    public static function getImgLink($p)
    {
        if (strstr( $p->thumb, "http"))
            return  $p->thumb;
        else
            return asset('storage/upload/image') . '/' . $p->thumb;
    }

    /**
     * @param Post $post
     * @return bool
     */
    public static function isUserLikePost(Post $post)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $post_id = $post->id;
            $post_like = PostLike::where([
                    ['post_id', '=', $post_id],
                    ['user_id', '=', $user->id],
                    ['action', '=', 1]
                ]
            )->get()->first();

            if ($post_like instanceof PostLike) {
                return true;
            }
        }
        return false;
    }

    public static function isUserDisLikePost(Post $post)
    {
        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $post_id = $post->id;
            $post_like = PostLike::where([
                ['post_id', '=', $post_id],
                ['user_id', '=', $user_id],
                ['action', '=', 0]
            ])->get()->first();
            if ($post_like instanceof PostLike) {
                return true;
            }
        }
        return false;
    }

    public static function getPostLikes(Post $post, $action)
    {
        return $post->Like->where('action', $action)->count();
    }
}
