<?php
/**
 * Created by PhpStorm.
 * User: Reza
 * Date: 4/5/2019
 * Time: 8:02 AM
 */

namespace App\Utility;


use App\Model\Ticket;
use App\Model\TicketMsg;
use App\User;

class TicketUtility
{
    public static function getNewMsgCount(Ticket $ticket, User $user)
    {
        $ticketCount = TicketMsg::where([
            ['status', '=', '1'],
            ['ticket_id', '=', $ticket->id],
            ['user_id', '<>', $user->id],
        ])->count('id');

        return $ticketCount ? $ticketCount : 0;
    }
}