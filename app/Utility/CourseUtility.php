<?php


namespace App\Utility;


use App\Model\Course;
use App\User;

class CourseUtility
{
    public static function getPrice(Course $course)
    {
        $discount = $course->discount;
        if ($discount > 0) {
            $price = ((100 - $discount) * $course->price) / 100;
            return '<div class="course-discount"><span>' . $discount . '</span>تخفیف %</div>
                    <span class="course-price">شهریه دوره :
                    <strike class="main-price">' . $course->price . '</strike>
                    <span>' . $price . '</span>
هزار تومان                    </span>                    ';
        }
        return ' <span class="course-price">شهریه دوره :
                    <span>' . $course->price . '</span>
هزار تومان                    </span>                    ';
    }

    public static function isUserCourseStudent($course_id)
    {
        if (\Auth::check()) {
            $res = auth()->user()->Course()->where('course_id', '=', $course_id)->limit(1)->get();
            return (intval($res->count())|| \Auth::user()->isAn('admin') || self::isCourseTeacher($course_id) ) ? true : false;
        }
        return false;
    }

    public static function calcPrice(Course $course)
    {
        $discount = $course->discount;
        $price = $course->price;
        if ($discount > 0) {
            $price = ((100 - $discount) * $course->price) / 100;
        }
        return $price;
    }

    public static function isCourseTeacher($course_id)
    {
        if (\Auth::check() && \Auth::user()->isAn('teacher')) {
            return in_array($course_id,\Auth::user()->Course_as_teacher->pluck('id')->toArray()) ? true : false;
        }
    }
}