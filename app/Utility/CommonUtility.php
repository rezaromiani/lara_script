<?php


namespace App\Utility;


class CommonUtility
{
    public static function getFileType($file)
    {
        $index = stripos($file, '.');
        return substr($file,$index+1);
    }
}