<?php
/**
 * Created by PhpStorm.
 * User: Reza
 * Date: 1/31/2019
 * Time: 6:49 PM
 */

namespace App\Utility;


use App\Model\Post;
use Illuminate\Database\Eloquent\Model;

class HeadTag
{
    public static function getTitle($post)
    {
        if ($post instanceof Model) {

            return config('app.name') . ' - ' . $post->title;
        }
        return config('app.name') . ' - ' . $post;
    }

    public static function getAdminTitle($p)
    {
        return $p;
    }
}