<?php
/**
 * Created by PhpStorm.
 * User: Reza
 * Date: 3/13/2019
 * Time: 4:40 PM
 */

namespace App\Utility;


use App\Model\Menu;

class menuUtility
{
    public static function updateMenu($menus, $parent_id = 0,$priority=1)
    {

        foreach ($menus as $m) {
            $menu = Menu::find($m['id']);
            $menu->update(['parent_id' => $parent_id,'priority'=>$priority]);
            $priority+=1;
            if (isset($m['children'])) {
                self::updateMenu($m['children'], $m['id'],$priority);
            }
        }
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public static function Menu()
    {
        $menus = Menu::orderBy('priority')->get()->groupBy('parent_id');

        return view('user.partials.top-menu-container',compact('menus'))->render();
    }
}