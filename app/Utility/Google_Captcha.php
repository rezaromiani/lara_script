<?php

namespace App\Utility;
class Google_Captcha
{
    public static function confirm($value)
    {

        $secret = config('app.google_captcha_secret_key');
        $response = $value;
        $ip = $_SERVER['REMOTE_ADDR'];
        $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response&remoteip=$ip";
        $result = json_decode(file_get_contents($url));
        if ($result->success) {
            return true;
        }
        return false;
    }
}