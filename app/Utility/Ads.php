<?php
/**
 * Created by PhpStorm.
 * User: Reza
 * Date: 3/8/2019
 * Time: 2:33 PM
 */

namespace App\Utility;


class Ads
{
    public static function date_to_arr($date = null, $sep = "/")
    {
        $res = explode($sep, $date);
        return $res;
    }
    public static function arr_to_date($arr = null, $sep = "-")
    {
        $res = join($sep,$arr);
        return $res;
    }
}