<?php

namespace App\Mail;

use App\Model\Comment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CommentNotification extends Mailable
{
    use Queueable, SerializesModels;
    public $comment;

    /**
     * Create a new message instance.
     *
     * @param Comment $comment
     */
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $post_link = route('post.single', [$this->comment->commentable->id, $this->comment->commentable->slug]).'#comment-'.$this->comment->id;
        return $this->view('email.commentStatusNotification')->with([
            'comment_txt' => $this->comment->comment,
            'post_link' => $post_link,
            'post_title' => $this->comment->commentable->title
        ]);
    }
}
