<?php

namespace App;

use App\Model\Comment;
use App\Model\Course;
use App\Model\ItemUserChoice;
use App\Model\Post;
use App\Model\PostLike;
use App\Model\Ticket;
use App\Model\TicketMsg;
use App\Model\Transaction;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Silber\Bouncer\Database\HasRolesAndAbilities;

class User extends Authenticatable
{
    use Notifiable;
    use HasRolesAndAbilities;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }


    public function post()
    {
        return $this->hasMany(Post::class, 'author_id', 'id');
    }

    public function Comment()
    {
        return $this->hasMany(Comment::class, 'user_id', 'id');
    }

    public function ticket()
    {
        return $this->hasMany(Ticket::class, 'user_id', 'id');
    }

    public function ticketMsg()
    {
        return $this->hasMany(TicketMsg::class, 'user_id', 'id');
    }

    public function PostLike()
    {
        return $this->hasMany(PostLike::class, 'user_id', 'id');
    }

    public function pollItemChoices()
    {
        return $this->hasMany(ItemUserChoice::class, 'user_id', 'id');
    }

    public function FullName()
    {
        return $this->attributes['first_name'] . ' ' . $this->attributes['last_name'];
    }

    public function Course()
    {
        return $this->belongsToMany(Course::class, 'user_course')->withPivot('created_at');
    }

    public function Transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function Course_as_teacher()
    {
        return $this->hasMany(Course::class, 'teacher_id', 'id');
    }
}
