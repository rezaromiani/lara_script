<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Progress extends Model
{
    protected $table = 'progress';

    protected $fillable = ['title', 'color', 'percentage'];

    public $timestamps = false;
}
