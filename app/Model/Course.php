<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';

    protected $fillable = ['title', 'demo_url', 'course_length', 'start_data', 'status', 'price', 'discount', 'image', 'teacher_id', 'prerequisites', 'Headlines', 'slug'];

    public function Teacher()
    {
        return $this->belongsTo(User::class, 'teacher_id', 'id');
    }

    public function Students()
    {
        return $this->belongsToMany(User::class, 'user_course')->withPivot('created_at');
    }

    public function Comment()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function Sessions()
    {
        return $this->hasMany(CourseSession::class, 'course_id', 'id');
    }
}
