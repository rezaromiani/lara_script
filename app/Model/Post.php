<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

class Post extends Model
{
    protected $guarded = ['id'];

    public function Category()
    {
        return $this->belongsTo(Category::class, 'cat_id', 'id');
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'author_id', 'id');
    }

    public function Comment()
    {
        return $this->morphMany(Comment::class,'commentable');
    }

    public function Like()
    {
        return $this->hasMany(PostLike::class, 'post_id', 'id');
    }
}
