<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PostLike extends Model
{
    const like = 1;
    const dislike = 0;

    protected $guarded = ['id'];
    public $timestamps = false;

    public function Post()
    {
        return $this->belongsTo(Post::class, 'post_id', 'id');
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
