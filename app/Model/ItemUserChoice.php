<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ItemUserChoice extends Model
{
    public $timestamps = false;
    protected $table = 'item_user_choices';
    protected $guarded = ['id'];

    public function poll()
    {
        return $this->belongsTo(Poll::class, 'poll_id', 'id');
    }
    public function Item()
    {
        return $this->belongsTo(PollItem::class, 'item_id', 'id');
    }
    public function User()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
