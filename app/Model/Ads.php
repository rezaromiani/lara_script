<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    public $timestamps = null;
    protected $table = "ads";
    protected $guarded = ['id'];
}
