<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PollItem extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    public function Poll()
    {
        return $this->belongsTo(Poll::class, 'poll_id', 'id');
    }
    public function pollItemChoices(){
        return $this->hasMany(ItemUserChoice::class,'poll_id','id');
    }
}
