<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    protected $guarded = ['poll_id'];
    const ACTIVE = 1;
    const DEACTIVATE = 0;

    public function PollItem()
    {
        return $this->hasMany(PollItem::class, 'poll_id', 'id');
    }

    public function pollStatus()
    {
        switch ($this->attributes['status']) {
            case self::ACTIVE:
                return 'فعال';
                break;
            case self::DEACTIVATE:
                return 'غیر فعال';
                break;
        }
    }

    public function pollItemChoices(){
        return $this->hasMany(ItemUserChoice::class,'poll_id','id');
    }
}
