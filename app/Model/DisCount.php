<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DisCount extends Model
{
    protected $table = 'discounts';

    protected $fillable = ['title', 'end_at'];
}
