<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class TicketMsg extends Model
{
    protected $table = 'tickets_msg';
    protected $guarded = ['id'];

    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'ticket_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
