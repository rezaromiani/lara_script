<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

class Transaction extends Model
{
    protected $table = 'transactions';

    protected $fillable = ['user_id', 'course_id', 'ref_number', 'order_number', 'price', 'status'];

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
