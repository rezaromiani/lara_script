<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $guarded = ['id'];

    public $timestamps = false;

    public function posts(){
        return $this->hasMany(Post::class,'cat_id','id');
    }
}
