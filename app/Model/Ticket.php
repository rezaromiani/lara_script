<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    const NORMAL=1;
    const IMPORTANT=2;
    const VERYIMPORTANT=3;

    protected $guarded = ['id'];
    protected $table = 'tickets';

    public function ticketMsg()
    {
        return $this->hasMany(TicketMsg::class, 'ticket_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function ticket_format()
    {
        switch ($this->attributes['priority']) {
            case self::NORMAL:
                return 'عادی';
                break;
            case self::IMPORTANT:
                return 'مهم';
                break;
            case self::VERYIMPORTANT:
                return 'بسیار مهم';
                break;
            default:
                return 'تعریف نشده';
        }
    }
}
