<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CourseSession extends Model
{
    protected $table = 'course_sessions';

    protected $fillable =['title','content','video_url','file_url','thumb','course_id','slug','view','status','video_time'];

    public function Course()
    {
        return $this->belongsTo(Course::class,'course_id','id');
    }
    public function Comment()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
