<?php

namespace App\Http\Controllers\user;

use App\Model\Course;
use App\Model\CourseSession;
use App\Utility\CourseUtility;
use App\Utility\HeadTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CoursesController extends Controller
{
    public function index(Course $course, $title)
    {
        CourseUtility::isUserCourseStudent(auth()->user(), $course->id);
        $sessions = CourseSession::where([
            ['course_id', '=', $course->id],
            ['status', '=', 1]
        ])->select('slug', 'title', 'video_time', 'id')->orderBy('created_at', 'asc')->get();
        $allPostComment = $course->Comment()->where('comment_status', 1)->get()->groupBy('comment_parent_id');
        $title = HeadTag::getTitle($course);

        return view('user.Course.single.single', compact('course','title', 'allPostComment', 'sessions'));
    }

    public function sendToCheckout(Course $course,Request $request)
    {
        $this->validate(request(), [
            'course' => 'required|numeric'
        ]);
        $request->session()->flash('checkout', $course->id);
        return redirect()->route('checkout');
    }
}
