<?php

namespace App\Http\Controllers\user;

use App\Model\Ads;
use App\Model\Comment;
use App\Model\Course;
use App\Model\CourseSession;
use App\Model\DisCount;
use App\Model\Notification;
use App\Model\Option;
use App\Model\Poll;
use App\Model\Post;
use App\Model\Progress;
use App\Model\Slider;
use App\Model\Transaction;
use App\User;
use App\Utility\Polls;
use App\Utility\Users;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $voteresult = null;
        $lastPost = CourseSession::take(4)->latest()->get();
        $randomPost = Post::take(8)->inRandomOrder()->get();

        $ads = Ads::whereDate('start_date', '<=', Carbon::now()->format('Y-m-d'))->whereDate('expire_date', '>=', Carbon::now()->format('Y-m-d'))->get();
        $progress = Progress::all();

        $post = Post::where('status', 1)->orderBy('created_at', 'desc')->take(8)->get();

        $vote = Poll::with('PollItem')->where('status', '=', '1')->whereDate('expire_at', '>=', Carbon::now()->format('Y-m-d H:i:s'))->orderBy('created_at', 'desc')->first();

        if ($vote instanceof Poll && Users::isUserVote($vote)) {
            $voteresult = Polls::getvoteRes($vote);
        }
        $slides = Slider::orderBy('created_at', 'desc')->get();
        $notifications = Notification::orderBy('created_at', 'desc')->take(3)->get();
        $course = Course::with('Teacher')->orderBy('created_at', 'desc')->take(4)->get();
        $statistics = $this->statistics();
        $dis_count = DisCount::where('end_at', '>', Carbon::now())->orderBy('created_at', 'desc')->first();


        return view('user.home.home', compact('dis_count', 'statistics', 'post', 'progress', 'lastPost', 'randomPost', 'course', 'vote', 'voteresult', 'ads', 'slides', 'notifications'));
    }

    private function statistics()
    {
        return [
            'post_count' => Post::count('id') + CourseSession::count('id'),
            'user_count' => User::count('id'),
            'view_count' => CourseSession::sum('video_time'),
        ];
    }
}
