<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Model\Comment;
use App\Model\Course;
use App\Model\CourseSession;
use App\Model\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class CommentsController extends Controller
{
    public function saveComment(Request $request)
    {
        if (Auth::check()) {
            $this->validate($request, [
                'post_id' => 'required',
                'parent_id' => 'required',
                'text' => 'required',
                'type' => 'required'
            ]);
            $type = $this->getType($request->input('type'));
            $post = $type::find($request->input('post_id'));
            if ($post) {
                $comment = Comment::create([
                    'commentable_id' => $request->input('post_id'),
                    'comment_parent_id' => $request->input('parent_id'),
                    'comment' => $request->input('text'),
                    'user_id' => Auth::user()->id,
                    'commentable_type' => $type
                ]);
                return Response::json(['success' => true, 'msg' => ' نظر شما با موفقیت ثبت شد.']);
            }
        }
    }

    private function getType($type)
    {
        switch ($type) {
            case 'Course':
                return Course::class;
                break;
            case 'post':
                return Post::class;
                break;
            case 'session':
                return CourseSession::class;
                break;

        }
    }
}
