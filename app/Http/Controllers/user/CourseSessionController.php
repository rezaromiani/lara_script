<?php

namespace App\Http\Controllers\user;

use App\Model\CourseSession;
use App\Utility\DownloadHelper;
use App\Utility\HeadTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseSessionController extends Controller
{
    public function index(CourseSession $courseSession)
    {
        $allPostComment = $courseSession->Comment()->where('comment_status', 1)->get()->groupBy('comment_parent_id');
        $sessions = CourseSession::where('course_id','=',$courseSession->course_id)->select('title','slug','id')->get();
        $title = HeadTag::getTitle($courseSession);
        return view('user.Course.session.single',compact('courseSession','title','allPostComment','sessions'));
    }
    public function dowloadPostVideo($file_name)
    {
        $filePath = storage_path('app/public/upload/file/' . $file_name);
        $download = new DownloadHelper($filePath);
        $download->process();
    }

}
