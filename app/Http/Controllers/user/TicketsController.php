<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Model\Ticket;
use App\Model\TicketMsg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class TicketsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'ticket_title' => 'required',
            'ticket_text' => 'required',
            'ticket_priority' => 'required',
        ]);

        Auth::check() || Response::json(['error' => true], 500);

        $user = Auth::user()->id;

        $Ticket = Ticket::create([
            'title' => $request->input('ticket_title'),
            'user_id' => $user,
            'priority' => $request->input('ticket_priority')
        ]);

        $Ticket->ticketMsg()->save(new TicketMsg([
                'content' => $request->input('ticket_text'),
                'user_id' => $user
            ]
        ));
        if ($Ticket) {
            $tickets = Ticket::where('user_id','=',$user)->orderBy('created_at', 'desc')->get();
            $view = view('user.account.allticketloop', compact('tickets'))->render();
            return Response::json(['success' => true, 'view' => $view]);
        }
        return Response::json(['error' => true], 500);
    }

    public function answer(Request $request)
    {
        $this->validate($request, [
            'ticket_text' => 'required',
            'ticket_id' => 'required',
        ]);
        Auth::check() || Response::json(['error' => true], 500);

        $user = Auth::user()->id;
        $ticket_answer = TicketMsg::create([
            'ticket_id' => $request->input('ticket_id'),
            'content' => $request->input('ticket_text'),
            'user_id' => $user
        ]);
        if ($ticket_answer) {
            $ti = Ticket::find($request->input('ticket_id'));
            $view = view('user.account.answermsg', compact('ti'))->render();
            return Response::json(['success' => true, 'view' => $view]);
        }
        return Response::json(['error' => true], 500);
    }
}
