<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Model\Post;
use App\model\PostLike;
use App\Utility\PostUtility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class PostLikesController extends Controller
{
    public function like(Request $request)
    {
        $this->validate($request, [
            'post_id' => 'required',
            'action' => 'required'
        ]);
        if (Auth::check()) {
            $action = $request->input('action');
            $Like = null;
            $del=false;
            $post=Post::find($request->input('post_id'));
            $user_id=Auth::user()->id;
            $msg="نظر شما با موفقیت ثبت شد.";
            switch ($action) {
                case 1:
                    $Like = PostLike::updateOrCreate(
                        ['post_id' => $request->input('post_id'), 'user_id' => $user_id],
                        ['action' => PostLike::like]
                    );

                    $countlike=PostUtility::getPostLikes($post,1);
                    $countdislike=PostUtility::getPostLikes($post,0);
                    break;
                case 0:
                    $Like = PostLike::updateOrCreate(
                        ['post_id' => $request->input('post_id'), 'user_id' => $user_id],
                        ['action' => PostLike::dislike]
                    );
                    $countlike=PostUtility::getPostLikes($post,1);
                    $countdislike=PostUtility::getPostLikes($post,0);
                    break;
                case 2:
                    PostLike::where([
                        ['post_id', '=', $request->input('post_id')],
                        ['user_id', '=', $user_id],
                    ])->delete();
                    $countlike=PostUtility::getPostLikes($post,1);
                    $countdislike=PostUtility::getPostLikes($post,0);
                    $del=true;
                    $msg="نظر شما با موفقیت حذف شد.";
                    break;
            }

            if ($Like instanceof PostLike || $del) {

                return Response::json(['success' => true, 'msg' => $msg,'likecount'=>$countlike,'dislikecount'=>$countdislike]);
            }
        } else {
            return Response::json(['error' => true, 'msg' => 'لطفا برای ثبت نظر وارد سایت شوید.']);
        }
    }
}
