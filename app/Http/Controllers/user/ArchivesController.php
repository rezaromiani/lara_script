<?php

namespace App\Http\Controllers\user;

use App\Model\Course;
use App\Model\Post;
use App\Utility\HeadTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArchivesController extends Controller
{
    /**
     * show all post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function allPost()
    {
        $title = HeadTag::getTitle('آرشیو مقالات');
        $archive_title       = 'همه مطالب آموزشی';
        $post=Post::orderBy('created_at','desc')->paginate(8);
        return view('user.archive.posts',compact('title','archive_title','post'));
    }

    /**
     * show all course
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function allCourse()
    {
        $title = HeadTag::getTitle('دوره های آموزشی');
        $courses=Course::orderBy('created_at','desc')->paginate(8);
        return view('user.archive.course',compact('courses','title'));
    }

    /**
     * show all post with this tag
     * @param $tag
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tagPost($tag)
    {
        $title = HeadTag::getTitle(' آرشیو مقالات برچسب '.$tag);
        $archive_title = 'همه مطالب برچسب: '.$tag;
        $post=Post::where('tags','LIKE','%'.$tag.'%')->orderBy('created_at','desc')->paginate(8);
        $post_count=Post::count('id');
        return view('user.archive.posts',compact('title','archive_title','post','post_count'));
    }

    /**
     * show search result
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search()
    {
        $search = request()->input('s');
        $title = HeadTag::getTitle(' آرشیو مقالات : '.$search);
        $archive_title = 'جستجو براساس: '.$search;

        $post=Post::where('title','LIKE','%'.$search.'%')->orWhere('content','LIKE','%'.$search.'%')->orWhere('tags','LIKE','%'.$search.'%')->orderBy('created_at','desc')->paginate(8);
        return view('user.archive.posts',compact('title','archive_title','post','post_count'));

    }
}
