<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Model\Post;
use App\Utility\DownloadHelper;
use App\Utility\HeadTag;
use App\Utility\PostUtility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function post($post_id, $post_slug)
    {

        $post = Post::where([
            ['id', '=', $post_id],
            ['slug', '=', $post_slug],
        ])->firstOrFail();
        $post->increment('view');
        $title = HeadTag::getTitle($post);
        $relatedPost = PostUtility::getRelatedPost($post);
        $tag = PostUtility::getTag($post);
        $allPostComment = $post->Comment()->where('comment_status', 1)->get()->groupBy('comment_parent_id');
        return view('user.post.single', compact('title', 'post', 'relatedPost', 'tag', 'allPostComment'));
    }


    public function load_Post(Request $request, $type)
    {
        $type = intval($type);
        $post = null;
        switch ($type) {
            case 1:
                $post = Post::take(8)->latest()->get();
                break;
            case 2:
                $ids = DB::table('comments')
                    ->select(DB::raw('count(*) as post_count, post_id'))
                    ->orderBy('post_count', 'desc')
                    ->groupBy('post_id')
                    ->pluck('post_id')->toArray();
                $post = Post::find($ids);
                break;
            case 3:
                $post = Post::take(8)->orderBy('view', 'desc')->get();
                break;
        }

        return view('user.home.post-loop', compact('post'));
    }
}
