<?php

namespace App\Http\Controllers\user;

use App\Model\Course;
use App\services\Payment\paymentFactory;
use App\Utility\CourseUtility;
use App\Utility\HeadTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class paymentsController extends Controller
{
    public function checkout()
    {
        $checkout = session('checkout');
        if ($checkout) {
            $course = Course::find($checkout);
            $title = HeadTag::getTitle('تایید نهایی');

            return view('user.payment.checkout', compact('order', 'course','title'));
        }
        return redirect()->route('user.home');
    }

    public function payment()
    {
        $this->validate(request(), [
            'course' => 'required|numeric'
        ]);
        $user = auth()->user();
        $course = Course::find(request()->input('course'));
        $arr = [
            'user_id' => $user->id,
            'course_id' => $course->id,
            'price' => CourseUtility::calcPrice($course),
            'order_number' => intval(microtime(true)) . $user->id,
            'description' => $course->title,
            'callback' => route('verifyPayment'),
            'email' => $user->email
        ];
        session(['order' => $arr]);
        $payment = paymentFactory::factory('zarinpal');
       return $payment->doPayment($arr);
    }

    public function verifyPayment(Request $request)
    {
        $arr = session('order');
        $payment = paymentFactory::factory('zarinpal');
        $order_number = $arr['order_number'];
        $c_id=$arr['course_id'];
        $c_title=$arr['description'];
        $request->session()->forget('order');
        $res = $payment->verifyPayment($arr,$request);

           return view('user.payment.success',compact('res','order_number','c_id','c_title'));

    }
}
