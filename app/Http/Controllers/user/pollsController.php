<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Model\ItemeUserChoices;
use App\Model\ItemUserChoice;
use App\Model\Poll;
use App\Utility\Polls;
use App\Utility\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class pollsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function vote(Request $request)
    {
        if (Auth::check()) {
            $this->validate($request, [
                'item' => 'required|numeric',
                'poll' => 'required|numeric'
            ]);
            $item = $request->input('item');
            $poll = $request->input('poll');
            $p=Poll::find($poll);

            if (!Users::isUserVote($p)) {
                $user = Auth::user();
                $user->pollItemChoices()->save(new ItemUserChoice(['poll_id' => $poll, 'item_id' => $item]));
                $voteresult=Polls::getvoteRes($p);
                $dis = view('user.home.vote-result',compact('voteresult'))->render();
                return Response::json(['success' => true,'result'=>$dis]);
            }
            return Response::json(['voted' => true]);
        }
        return Response::json([], 500);
    }
}
