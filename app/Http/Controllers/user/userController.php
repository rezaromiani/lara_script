<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Model\Ads;
use App\Model\Course;
use App\Model\Poll;
use App\Model\Post;
use App\Model\Ticket;
use App\User;
use App\Utility\HeadTag;
use App\Utility\Polls;
use App\Utility\Users;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\Request;

class userController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $tickets = Ticket::where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->get();
        $course = $user->Course;
        $transactions = $user->Transactions;
        $title = HeadTag::getTitle('پروفایل');
        return view('user.account.index', compact('user','title', 'tickets','course','transactions'));
    }


    public function edit(Request $request)
    {
        $this->validate($request, [
            'fname' => 'required',
            'lname' => 'required',
            'uname' => 'required'
        ]);
        if (!Auth::check()) {
            return Response::json(['error' => true], 500);
        }
        $user = Auth::user();
        $res = $user->update([
            'first_name' => $request->input('fname'),
            'last_name' => $request->input('lname'),
            'username' => $request->input('uname'),
        ]);
        return $res ? Response::json(['success' => true]) : Response::json(['error' => true], 500);
    }

    public function chpass()
    {
        $this->validate(\request(), [
            'password' => 'required|confirmed|min:5',
        ]);
        $user = Auth::user();
        $res = $user->update([
            'password' => \request()->input('password'),
        ]);
        return redirect()->back();
    }
}
