<?php

namespace App\Http\Controllers\authentication;

use App\Http\Controllers\Controller;
use App\Mail\resetPass;
use App\User;
use App\Utility\Google_Captcha;
use App\Utility\HeadTag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class forgetPassController extends Controller
{
    public function forgetPass()
    {
        $title = HeadTag::getTitle('فراموشی رمز عبور');
        return view('user.auth.resetPass',compact('title'));
    }

    public function DoForgetPass(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'g-recaptcha-response' => 'required',
        ]);
        $result = Google_Captcha::confirm($request->input('g-recaptcha-response'));
        if ($result) {
            $user = User::where('email', $request->input('email'))->first();
            if ($user instanceof User) {
                $token = Str::random(60);
                DB::table('password_resets')->insert([
                    'email' => $request->input('email'),
                    'token' => $token,
                    'created_at' => Carbon::now()
                ]);
                Mail::to($user)->send(new resetPass($token));
                return view('user.auth.resetPass')->with('success', 'ایمیل تغییر رمز به ایمیل شما ارسال شد و تا 24 ساعت اعتبار دارد.');
            } else {
                return view('user.auth.resetPass')->with('error', 'مشکلی در ارسال ایمیل پیش امده');
            }
        }

    }

    public function resetPass(Request $request, $key)
    {
        $userToken = DB::table('password_resets')->where('token', '=', $key)->first();
        if ($userToken) {
        $title = HeadTag::getTitle('فراموشی رمز عبور');
            return view('user.auth.forgetPass',compact('title'));
        } else {
            return redirect()->route('user.home');
        }
    }

    public function DoresetPass(Request $request, $key)
    {
        $userToken = DB::table('password_resets')->where('token', '=', $key)->first();
        if ($userToken) {
            $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required|confirmed',
            ]);
            if ($userToken->email == $request->input('email')) {
                $user = User::where('email', $request->input('email'))->first();
                if ($user instanceof User) {
                    $user->update([
                        'password' => $request->input('password')
                    ]);
                    DB::table('password_resets')->where('token', '=', $key)->delete();
                    return redirect()->route('auth')->with('success', 'پسورد شما با موفقیت تغییر یافت');
                }

            } else {
                return view('user.auth.forgetPass')->with(['error' => 'ایمیل وارد شده معتبر نیست.']);
            }
        } else {
            return redirect()->route('user.home');
        }
    }
}
