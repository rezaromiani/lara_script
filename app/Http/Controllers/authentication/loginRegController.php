<?php

namespace App\Http\Controllers\authentication;


use App\Utility\Google_Captcha;
use App\Utility\HeadTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\userReg;
use Illuminate\Support\Str;
use Silber\Bouncer\Bouncer;

class loginRegController extends Controller
{
    public function auth()
    {
        $title = HeadTag::getTitle('ورود-عضویت');
        return view('user.auth.auth',compact('title'));
    }

    public function doLogReg(Request $request)
    {
        if ($request->has('doLog')) {
            $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required',
                'g-recaptcha-response' => 'required'
            ]);
            $email = $request->input('email');
            $password = $request->input('password');
            $remmemberMe = ($request->has('remmemberMe')) ? true : false;
            $response = $request->input('g-recaptcha-response');
//            $result = Google_Captcha::confirm($response);
            if (true) {
                if (Auth::attempt(['email' => $email, 'password' => $password, 'status' => 1], $remmemberMe)) {
                    return redirect()->route('user.home');
                } else {
                    return view('user.auth.auth')->with('error_login', 'نام کاربری یا رمز عبور شما صحیح نیست.');
                }
            }
        }

        if ($request->has('doReg')) {

            $this->validate($request, [
                'firstName' => 'required|min:3',
                'lastName' => 'required|min:3',
                'username' => 'required|min:5',
                'email-reg' => 'required|email|confirmed|unique:users,email',
                'password-reg' => 'required|confirmed'
            ]);
            $activation_key = Str::random(50);
            $user = User::create([
                'username' => $request->input('username'),
                'first_name' => $request->input('firstName'),
                'last_name' => $request->input('lastName'),
                'email' => $request->input('email-reg'),
                'password' => $request->input('password-reg'),
                'status' => 0,
                'activation_key' => $activation_key
            ]);
            $user->assign('user');
            Mail::to($user)->send(new userReg($user));
            return view('user.auth.auth')->with('success', 'شما با موفقیت در سایت ثبت نام کردید.ایمیل فعالسازی به ایمیل شما ارسال شد لطفا برای فعالسازی حساب کاربری خود اقدام کنید.');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('user.home');

    }

    public function activation_user(Request $request, $key)
    {
        $user = User::where('activation_key', $key)->limit(1)->latest()->first();
        if ($user instanceof User) {
            $user->update([
                'activation_key' => 1,
                'status' => 1
            ]);
            return view('user.auth.activa', compact('user'));
        }
        return redirect()->route('user.home');
    }}
