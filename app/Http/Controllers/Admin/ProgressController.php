<?php

namespace App\Http\Controllers\Admin;

use App\Model\Progress;
use App\Utility\HeadTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProgressController extends Controller
{
    public function index()
    {
        $title = HeadTag::getAdminTitle('مدیریت پیشرفه دوره ها');
        $progress = Progress::all();
        return view('admin.progress.index', compact('title', 'progress'));
    }

    public function edit()
    {
        $this->validate(\request(), [
            'title' => 'required',
            'percent' => 'required',
            'color' => 'required',
        ]);
        $res = Progress::where('id', '=', \request()->input('title'))->update([
            'percentage' => request()->input('percent'),
            'color' => request()->input('color'),
        ]);
        if ($res) {
            return redirect()->route('admin.progress.index')->with('success', 'پیشرفت دوره با موفقیت ذخیره شد.');
        }
    }

    public function create()
    {
        $title = HeadTag::getAdminTitle('ایجاد پیشرفت دوره');
        return view('admin.progress.create', compact('title'));
    }

    public function storage()
    {
        $this->validate(\request(), [
            'title' => 'required',
            'percent' => 'required',
            'color' => 'required',
        ]);
        $result = Progress::create([
            'title' => request()->input('title'),
            'percentage' => request()->input('percent'),
            'color' => request()->input('color'),

        ]);
        if ($result instanceof Progress) {
            return redirect()->route('admin.progress.create')->with('success', 'پیشرفت دوره با موفقیت اضافه شد.');
        }
    }

    public function delete($progress)
    {
        Progress::destroy($progress);
        return redirect()->back();
    }
}
