<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Utility\HeadTag;
use App\Utility\Users;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{

    public function edit()
    {
        $user = \Auth::user();
        $page_title = 'پروفایل کاربر';
        $title = HeadTag::getAdminTitle('پروفایل کاربر');

        return view('admin.profile.form', compact('user','page_title','title'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'username' => 'required',
            'email' => 'required|email|confirmed',
        ]);
        $data = [
            'username' => $request->input('username'),
            'first_name' => $request->input('firstName'),
            'last_name' => $request->input('lastName'),
            'email' => $request->input('email'),
            'mobile' => $request->input('mobile'),
        ];
        if ($request->has('password') && !empty($request->input('password'))) {
            $data['password'] = $request->input('password');
        }
        if ($request->has('Resumes') && !empty($request->input('Resumes'))) {
            $data['Resumes'] = $request->input('Resumes');
        }
        $user = \Auth::user();
        if ($user instanceof User) {
            $res = $user->update($data);
            if ($res) {
                return redirect()->route('admin.user.profile')->with('success', 'عملیات با موفقیت انجام شد.');
            }
                return redirect()->route('admin.user.profile')->with('error', 'مشکلی در انجام عملیات پیش امده.');
        }
    }

}
