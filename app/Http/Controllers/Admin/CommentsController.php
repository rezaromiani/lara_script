<?php

namespace App\Http\Controllers\Admin;

use App\Events\CommentStatus;
use App\Http\Controllers\Controller;
use App\Model\Comment;
use App\Utility\HeadTag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class CommentsController extends Controller
{
    public function index()
    {
        $title = HeadTag::getAdminTitle('نظرات کاربران');

        $Comment = Comment::orderBy('created_at', 'desc')->paginate(20);
        return view('admin.comment.comment', compact('Comment','title'));
    }

    public function delete(Request $request, $id)
    {
        if (intval($id))
            Comment::destroy($id);
        return redirect()->route('admin.comment.index')->with('success', 'کامنت با موفقیت حذف شد.');
    }

    public function edit(Request $request)
    {
        $this->validate($request, [
            'comment-edit-text' => 'required',
            'comment-edit-id' => 'required|numeric'
        ]);
        $comment = Comment::find($request->input('comment-edit-id'));
        if ($comment instanceof Comment) {
            $comment->update([
                'comment' => $request->input('comment-edit-text')
            ]);
        }
        return redirect()->back();
    }

    public function updateStatus(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|numeric',
            'status' => 'required|in:publish,pending'
        ]);
        $status = $request->input('status');
        $comment = Comment::find($request->input('id'));
        if ($status == 'publish') {
            $comment->update(['comment_status' => 1]);
            event(new CommentStatus($comment));
        } elseif ($status == 'pending') {
            $comment->update(['comment_status' => 0]);
        }
        return Response::json(['success' => true]);
    }

    public function reply(Request $request)
    {
        $this->validate($request, [
            'post_id' => 'required',
            'parent_id' => 'required',
            'text' => 'required',
            'type'=>'required'
        ]);
        $Comment = Comment::create([
            'commentable_id' => $request->input('post_id'),
            'comment_parent_id' => $request->input('parent_id'),
            'comment' => $request->input('text'),
            'user_id' => Auth::user()->id,
            'comment_status' => 1,
            'commentable_type' => $request->input('type')
        ]);
        return redirect()->back();
    }
}
