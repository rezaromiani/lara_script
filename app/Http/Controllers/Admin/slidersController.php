<?php

namespace App\Http\Controllers\Admin;

use App\Model\Slider;
use App\Utility\HeadTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class slidersController extends Controller
{
    public function __construct()
    {
        \View::composers([
            'App\Http\View\Composers\fileManagerComposer' => 'admin.slider.index'
        ]);
    }
    public function index(){
        $slides = Slider::all();
        $title = HeadTag::getAdminTitle('اسلایدر');

        return view('admin.slider.index',compact('slides','title'));
    }
    public function create(){
        $this->validate(request(),[
            'slide'=>'required',
            'slideUrl'=>'required'
        ]);
        $result = Slider::create([
            'img' => request()->input('slide'),
            'url' => request()->input('slideUrl'),
        ]);
        if ($result instanceof Slider) {
            return redirect()->back();
        }
    }
    public function edit(Slider $slider){
        $slides = Slider::all();
        $title = HeadTag::getAdminTitle('ویرایش اسلایدر');

        return view('admin.slider.index',compact('slides','slider','title'));
    }
    public function SaveEdit(Slider $slider){
        $this->validate(request(),[
            'slide'=>'required',
            'slideUrl'=>'required'
        ]);
        $slider->update([
            'img' => request()->input('slide'),
            'url' => request()->input('slideUrl'),
        ]);
        return redirect()->route('admin.slider.index');
    }
    public function delete($slider){
        Slider::destroy($slider);
        return redirect()->back();
    }
}
