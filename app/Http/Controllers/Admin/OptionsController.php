<?php

namespace App\Http\Controllers\Admin;

use App\Model\Option;
use App\Utility\HeadTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OptionsController extends Controller
{
    public function index()
    {
        $title = HeadTag::getAdminTitle('تنظیمات عمومی سایت');
        $options = Option::find('options');
        if ($options)
            $options = $options->value;

        return view('admin.option.index', compact('title', 'options'));
    }

    public function storage()
    {
        dd(\request()->input('options'));
        $this->validate(\request(), [
            'options' => 'required'
        ]);
        $res = Option::updateOrCreate(
            ['name' => 'options'],
            ['value' => \request()->input('options')]
        );
        return redirect()->back();
    }
}
