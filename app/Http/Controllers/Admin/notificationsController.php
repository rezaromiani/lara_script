<?php

namespace App\Http\Controllers\Admin;

use App\Model\Notification;
use App\Utility\HeadTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class notificationsController extends Controller
{

    public function index(){
        $notifications = Notification::all();
        $title = HeadTag::getAdminTitle('اطلاعیه ها');

        return view('admin.notifications.index',compact('notifications','title'));
    }
    public function create(){
        $this->validate(request(),[
            'text'=>'required',
        ]);
        $result = Notification::create([
            'text' => request()->input('text'),
        ]);
        if ($result instanceof Notification) {
            return redirect()->back();
        }
    }
    public function edit(Notification $notification){
        $notifications = Notification::all();
        $title = HeadTag::getAdminTitle('اطلاعیه ها');
        return view('admin.notifications.index',compact('notifications','notification','title'));
    }
    public function SaveEdit(Notification $notification){
        $this->validate(request(),[
            'text'=>'required',
        ]);
        $notification->update([
            'text' => request()->input('text'),
        ]);
        return redirect()->route('admin.notification.index');
    }
    public function delete($notification){
        Notification::destroy($notification);
        return redirect()->back();
    }
}
