<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCourseRequest;
use App\Http\Requests\DeleteManyItemRequest;
use App\Model\Course;
use App\User;
use App\Utility\HeadTag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CourseController extends Controller
{

    public function index()
    {
        $courses = Course::with(['Teacher','Students'])->paginate(15);
        $title = HeadTag::getAdminTitle('دوره ها');

        return view('admin.course.index', compact('courses','title'));
    }

    public function create()
    {
        $users = User::whereIs('teacher')->get();
        $video = Storage::allFiles('video');
        $files = Storage::allFiles('file');
        $image = Storage::allFiles('image');
        $title = HeadTag::getAdminTitle('افزودن دوره');

        return view('admin.course.form', compact('users', 'files','video','image','title'));
    }
    public function save(CreateCourseRequest $createCourseRequest)
    {
        $course = Course::create($this->getsaveArr());
        if ($course instanceof Course) {
            return redirect()->route('admin.course.index')->with('success', 'دوره با موفقیت ایجاد شد.');
        }
    }
    public function delteone($course)
    {
        Course::destroy($course);
        return redirect()->back();
    }
    public function deltemany(DeleteManyItemRequest $deleteManyItemRequest)
    {
        Course::destroy(request()->input('checkbox'));
        return redirect()->back();

    }
    public function edit(Course $course)
    {
        $users = User::whereIs('teacher')->get();
        $video = Storage::allFiles('video');
        $files = Storage::allFiles('file');
        $image = Storage::allFiles('image');
        $title = HeadTag::getAdminTitle('ویرایش دوره');

        return view('admin.course.form', compact('users', 'files','video','image','course','title'));

    }

    public function editSave(CreateCourseRequest $createCourseRequest, Course $course)
    {
        $response = $course->update($this->getsaveArr());
        if ($response) {
            return redirect()->back()->with('success', 'تغییرات با موفقیت ذخیره شد.');}
    }
    private function getsaveArr()
    {

        return [
            'title' => request()->input('courseTitle'),
            'demo_url' => request()->input('courseDemo'),
            'course_length' => request()->input('courseLength'),
            'start_data' => request()->input('courseStartAt'),
            'status' => request()->input('courseStatus'),
            'price' => request()->input('coursePrice'),
            'discount' => request()->input('courseDiscount'),
            'image' => request()->input('course_image'),
            'teacher_id' => request()->input('teacher'),
            'prerequisites' => request()->input('coursePre'),
            'Headlines' => request()->input('courseReference'),
            'slug' => request()->input('slug'),
        ];

    }
}
