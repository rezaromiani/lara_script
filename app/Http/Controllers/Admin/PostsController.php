<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\Post;
use App\User;
use App\Utility\HeadTag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostsController extends Controller
{
    public function index()
    {
        $posts = Post::with(['Category','Comment','User'])->orderBy('created_at','desc')->paginate(15);
        $title = HeadTag::getAdminTitle('مطالب');
        return view('admin.post.index', compact('posts','title'));
    }

    public function delAll(Request $request)
    {
        $this->validate($request, [
            "checkbox" => 'required'
        ]);
        Post::destroy($request->input('checkbox'));
        return redirect()->back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {

        $video = Storage::allFiles('video');
        $files = Storage::allFiles('file');
        $image = Storage::allFiles('image');

        $cats = Category::all();
        $title = HeadTag::getAdminTitle('افزودن مطلب');

        $users = User::select('id', 'first_name', 'last_name')->whereIs('admin', 'teacher','writer')->get();
        return view('admin.post.create', compact('cats', 'users', 'files','video','image','title'));
    }

    public function storage(Request $request)
    {
        $this->validate($request, [
            "title" => "required",
            "detail" => "required",
            "slug" => "required",
            "author_id" => "required",
            "cat-id" => "required",
            "tags" => "required",
            "file" => "required"
        ]);
        if ($request->has('pending')) {
            if ($request->has('post_id') && intval($request->input('post_id'))) {
                $post = Post::find($request->input('post_id'));
                $post->update([
                    'title' => $request->input('title'),
                    'content' => $request->input('detail'),
                    'thumb' => $request->input('file'),
                    'author_id' => $request->input('author_id'),
                    'cat_id' => $request->input('cat-id'),
                    'tags' => $request->input('tags'),
                    'slug' => $request->input('slug')
                ]);
            } else {
                $post = Post::create([
                    'title' => $request->input('title'),
                    'content' => $request->input('detail'),
                    'thumb' => $request->input('file'),
                    'author_id' => $request->input('author_id'),
                    'cat_id' => $request->input('cat-id'),
                    'tags' => $request->input('tags'),
                    'slug' => $request->input('slug')
                ]);
            }
            $post = $post->getAttributes();

        }
        if ($request->has('publish')) {
            if ($request->has('post_id') && intval($request->input('post_id'))) {
                $post = Post::find($request->input('post_id'));
                $post->update([
                    'status' => 1,
                    'title' => $request->input('title'),
                    'content' => $request->input('detail'),
                    'thumb' => $request->input('file'),
                    'author_id' => $request->input('author_id'),
                    'cat_id' => $request->input('cat-id'),
                    'tags' => $request->input('tags'),
                    'slug' => $request->input('slug')
                ]);

            }
        }
        if ($request->has('delete')) {
            if ($request->has('post_id') && intval($request->input('post_id'))) {
                Post::destroy($request->input('post_id'));
                return redirect()->route('admin.post.index');
            }
        }
        $cats = Category::all();
        $users = User::select('id', 'first_name', 'last_name')->whereIs('admin', 'teacher','writer')->get();
        $video = Storage::allFiles('video');
        $files = Storage::allFiles('file');
        $image = Storage::allFiles('image');
        return view('admin.post.create', compact('post', 'cats', 'users', 'files','video','image'));
    }

    public function remove(Request $request, $post_id)
    {
        if (intval($post_id)) {
            Post::destroy($post_id);
            return redirect()->back();
        }
        return redirect()->back();
    }

    public function edit(Request $request, $post_id)
    {
        if (intval($post_id)) {
            $post = Post::find($post_id);
            $video = Storage::allFiles('video');
            $files = Storage::allFiles('file');
            $image = Storage::allFiles('image');
            $cats = Category::all();
            $users = User::select('id', 'first_name', 'last_name')->whereIs('admin', 'teacher','writer')->get();
            $title = HeadTag::getAdminTitle('ویراش مطلب');

            return view('admin.post.edit', compact('files','video','image', 'cats', 'users', 'post','title'));
        }
        return redirect()->back();
    }
}
