<?php

namespace App\Http\Controllers\Admin;

use App\Model\Course;
use App\Model\CourseSession;
use App\User;
use App\Utility\HeadTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CourseSessionsController extends Controller
{
    public function index(Course $course)
    {
        $sessions = $course->Sessions()->paginate(15);
        $title = HeadTag::getAdminTitle($course->title.'جلسات دوره ');

        return view('admin.course.sessions.index', compact('sessions', 'course','title'));
    }

    public function create(Course $course)
    {
        $video = Storage::allFiles('video');
        $files = Storage::allFiles('file');
        $image = Storage::allFiles('image');
        $title = HeadTag::getAdminTitle('افزودن جلسه');

        return view('admin.course.sessions.form', compact('files', 'video', 'image', 'course','title'));
    }

    public function edit(Course $course,CourseSession $courseSession)
    {
        $post = $courseSession;
        $video = Storage::allFiles('video');
        $files = Storage::allFiles('file');
        $image = Storage::allFiles('image');
        $title = HeadTag::getAdminTitle('ویرایش جلسه');

        return view('admin.course.sessions.form',compact('files', 'video', 'image','course','post','title'));
    }

    public function storage(Request $request, Course $course)
    {
        $this->validate($request, [
            "title" => "required",
            "detail" => "required",
            "slug" => "required",
            "file" => "required"
        ]);
        if ($request->has('pending')) {
            if ($request->has('session_id') && intval($request->input('session_id'))) {
                $post = CourseSession::find($request->input('session_id'));
                $post->update($this->getDataArr($request,$course));
            } else {
                $post = CourseSession::create($this->getDataArr($request,$course));
            }
            $post = $post->getAttributes();

        }
        if ($request->has('publish')) {
            if ($request->has('session_id') && intval($request->input('session_id'))) {
                $post = CourseSession::find($request->input('session_id'));
                $post->update($this->getDataArr($request,$course,1));

            }
        }
        if ($request->has('delete')) {
            if ($request->has('session_id') && intval($request->input('session_id'))) {
                CourseSession::destroy($request->input('session_id'));
                return redirect()->route('admin.course.sessions',$course->id);
            }
        }
        $video = Storage::allFiles('video');
        $files = Storage::allFiles('file');
        $image = Storage::allFiles('image');
        $users = User::select('id', 'first_name', 'last_name')->whereIs('admin', 'teacher', 'writer')->get();
        return view('admin.course.sessions.form', compact('post','course','files', 'video', 'image'));
    }

    public function delete($post_id)
    {
        if (intval($post_id)) {
            CourseSession::destroy($post_id);
            return redirect()->back();
        }
        return redirect()->back();
    }
    public function delAll(Request $request)
    {
        $this->validate($request, [
            "checkbox" => 'required'
        ]);
        CourseSession::destroy($request->input('checkbox'));
        return redirect()->back();
    }
    private function getDataArr(Request $request,$course,$status=0)
    {
        return [
            'title' => $request->input('title'),
            'content' => $request->input('detail'),
            'video_url' => $request->input('course_video'),
            'file_url' => $request->input('course_file'),
            'course_id' => $course->id,
            'thumb' => $request->input('file'),
            'slug' => $request->input('slug'),
            'status'=> $status,
            'video_time'=>$request->input('video_length')
        ];
    }
}
