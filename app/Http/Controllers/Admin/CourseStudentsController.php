<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeleteManyItemRequest;
use App\Model\Course;
use App\User;
use App\Utility\HeadTag;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CourseStudentsController extends Controller
{
    public function student(Course $course)
    {
        $students = $course->Students()->paginate(20);

        $Users = User::whereNotIn('id', $students->pluck('id')->toArray())->get();
        $title = HeadTag::getAdminTitle($course->title.'دانشجویان دوره ');

        return view('admin.course.students', compact('Users', 'course', 'students','title'));
    }
    public function SyncStudent(Course $course)
    {
        $this->validate(request(), [
            'userID' => 'required',
            'userID.*' => 'required|numeric',
        ]);
        $ids = request()->input('userID');
        $data = array_fill_keys($ids, ['created_at' => Carbon::now()]);
        $course->Students()->attach($data);
        return redirect()->back();
    }
    public function delteone(Course $course, $student)
    {
        $course->Students()->detach($student);
        return redirect()->back();
    }
    public function deltemany(DeleteManyItemRequest $deleteManyItemRequest, Course $course)
    {
        $course->Students()->detach(request()->input('checkbox'));
        return redirect()->back();

    }
}
