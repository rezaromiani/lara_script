<?php

namespace App\Http\Controllers\Admin;

use App\Model\Category;
use App\Utility\HeadTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function index(Request $request, $post_id = null)
    {
        $cats = Category::all();
        $title = HeadTag::getAdminTitle('دسته بندی ها');

        if (intval($post_id))
            $cat = Category::find($post_id);
        return view('admin.categories.index', compact('cats', 'cat','title'));
    }

    public function create(Request $request, $post_id = null)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);
        if (intval($post_id)) {
            $cat = Category::find($post_id);
            $cat->update(['cat_name' => $request->input('title')]);
        } else {
            $res = Category::create(['cat_name' => $request->input('title')]);
        }
        return redirect()->route('admin.categories.index');
    }

    public function delete(Request $request,$post_id)
    {
        if (intval($post_id)){
            Category::destroy($post_id);
        }
        return redirect()->route('admin.categories.index');
    }
}
