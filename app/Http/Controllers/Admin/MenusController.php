<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Menu;
use App\Utility\HeadTag;
use App\Utility\menuUtility;
use Illuminate\Http\Request;

class MenusController extends Controller
{
    public function index()
    {
        $menus = Menu::orderBy('priority')->get()->groupBy('parent_id');
        $title = HeadTag::getAdminTitle('منو ها');

        return view('admin.menu.index', compact('menus','title'));
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'href' => 'required'
        ]);
        $menu = Menu::create([
            'href' => $request->input('href'),
            'title' => $request->input('title')
        ]);
        if ($menu instanceof Menu) {
            return redirect()->back()->with('success', 'منو با با موفقیت ایجاد شد.');
        }
        return redirect()->back()->with('error', 'خطایی در ایجاد منو پیش آمده لطفا تلاش کنید.');
    }

    public function edit(Menu $menu)
    {
        $title = HeadTag::getAdminTitle('ویرایش منو');

        return view('admin.menu.edit', compact('menu','title'));
    }

    public function editStorage(Request $request, Menu $menu)
    {
        $this->validate($request, [
            'title' => 'required',
            'href' => 'required',
            'menu_id' => 'required'
        ]);
        $menu->title=$request->input('title');
        $menu->href=$request->input('href');
        $menu->save();
        return redirect()->route('admin.menu.index');

    }

    public function ajax(Request $request)
    {
        menuUtility::updateMenu($request->input('list'));
    }

    /**
     * @param Menu $menu
     * @throws \Exception
     */
    public function del(Menu $menu)
    {
        $parent_id = $menu->id;
        Menu::where('parent_id', '=', $parent_id)->orWhere('id', '=', $parent_id)->delete();
        return redirect()->route('admin.menu.index');
    }
}
