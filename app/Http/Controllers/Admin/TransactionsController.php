<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DeleteManyItemRequest;
use App\Model\Transaction;
use App\Utility\HeadTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TransactionsController extends Controller
{
    public function index()
    {
        if (request()->has(['search_type', 'search'])) {
            $search_type = request()->input('search_type');
            $search = request()->input('search');
           $transactions = $this->search($search_type, $search);
        } else {

            $transactions = Transaction::with('User')->orderBy('created_at', 'desc')->paginate(20);
        }
        $title = HeadTag::getAdminTitle('تراکنش ها');
        $admin = Auth::check() && Auth::user()->isAn('admin');
        return view('admin.transaction.index', compact('transactions','title','admin'));
    }

    public function edit(Transaction $transaction)
    {
        $title = HeadTag::getAdminTitle('ویرایش تراکنش');

        return view('admin.transaction.edit', compact('transaction', 'title'));
    }

    public function saveEdit(Transaction $transaction)
    {
        $transaction->update([
            'ref_number' => request()->input('ref_number'),
            'status' => request()->input('status'),
        ]);
        return redirect()->back();
    }

    public function delete($transaction)
    {
        Transaction::destroy($transaction);
        return redirect()->back();
    }

    public function deltemany(DeleteManyItemRequest $deleteManyItemRequest)
    {
        Transaction::destroy(request()->input('checkbox'));
        return redirect()->back();

    }

    private function search($search_type, $search)
    {
        $transaction = Transaction::where ( $search_type, 'LIKE', '%' . $search . '%' )->paginate (20)->setPath ( '' );
        $transaction->appends(['search_type' => $search_type, 'search' => $search]);
        return $transaction;
    }
}
