<?php

namespace App\Http\Controllers\Admin;

use App\Model\DisCount;
use App\Utility\HeadTag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DisCountController extends Controller
{
    public function index()
    {
        $title= HeadTag::getAdminTitle('مدیریت تخفیف');
        $dis_count = DisCount::all();
        return view('admin.discount.index',compact('title','dis_count'));
    }

    public function edit()
    {
        $this->validate(\request(), [
            'title' => 'required',
            'day_count' => 'required',
        ]);
        $result = DisCount::where('id','=',\request()->input('title'))->update([
            'end_at' => Carbon::now()->addDays(request()->input('day_count'))->format('Y-m-d 00:00:00')
        ]);
        if ($result) {
            return redirect()->route('admin.discount.index')->with('success', 'تخفیف با موفقیت ذخیره شد.');
        }
    }

    public function create()
    {
        $title= HeadTag::getAdminTitle('ایجاد تخفیف');
        return view('admin.discount.create',compact('title'));
    }

    public function storage()
    {
        $this->validate(\request(), [
            'title' => 'required',
            'day_count' => 'required',
        ]);
        $result = DisCount::create([
            'title' => request()->input('title'),
            'end_at' => Carbon::now()->addDays(request()->input('day_count'))->format('Y-m-d 00:00:00')
        ]);
        if ($result instanceof DisCount) {
            return redirect()->route('admin.discount.create')->with('success', 'تخفیف  با موفقیت اضافه شد.');
        }
    }

    public function delete($discount)
    {
        DisCount::destroy($discount);
        return redirect()->back();
    }
}
