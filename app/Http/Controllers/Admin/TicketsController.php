<?php



namespace App\Http\Controllers\Admin;
use App\Utility\HeadTag;
use App\Http\Controllers\Controller;
use App\Model\Ticket;
use App\Model\TicketMsg;
use App\Utility\TicketUtility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class TicketsController extends Controller
{
    public function index()
    {
        $tickets = Ticket::orderBy('created_at', 'desc')->get();
        $title = HeadTag::getAdminTitle('مدیریت تیکت ها');

        return view('admin.ticket.index', compact('tickets','title'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function answer(Request $request)
    {
        $this->validate($request, [
            'ticket_text' => 'required',
            'ticket_id' => 'required',
        ]);
        Auth::check() || Response::json(['error' => true], 500);

        TicketMsg::where([
            ['ticket_id', '=', $request->input('ticket_id')],
            ['status', '=', 1],
        ])->update(['status' => 0]);

        $user = Auth::user()->id;
        $ticket_answer = TicketMsg::create([
            'ticket_id' => $request->input('ticket_id'),
            'content' => $request->input('ticket_text'),
            'user_id' => $user
        ]);

        if ($ticket_answer) {
            $ti = Ticket::find($request->input('ticket_id'));
            $view = view('user.account.answermsg', compact('ti'))->render();
            return Response::json(['success' => true, 'view' => $view,'count'=>TicketUtility::getNewMsgCount($ti,Auth::user())]);
        }
        return Response::json(['error' => true], 500);
    }
}
