<?php

namespace App\Http\Controllers\Admin;

use App\Model\ItemeUserChoices;
use App\Model\ItemUserChoice;
use App\Model\Poll;
use App\Model\PollItem;
use App\Utility\HeadTag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class pollsController extends Controller
{
    public function index()
    {
        $polls = Poll::withCount('PollItem')->get();
        $title = HeadTag::getAdminTitle('نظر سنجی ها');
        return view('admin.poll.index', compact('polls', 'title'));

    }

    /**
     * @param Request $request
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'question' => 'required',
            'item' => 'required|array'
        ]);
        $day = $request->input('day');
        $expire_at = Carbon::now()->addDays($day)->format('Y-m-d 00:00:00');
        $poll = Poll::create([
            'poll_title' => $request->input('question'),
            'expire_at' => $expire_at
        ]);
        $items = $request->input('item');
        $arr = [];
        foreach ($items as $t) {
            if (!empty($t)) {
                $p = new PollItem(['title' => $t]);
                $poll->PollItem()->save($p);
                $arr[] = $p->getAttribute('id');
            }
        }
        $this->setDefaultItemChoise($arr, $poll->getAttribute('id'));
        return redirect()->back();
    }

    public function edit(Poll $poll)
    {
        $pollItem = $poll->PollItem()->get();
        $title = HeadTag::getAdminTitle($poll->poll_title . 'ویرایش نظر سنجی ');
        return view('admin.poll.edit', compact('poll', 'pollItem', 'title'));
    }

    public function delItemAjax(Request $request)
    {
        if (intval($request->input('item_id'))) {
            PollItem::destroy($request->input('item_id'));
            return Response::json(['success' => true]);
        }
        return Response::json([], 403);
    }

    public function editsave(Request $request, Poll $poll)
    {
        $this->validate($request, [
            'question' => 'required',
            'olditem' => 'required|array'
        ]);
        $poll->poll_title = $request->input('question');
        $poll->save();
        $olditem = $request->input('olditem');
        foreach ($olditem as $key => $val) {
            PollItem::where('id', $key)->update(['title' => trim($val)]);
        }
        if ($request->has('item') && is_array($request->input('item'))) {
            $items = $request->input('item');
            foreach ($items as $t) {
                if (!empty($t)) {
                    $poll->PollItem()->save(new PollItem(['title' => $t]));
                }
            }
        }
        return redirect()->back();
    }

    public function delete($poll)
    {
        PollItem::where('poll_id', '=', $poll)->delete();
        ItemeUserChoices::where('poll_id', '=', $poll)->delete();
        Poll::destroy($poll);
        return redirect()->back();
    }

    private function setDefaultItemChoise(array $arr, $poll_id)
    {
        $user = \Auth::user();
        foreach ($arr as $a) {
            $user->pollItemChoices()->save(new ItemUserChoice(['poll_id' => $poll_id, 'item_id' => $a]));
        }
    }
}
