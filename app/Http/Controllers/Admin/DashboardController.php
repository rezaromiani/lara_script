<?php

namespace App\Http\Controllers\Admin;

use App\Model\Comment;
use App\Model\CourseSession;
use App\Model\Post;
use App\Model\Transaction;
use App\User;
use App\Utility\HeadTag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Morilog\Jalali\Jalalian;

class DashboardController extends Controller
{
    public function index()
    {

        $statistics = $this->statistics();
        $lsat_users = User::orderBy('created_at', 'desc')->select('email', 'first_name', 'last_name', 'created_at')->take(4)->get();
        $last_comment = Comment::with('User')->orderBy('created_at', 'desc')->take(3)->get();
        $post_statistics = $this->postChartData();
        $transaction_statistics = $this->TransactionsChartData();
        $title = HeadTag::getAdminTitle('داشبورد');

        return view('admin.dashboard.index', compact('statistics','title', 'lsat_users', 'last_comment', 'post_statistics','transaction_statistics'));
    }

    private function statistics()
    {
        return [
            'post_count' => Post::count('id'),
            'user_count' => User::count('id'),
            'comment_count' => Comment::count('id'),
            'view_count' => Post::sum('view') + CourseSession::sum('view'),
            'total_sale' => Transaction::sum('price'),
            'today_sale' => Transaction::whereDate('created_at', Carbon::today()->toDateString())->sum('price')
        ];
    }

    private function postChartData()
    {
        $results = Post::selectRaw('COUNT(id) AS data,DATE(created_at) as date')->groupBy(DB::raw('DATE(created_at)'))->get();
        $post_statistics = [];
        foreach ($results as $item) {
            $post_statistics[Jalalian::forge(strtotime($item->date))->format('Y-m-d')] = $item->data;
        }
        return $post_statistics;
    }

    private function TransactionsChartData()
    {
        $results = Transaction::selectRaw('SUM(price) AS data,DATE(created_at) as date')->where('status', '=', 1)->groupBy(DB::raw('DATE(created_at)'))->get();
        $Transaction_statistics = [];
        foreach ($results as $item) {
            $Transaction_statistics[Jalalian::forge(strtotime($item->date))->format('Y-m-d')] = $item->data;
        }
        return $Transaction_statistics;
    }
}
