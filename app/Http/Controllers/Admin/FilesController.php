<?php

namespace App\Http\Controllers\Admin;

use App\Utility\CommonUtility;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;


class FilesController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function upload(Request $request)
    {
        $this->validate($request, [
            'file' => 'mimes:mpeg,pdf,jpeg,png,mp4,zip|max:1000000'
        ]);
        $mimeType = $request->file('file')->getMimeType();
        $file_name = $request->file('file')->getClientOriginalName();
        switch ($mimeType) {
            case 'video/mp4':
                $request->file('file')->storeAs(
                    'video', $file_name
                );
                break;
            case 'application/pdf':
            case 'application/zip':
                $request->file('file')->storeAs(
                    'file', $file_name
                );
                break;
            default:
                $request->file('file')->store('image');
                break;
        }


        $videos = Storage::allFiles('video');
        $files = Storage::allFiles('file');
        $image = Storage::allFiles('image');
        $img = '';
        $allFiles = '';
        $video = '';
        if (count($image) > 0)
            foreach ($image as $file) {
                $img .= '<img class="img-responsive img-thumbnail img-upload" src="' . asset('storage/upload/') . '/' . $file . '" alt="">';
            }
        if (count($videos) > 0)
            foreach ($videos as $file) {
                $video .= '<div class="fn-file-container fn-file-container-video" data-src="' . asset('storage/upload/') . '/' . $file . '">
                                        <img src="/img/video.png" class="img-responsive" alt=""><span>' . basename($file) . '</span></div>';
            }
        if (count($files) > 0)
            foreach ($files as $f) {

                $allFiles .= '<div class="fn-file-container fn-file-container-file" data-src="' . asset('storage/upload/') . '/' . $f . '">';
                if (CommonUtility::getFileType($f) == 'zip') {
                    $allFiles .= '<img src="/img/zip.jpg" class="img-responsive" alt="">';
                } else {
                    $allFiles .= '<img src="/img/pdf.jpg" class="img-responsive" alt="">';
                }
                $allFiles .= '<span>' . basename($f) . '</span>';
                $allFiles .= '</div>';
            }
        return \response()->json([
            'img' => $img,
            'video' => $video,
            'file' => $allFiles
        ]);
    }

    public function delete()
    {
        $this->validate(\request(), [
            'file' => 'required'
        ]);
        if (Storage::delete(\request()->input('file'))) {
            return \response()->json(['success' => true]);
        }
        return \response()->json(['success' => false]);
    }
}
