<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Utility\HeadTag;
use App\Utility\Users;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    public function index()
    {

        $users = User::with('post')->paginate(20);
        $title = HeadTag::getAdminTitle('کاربران');
        $roles = \DB::table('roles')->select('name', 'title')->get();
        return view('admin.user.index', compact('users', 'title', 'roles'));
    }

    public function userOrderBY(Request $request, $order)
    {
        $or = $order;
        $users = User::whereIs($or)->paginate(20);

        $title = HeadTag::getAdminTitle('کاربران');
        $roles = \DB::table('roles')->select('name', 'title')->get();

        return view('admin.user.index', compact('users', 'title', 'roles'));
    }

    public function create()
    {
        $page_title = 'ایجاد کاربر جدید';
        $title = HeadTag::getAdminTitle('افزودن کاربر');
        $edit =true;
        return view('admin.user.create', compact('page_title', 'title','edit'));
    }

    public function storage(Request $request)
    {

        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'username' => 'required',
            'password' => 'required|confirmed|min:5',
            'email' => 'required|email|confirmed|unique:users,email',
            'mobile' => 'required',
            'role' => 'required',
        ]);
        $result = User::create([
            'username' => $request->input('username'),
            'first_name' => $request->input('firstName'),
            'last_name' => $request->input('lastName'),
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'mobile' => $request->input('mobile'),
        ]);
        if ($result instanceof User) {
            $result->assign($request->input('role'));
            return redirect()->route('admin.user.index')->with('success', 'کاربر با موفقیت اضافه شد.');
        }
            return redirect()->route('admin.user.index')->with('error', 'مشکلی در ذخیره کاربر پیش آمده لطفا بعدا تلاش کنید.');
    }

    public function edit(Request $request, $user_id)
    {
        $user = User::find($user_id);
        $page_title = 'ویرایش کاربر';
        $title = HeadTag::getAdminTitle('ویرایش کاربر');
        $edit =Auth::check() && Auth::user()->isAn('admin');
        return view('admin.user.create', compact('user', 'page_title', 'title','edit'));
    }

    public function update(Request $request, $user_id)
    {
        $user_id = intval($user_id);
        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'username' => 'required',
            'email' => 'required|email|confirmed',
            'mobile' => 'required',
        ]);
        $date = [
            'username' => $request->input('username'),
            'first_name' => $request->input('firstName'),
            'last_name' => $request->input('lastName'),
            'email' => $request->input('email'),
            'mobile' => $request->input('mobile'),
        ];
        if ($request->has('password') && !empty($request->input('password'))) {
            $date['password'] = $request->input('password');
        }
        $user = User::find($user_id);
        if ($user instanceof User) {
            $res = $user->update($date);
            if ($res) {
                if (\Auth::user()->isAn('admin')) {
                    $user->retract($user->getRoles()[0]);
                    $user->assign($request->input('role'));
                }
                return redirect()->route('admin.user.index')->with('success', 'عملیات با موفقیت انجام شد.');
            }
            return redirect()->route('admin.user.index')->with('error', 'مشکلی در انجام عملیات پیش امده.');
        }
    }

    public function delete(Request $request, $user_id)
    {
        if (!intval($user_id)) {
            return redirect()->route('admin.user.index');
        }
        $user = User::destroy($user_id);
        return redirect()->route('admin.user.index');
    }

    public function deletes(Request $request)
    {
        if ($request->has('checkbox')) {
            User::destroy($request->input('checkbox'));
            return redirect()->route('admin.user.index')->with('success', 'عملیات با موفقیت انجام شد.');
        }
        return redirect()->route('admin.user.index');
    }
}
