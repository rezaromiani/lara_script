<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Utility\Ads;
use App\Utility\HeadTag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Morilog\Jalali\CalendarUtils;

class AdsController extends Controller
{
    public function __construct()
    {
        \View::composers([
            'App\Http\View\Composers\fileManagerComposer' => ['admin.ads.index','admin.ads.edit'],
        ]);
    }
    public function index()
    {
        $ads = \App\Model\Ads::all();
        $title = HeadTag::getAdminTitle('تبلیغات');
        return view('admin.ads.index', compact( 'ads',compact('title')));
    }

    /**
     * @param Request $request
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'href' => 'required',
            'start_date' => 'required',
            'expire_date' => 'required',
            'image' => 'required',
        ]);

        $start_arr = \App\Utility\Ads::date_to_arr($request->input('start_date'));
        $expire_arr = \App\Utility\Ads::date_to_arr($request->input('expire_date'));
        $s_date = Ads::arr_to_date(CalendarUtils::toGregorian($start_arr[0], $start_arr[1], $start_arr[2]));
        $e_date = Ads::arr_to_date(CalendarUtils::toGregorian($expire_arr[0], $expire_arr[1], $expire_arr[2]));

        $ads = \App\Model\Ads::create([
            "title" => $request->input('title'),
            "href" => $request->input('href'),
            "img" => $request->input('image'),
            "start_date" => $s_date,
            "expire_date" => $e_date,
        ]);
        if ($ads instanceof \App\Model\Ads) {
            return redirect()->back()->with('success', true);
        }
        return redirect()->back()->with('error', true);
    }

    public function edit(\App\Model\Ads $aid)
    {
        $title = HeadTag::getAdminTitle('ویرایش تبلیغ');

        return view('admin.ads.edit', compact('aid', 'files','title'));
    }

    public function saveEdit(Request $request, \App\Model\Ads $aid)
    {
        $this->validate($request, [
            'title' => 'required',
            'href' => 'required',
            'start_date' => 'required',
            'expire_date' => 'required',
            'image' => 'required',
        ]);

        $start_arr = \App\Utility\Ads::date_to_arr($request->input('start_date'));
        $expire_arr = \App\Utility\Ads::date_to_arr($request->input('expire_date'));
        $s_date = Ads::arr_to_date(CalendarUtils::toGregorian($start_arr[0], $start_arr[1], $start_arr[2]));
        $e_date = Ads::arr_to_date(CalendarUtils::toGregorian($expire_arr[0], $expire_arr[1], $expire_arr[2]));

        $ads = $aid->update([
            "title" => $request->input('title'),
            "href" => $request->input('href'),
            "img" => $request->input('image'),
            "start_date" => $s_date,
            "expire_date" => $e_date,
        ]);
        if ($ads) {
            return redirect()->back()->with('success', true);
        }
        return redirect()->back()->with('error', true);
    }
    public function delete($ads){
        \App\Model\Ads::destroy($ads);
        return redirect()->back();
    }
}
