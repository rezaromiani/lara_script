<?php


namespace App\Http\View\Composers;


use App\Model\Comment;
use App\Model\Option;

class OptionsComposer
{
    public function compose($view)
    {
        $options = Option::find('options');

        $view->with([
            'options' => $options ? $options->value : false,
            'comments'=> Comment::where('comment_status', 1)->limit(4)->get()
        ]);
    }
}
