<?php
namespace App\Http\View\Composers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class fileManagerComposer
{

    public function compose($view)
    {
        $video = Storage::allFiles('video');
        $files = Storage::allFiles('file');
        $image = Storage::allFiles('image');
        $view->with([
            'video' => $video,
            'files' => $files,
            'image' => $image,

        ]);
    }

}
