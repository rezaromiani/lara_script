<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'courseTitle'=>'required',
            'courseDemo'=>'required',
            'courseLength'=>'required',
            'courseStartAt'=>'required',
            'courseStatus'=>'required',
            'coursePrice'=>'required',
            'courseDiscount'=>'required',
            'teacher'=>'required',
            'coursePre'=>'required',
            'course_image'=>'required',
            'courseReference'=>'required',
            'slug'=>'required'
        ];
    }
}
