پروژه سایت آموزشی با laravel

<p align="center"><img src="https://i.ibb.co/XJ2G7PZ/screencapture-127-0-0-1-8000-2020-08-16-00-47-11.png" alt="screencapture-127-0-0-1-8000-2020-08-16-00-47-11" border="0"></p>

> **Note**: requires PHP 7.2+
> 

### Installing 

1) Install dependency

```
$ composer install
```

2)  Create a symbolic link from:

    ```
    php artisan storage:link 
    ```

3) run migrations:

    ```
    php artisan migrate
    ```

4) run bouncer seeder:

    ```
    php artisan db:seed --class=BouncerSeeder
    ```
5) run database  seeder:

    ```
    php artisan db:seed
    ```
admin user name
```
admin@gmail.com
```
admin password:
```
123456
```