<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'user'], function () {
    Route::get('/', 'HomeController@index')->name('user.home');
    Route::get('/post/{post_id}/{post_slug}', 'PostsController@post')->name('post.single');
    Route::get('postload/load/{type}', 'PostsController@load_Post')->where('type', '[1-3]+')->name('post.load.jqload');
    Route::post('/postlike/action', 'PostLikesController@like')->name('post.like.action');


    /*Comments*/
    Route::post('/comment/{post_id}/{post_slug}', 'CommentsController@saveComment')->name('post.single.ajax');


    //    archive
    Route::get('archive/allpost', 'ArchivesController@allPost')->name('all.post.archive');
    Route::get('archive/allCourse', 'ArchivesController@allCourse')->name('all.post.Course');
    Route::get('archive/tag/{tag}', 'ArchivesController@tagPost')->name('all.post.tag');
    Route::get('archive/search', 'ArchivesController@search')->name('all.post.search');
    /*vote*/
    Route::post('vote/user/gJFospAAfwofj4a7d', 'pollsController@vote')->name('poll.item.ajax');


    /*Course*/
    Route::get('/course/{course}/{title}', 'CoursesController@index')->name('course.single');
    Route::post('/course/{course}/{title}', 'CoursesController@sendToCheckout')->name('course.single')->middleware('auth');
    //course session single
    Route::get('/session/{courseSession}/{slug}', 'CourseSessionController@index')->name('course.session.single');
    Route::get('file/dl/{file_name}', 'CourseSessionController@dowloadPostVideo')->name('file.download');

    /* payments & checkout*/
    Route::get('checkout', 'paymentsController@checkout')->name('checkout')->middleware('auth');
    Route::post('checkout', 'paymentsController@payment')->name('checkout')->middleware('auth');
    Route::get('verifyPayment', 'paymentsController@verifyPayment')->name('verifyPayment')->middleware('auth');

});
Route::get('/logout', 'authentication\loginRegController@logout')->name('logOut');
Route::group(['namespace' => 'user', 'middleware' => 'auth'], function () {

    /*userAccount*/
    Route::get('/userAccount', 'userController@index')->name('user.account.index');
    Route::post('/userAccount/profilesave', 'userController@edit')->name('user.account.edit');
    Route::post('/userAccount/chpass', 'userController@chpass')->name('user.account.chPassword');

    Route::post('userAccount/profile/sendTicket/51ad5awd1s3qwd', 'TicketsController@create')->name('user.account.ticket.send');
    Route::post('userAccount/profile/sendTicketAnswer/dasdw3edasd', 'TicketsController@answer')->name('user.account.ticket.answer');
});
Route::group(['namespace' => 'authentication', 'middleware' => 'guest'], function () {
    Route::get('/auth', 'loginRegController@auth')->name('auth');
    Route::post('/auth', 'loginRegController@doLogReg');
    Route::get('/auth/activation_user/{key}', 'loginRegController@activation_user')->name('userActivation');
    RoUte::get('/auth/resetPassword', 'forgetPassController@forgetpass')->name('resetPassword');
    RoUte::get('/auth/resetPassword/pass/{key}', 'forgetPassController@resetPass')->name('resetPasswordEmailLink');
    RoUte::post('/auth/resetPassword/pass/{key}', 'forgetPassController@DoresetPass');
    RoUte::post('/auth/resetPassword', 'forgetPassController@DoForgetPass');
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'see-dashboard'], function () {
    Route::get('/', 'DashboardController@index')->name('admin.dashboard');

    /*users*/
    Route::get('/users', 'UsersController@index')->name('admin.user.index');
    Route::get('/users/order/{order}', 'UsersController@userOrderBY')->name('admin.user.index.order');
    Route::post('/users/order/{order}', 'UsersController@deletes');
    Route::post('/users', 'UsersController@deletes');
    Route::get('/users/create', 'UsersController@create')->name('admin.user.create');
    Route::post('/users/create', 'UsersController@storage')->name('admin.user.create');
    Route::get('/users/edit/{user_id}', 'UsersController@edit')->name('admin.user.edit');
    Route::get('/users/delete/{user_id}', 'UsersController@delete')->name('admin.user.delete');
    Route::post('/users/edit/{user_id}', 'UsersController@update')->name('admin.user.update');

    /*posts*/
    Route::get('/posts', 'PostsController@index')->name('admin.post.index');
    Route::post('/posts', 'PostsController@delAll')->name('admin.post.index');
    Route::get('/posts/create', 'PostsController@create')->name('admin.post.create');
    Route::post('/posts/create', 'PostsController@storage');
    Route::get('/post/delete/{post_id}', 'PostsController@remove')->name('admin.post.delete');
    Route::get('/post/edit/{post_id}', 'PostsController@edit')->name('admin.post.edit');
    Route::post('/post/edit/{post_id}', 'PostsController@storage')->name('admin.post.edit');
    /*Categories*/
    Route::get('/category/{cat_id?}', 'CategoriesController@index')->name('admin.categories.index');
    Route::get('/category/del/{cat_id}', 'CategoriesController@delete')->name('admin.categories.delete');
    Route::post('/category/{cat_id?}', 'CategoriesController@create');
    /*File Upload*/
    Route::post('file/file_upload', 'FilesController@upload')->name('admin.file.upload');
    Route::post('file/file_delete', 'FilesController@delete')->name('admin.file.delete');
    /*Comments*/
    Route::get('comment', 'CommentsController@index')->name('admin.comment.index');
    Route::post('comment', 'CommentsController@edit')->name('admin.comment.edit');
    Route::post('comment/status', 'CommentsController@updateStatus')->name('admin.comment.status');
    Route::post('comment/reply', 'CommentsController@reply')->name('admin.comment.reply');
    Route::get('comment/del/{id}', 'CommentsController@delete')->name('admin.comment.delete');
    /*poll*/
    Route::get('/poll', 'pollsController@index')->name('admin.poll.index');
    Route::get('/poll/edit/{poll}', 'pollsController@edit')->name('admin.poll.edit');
    Route::post('/poll/edit/{poll}', 'pollsController@editsave');
    Route::post('/poll', 'pollsController@create');
    Route::get('/poll/{poll}', 'pollsController@delete')->name('admin.poll.delete');
    Route::post('poll/delitem/aDSDWasdwKHGkchnce/', 'pollsController@delItemAjax')->name('admin.pollitem.ajax');

    /*Transactions*/
    Route::get('transactions', 'TransactionsController@index')->name('admin.transaction.index');
    Route::get('transaction/{transaction}/edit', 'TransactionsController@edit')->name('admin.transaction.edit')->middleware('admin');
    Route::post('transaction/{transaction}/edit', 'TransactionsController@saveEdit')->name('admin.transaction.edit')->middleware('admin');
    Route::get('transactions/delete/{transaction_id}', 'TransactionsController@delete')->name('admin.transaction.delete')->middleware('admin');
    Route::post('transactions/delete', 'TransactionsController@deltemany')->name('admin.transaction.delete.many')->middleware('admin');
    /*menu*/
    Route::get('menu', 'MenusController@index')->name('admin.menu.index');
    Route::get('menu/del/{menu}', 'MenusController@del')->name('admin.menu.delete');
    Route::get('menu/edit/{menu}', 'MenusController@edit')->name('admin.menu.edit');
    Route::post('menu/edit/{menu}', 'MenusController@editStorage');
    Route::post('menu', 'MenusController@save');
    Route::post('menu/sort/ajax', 'MenusController@ajax')->name('admin.menu.ajax');
    /*Ticket*/
    Route::get('tickets', 'TicketsController@index')->name('admin.ticket.index');
    Route::post('ticket/answer', 'TicketsController@answer')->name('admin.ticket.answer');
    /*slider */
    Route::get('sliders', 'slidersController@index')->name('admin.slider.index');
    Route::post('sliders', 'slidersController@create')->name('admin.slider.index');
    Route::get('slider/{slider}/edit', 'slidersController@edit')->name('admin.slider.edit');
    Route::post('slider/{slider}/edit', 'slidersController@SaveEdit')->name('admin.slider.edit');
    Route::get('slider/{slider}/delete', 'slidersController@delete')->name('admin.slider.del');
    /*notification*/
    Route::get('notifications', 'notificationsController@index')->name('admin.notification.index');
    Route::post('notifications', 'notificationsController@create')->name('admin.notification.index');
    Route::get('notification/{notification}/edit', 'notificationsController@edit')->name('admin.notification.edit');
    Route::post('notification/{notification}/edit', 'notificationsController@SaveEdit')->name('admin.notification.edit');
    Route::get('notification/{notification}/delete', 'notificationsController@delete')->name('admin.notification.del');
    /*progress*/
    Route::get('progress', 'ProgressController@index')->name('admin.progress.index');
    Route::post('progress', 'ProgressController@edit')->name('admin.progress.index');
    Route::get('progress/create', 'ProgressController@create')->name('admin.progress.create');
    Route::post('progress/create', 'ProgressController@storage')->name('admin.progress.create');
    Route::get('progress/{progress}/delete', 'ProgressController@delete')->name('admin.progress.del');

    /*Profiles*/
    Route::get('profile', 'ProfileController@edit')->name('admin.user.profile');
    Route::post('profile', 'ProfileController@update')->name('admin.user.profile');
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['see-dashboard', 'teacher']], function () {
    /*Course*/
    Route::get('/course', 'CourseController@index')->name('admin.course.index');
    Route::get('/course/create', 'CourseController@create')->name('admin.course.create');
    Route::post('/course/create', 'CourseController@save')->name('admin.course.create');
    Route::get('/course/{course}/edit', 'CourseController@edit')->name('admin.course.edit');
    Route::post('/course/{course}/edit', 'CourseController@editSave')->name('admin.course.edit');
    Route::get('/course/{course}/delete', 'CourseController@delteone')->name('admin.course.delete.one');
    Route::post('/course/delete', 'CourseController@deltemany')->name('admin.course.delete.many');
    //course sessions
    Route::get('/course/{course}/sessions', 'CourseSessionsController@index')->name('admin.course.sessions');
    Route::get('/course/{course}/session/create', 'CourseSessionsController@create')->name('admin.course.sessions.create');
    Route::post('/course/{course}/session/create', 'CourseSessionsController@storage')->name('admin.course.sessions.create');
    Route::get('/course/{course}/session/{courseSession}/edit', 'CourseSessionsController@edit')->name('admin.course.sessions.edit');
    Route::post('/course/{course}/session/{courseSession}/edit', 'CourseSessionsController@storage')->name('admin.course.sessions.edit');
    Route::get('/course/session/{courseSession}/delete', 'CourseSessionsController@delete')->name('admin.course.sessions.delete');
    Route::post('/course/session/delete', 'CourseSessionsController@delAll')->name('admin.course.sessions.delete.all');
    //course Student Mnagement
    Route::get('/course/{course}/students', 'CourseStudentsController@Student')->name('admin.course.student');
    Route::post('/course/{course}/students', 'CourseStudentsController@SyncStudent')->name('admin.course.student');
    Route::get('/course/{course}/students/{student}/delete', 'CourseStudentsController@delteone')->name('admin.course.student.delete.one');
    Route::post('/course/{course}/students/delete', 'CourseStudentsController@deltemany')->name('admin.course.student.delete.many');


});


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['see-dashboard', 'admin']], function () {
    /*discount*/
    Route::get('discount', 'DisCountController@index')->name('admin.discount.index');
    Route::post('discount', 'DisCountController@edit')->name('admin.discount.index');
    Route::get('discount/create', 'DisCountController@create')->name('admin.discount.create');
    Route::post('discount/create', 'DisCountController@storage')->name('admin.discount.create');
    Route::get('discount/{discount}/delete', 'DisCountController@delete')->name('admin.discount.del');
    /*ads*/
    Route::get('ads', 'AdsController@index')->name('admin.ads.index');
    Route::get('ads/edit/{aid}', 'AdsController@edit')->name('admin.ads.edit');
    Route::post('ads/edit/{aid}', 'AdsController@saveEdit');
    Route::get('ads/{ads}/delete', 'AdsController@delete')->name('admin.ads.del');

    Route::post('ads', 'AdsController@create');
    /*options */
    Route::get('/options', 'OptionsController@index')->name('admin.options');
    Route::post('/options', 'OptionsController@storage')->name('admin.options');
});


